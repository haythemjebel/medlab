import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthentificationService} from '../../shared/service/authentification.service';
import {Message} from 'primeng/api';
import {User} from 'src/app/shared/model/user';
import {TranslateService} from '@ngx-translate/core';

declare let QB: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  msgs: Message[] = [];
  loginForm: FormGroup;
  errorMessage = '';
  CREDENTIALS: any;
  loading = false;
  submitted = false;
  currentUser: any;
  haslience: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthentificationService,

    public translate: TranslateService
  ) {
    // if already logged in
    if (localStorage.getItem('currentUser')) {
      this.router.navigate(['home/dashboard']);
    }

  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value).subscribe(result => {

      localStorage.setItem('currentUser', JSON.stringify((result.body)));
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      localStorage.setItem('token', result.headers.get('Authorization'));
      this.router.navigate(['home/dashboard']);
      this.loading = false;
    },
      (error) => {

        this.onLoginError(error);
        this.loading = false;


      });
  }

  private onLoginError(error: any) {
    const errorLogin = error.headers.get('X-validation-error');
    switch (errorLogin) {
      case 'error.badCredentials':
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: `L'adresse e-mail ou le mot de passe n'est pas valide` });
        break;
      case 'error.userIsDisabled':
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: `L'utilisateur est désactivé` });
        break;
      case 'error.userIsLocked':
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: `L'utilisateur est verrouillé` });
        break;
      default:
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: `Vous ne pouvez pas vous identifier` });
        break;
    }
  }


  hasAuthoriseToReports(authorities: any[]): boolean {
    return authorities.find(authoritie => {
      return authoritie.authority === 'Consulter-Tableau-Rapports';
    });
  }
  updateauthority(user :User){
    let authorityuser =user.authorities.filter(res=>{
      return res.authority != 'Permission_chat'
    })
    this.currentUser.authorities=authorityuser
    localStorage.setItem('currentUser', JSON.stringify((this.currentUser)));
  }

}

