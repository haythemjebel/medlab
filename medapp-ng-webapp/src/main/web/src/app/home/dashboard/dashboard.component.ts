import { Component, OnInit } from '@angular/core';
import { hasAuthorities } from 'src/app/shared/functions/hasAuthorities.function';
import { PermissionsEnum } from 'src/app/shared/helpers/permissions.enum';
import { User } from 'src/app/shared/model/user';
import {PrimeIcons} from 'primeng/api';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  currentUser: User;
  events: any[];
  constructor() { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.events = [
      {status: 'Processing', date: '15/10/2020 10:30', icon: PrimeIcons.COG, color: '#ff0000'},
      {status: 'Processing', date: '15/10/2020 14:00', icon: PrimeIcons.COG, color: '#ff0000'},
      {status: 'Delivered', date: '15/10/2020 16:15', icon: PrimeIcons.CHECK, color: '#098765'},
      {status: 'Delivered', date: '16/10/2020 10:00', icon: PrimeIcons.CHECK, color: '#098765'}
  ];

  }
  hasAuthoritieCreate() {
    return hasAuthorities(this.currentUser.authorities, PermissionsEnum.CreateUser);
  }
  public back() {
    window.history.back();
  }
}
