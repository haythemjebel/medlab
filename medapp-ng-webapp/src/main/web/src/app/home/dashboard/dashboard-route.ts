import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/helpers/auth.guard';
import { PermissionsEnum } from '../../shared/helpers/permissions.enum';
import { DashboardComponent } from './dashboard.component';

export const DashboardRoute: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: {
      permission: [
        PermissionsEnum.ConsulterUser
      ]
    }
  }
];
