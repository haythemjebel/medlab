import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Message, MessageService} from 'primeng/api';
import {UserService} from 'src/app/shared/service/user.service';
import {FileUpload} from 'primeng/fileupload';
import {User} from 'src/app/shared/model/user';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.sass']
})
export class UserProfileComponent implements OnInit {
  profileform: FormGroup = this.fb.group({
    'prenom': new FormControl('', Validators.required),
    'nom': new FormControl('', Validators.required),
    'email': new FormControl('', Validators.compose([Validators.required, Validators.email])),
    'identifiant': new FormControl('', Validators.required),
    'profile': new FormControl('', Validators.required),
    'appartenance': new FormControl('', Validators.required)
  });
  pwdform: FormGroup;
  msgs: Message[];
  user: User;
  currentUser: User;
  src: string = '../../../assets/layout/images/anonymous-user.png';
  readonly: boolean = true;
  isSaving: boolean = false;
  pwdReadonly: boolean = true;
  isPwdSaving: boolean = false;
  projectOfUser: any[];

  @ViewChild("pic", { static: false }) fileUpload: FileUpload;

  constructor(private userService: UserService, private messageService: MessageService, private fb: FormBuilder
            ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log(this.currentUser)
    this.userService.findOne(this.currentUser.id+"").subscribe(res => {
      this.user = res;
      this.profileform.setValue({
        prenom: this.user.prenom,
        nom: this.currentUser.nom,
        email: this.user.email,
        identifiant: this.user.identifiant,
        profile: this.user.profile.codeMetier,
      })
    })

    this.pwdform = this.fb.group({
      'id': new FormControl(this.currentUser.id),
      'current': new FormControl('', Validators.required),
      'newPassword': new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
      'confirmPassword': new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
    });

  }

  public chooseFile() {
    this.fileUpload.advancedFileInput.nativeElement.click();
  }

  edit() {
    this.readonly = false;
  }

  public onPicUploadError(event: any) {
    if (event.error.status && event.error.status !== 200)
      this.msgs.push({ severity: 'warn', summary: '', detail: `Une erreur est survenue lors du téléchargement de la Photo` });
    else {
      this.msgs = [];
      this.msgs.push({ severity: 'info', summary: '', detail: `La photo est téléchargé avec succès` });
    }
  }
  public onPicUploadSuccess(event: any) {
    if (event.error.status && event.error.status !== 200)
      this.msgs.push({ severity: 'info', summary: '', detail: `La photo est téléchargé avec succès` });
  }
  public onPicSelect(event: any) {
    this.src = event.files[0].objectURL;
  }

  public save() {
    this.isSaving = true;
    if (this.user.id) {
      if (this.profileform.touched) {
        this.user.prenom = this.profileform.get('prenom').value;
        this.user.nom = this.profileform.get('nom').value;
        this.userService.update(this.user).subscribe(response => {
          this.isSaving = false;
          this.msgs = [];
          this.currentUser.nom = this.user.nom;
          this.currentUser.prenom = this.user.prenom;
          localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          this.msgs.push({ severity: 'success', summary: '', detail: `Votre profile est enregistré avec succès` });
          this.readonly = true;
        }, (error) => {
          this.isSaving = false;
          this.msgs = [];
          this.msgs.push({ severity: 'error', detail: `Une erreur est survenue` });
        })
      } else {
        this.isSaving = false;
        this.readonly = true;
      }
    }
  }

  private onSaveError(error: any) {
    var error = error.headers.get('X-validation-error');
    switch (error) {
      case 'error.passwordlength8_100':
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: `Le mot de passe doit être entre 8 et 100 caractères` });
        break;
      case 'error.currentpwdIncorrect':
        this.msgs = [];
        this.msgs.push({ severity: 'error', detail: `le mot de passe actuel est incorrecte` }); break;
    }
  }

  savePwd() {
    this.isPwdSaving = true;

    const newPassword = this.pwdform.get(['newPassword']).value;
    if (newPassword !== this.pwdform.get(['confirmPassword']).value) {
      this.msgs = [];
      this.msgs.push({ severity: 'error', detail: `les mots de passe ne sont pas identiques` });
    } else {
      this.userService.savePassword(newPassword, this.pwdform.get(['current']).value).subscribe(
        () => {
          this.msgs = [];
          this.msgs.push({ severity: 'info', detail: `le mot de passe est mis à jour avec succès` });
          this.pwdReadonly = true;
          this.isPwdSaving = false;
        },
        (error) => {
          this.isPwdSaving = false;
          this.onSaveError(error);
        }
      );
    }
  }

  editPwd() {
    this.pwdReadonly = false;
  }

  hasAuthorities(authorities, authority): boolean {
    let has = authorities.find(auth => {
      return auth.authority == authority
    })
    if (has) return true;
    else return false;
  }

}
