import {Component, OnInit} from '@angular/core';
import {User} from 'src/app/shared/model/user';
import {UserService} from 'src/app/shared/service/user.service';
import {PageResponse} from 'src/app/shared/support/paging';
import {LazyLoadEvent, MenuItem, Message, MessageService} from 'primeng/api';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from 'src/environments/environment';
import {Subscription} from 'rxjs';
import {Filter} from 'src/app/layouts/filters-panel/filter';
import {PermissionService} from 'src/app/shared/service/permission.service';
import {TranslateService} from '@ngx-translate/core';
import {Column} from '../../shared/helpers/column';
import {hasAuthorities} from '../../shared/functions/hasAuthorities.function';
import {PermissionsEnum} from '../../shared/helpers/permissions.enum';


@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.sass']
})
export class UserManagementComponent implements OnInit {
  msgs: Message[] = [];
  private subscriptionRegion: Subscription;
  userColumns: Column[] = [];
  cmItems: MenuItem[] = [];
  userCriteria: User = new User();
  currentPage: PageResponse<User> = new PageResponse<User>(0, 0, []);
  rows: number = 30;
  loading: boolean;
  selectedUser: User[];
  selectedUserShow: User;
  userFilters: Filter[] = [];
  hasFastSearch: boolean = false;
  currentUser: User;
  private allSelectedUsers: Set<User>;
  users: User[];
  lastIndex: number;
  displayMessage: boolean = true;

  constructor(private userService: UserService, private router: Router,
              private route: ActivatedRoute, private permissionService: PermissionService,
              public translate: TranslateService, private messageService: MessageService) {
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.userColumns.push({field: 'email', header: this.translate.instant('PAGE-UTILISATEUR.LABELS.EMAIL'),
      width: '150px', type: 'string', sortable: true, tooltip: this.translate.instant('PAGE-UTILISATEUR.TOOLTIPS.EMAIL')});
    this.userColumns.push({field: 'identifiant', header: this.translate.instant('PAGE-UTILISATEUR.LABELS.IDENTIFIANT'),
      width: '150px', type: 'string', sortable: true, tooltip: this.translate.instant('PAGE-UTILISATEUR.TOOLTIPS.IDENTIFIANT')});
    this.userColumns.push({field: 'activated', header: this.translate.instant('PAGE-UTILISATEUR.LABELS.ACTIF'),
      width: '48px', type: 'boolean', sortable: true, tooltip: this.translate.instant('PAGE-UTILISATEUR.TOOLTIPS.ACTIF')});

    this.userColumns.push({field: 'profile', header: this.translate.instant('PAGE-UTILISATEUR.LABELS.PROFIL'),
        width: '100px', type: 'profile', sortable: true, tooltip: this.translate.instant('PAGE-UTILISATEUR.TOOLTIPS.PROFIL')});

    this.userColumns.push({field: 'prenom', header: this.translate.instant('PAGE-UTILISATEUR.LABELS.PRENOM'),
      width: '150px', type: 'string', sortable: true, tooltip: this.translate.instant('PAGE-UTILISATEUR.TOOLTIPS.PRENOM')});
    this.userColumns.push({field: 'nom', header: this.translate.instant('PAGE-UTILISATEUR.LABELS.NOM'),
      width: '150px', type: 'string', sortable: true, tooltip: this.translate.instant('PAGE-UTILISATEUR.TOOLTIPS.NOM') });
    this.userColumns.push({field: 'createdDate', header: this.translate.instant('CHAMPS-COMMUN.CREE-LE'),
      width: '150px', type: 'datetime', sortable: true, tooltip: this.translate.instant('CHAMPS-COMMUN.CREE-LE-TOOLTIP')});
    this.userColumns.push({field: 'lastModifiedDate', header: this.translate.instant('CHAMPS-COMMUN.MODIFIE-LE'),
      width: '150px', type: 'datetime', sortable: true, tooltip: this.translate.instant('CHAMPS-COMMUN.MODIFIE-LE-TOOLTIP')});
    this.userColumns.push({field: 'createdBy', header: this.translate.instant('CHAMPS-COMMUN.CREE-PAR'),
      width: '150px', type: 'string', sortable: true, tooltip: this.translate.instant('CHAMPS-COMMUN.CREE-PAR')});
    this.userColumns.push({field: 'lastModifiedBy', header: this.translate.instant('CHAMPS-COMMUN.MODIFIE-PAR'),
      width: '150px', type: 'string', sortable: true, tooltip: this.translate.instant('CHAMPS-COMMUN.MODIFIE-PAR')});

    this.cmItems.push({ label: this.translate.instant('CHAMPS-COMMUN.VISUALISER'),
      icon: 'fa fa-eye', command: () => this.viewUser(this.selectedUserShow)});
    this.cmItems.push({ label: this.translate.instant('CHAMPS-COMMUN.EDITER'),
      icon: 'pi pi-pencil', command: () => this.editUser(this.selectedUserShow)});

    this.userFilters.push({id: 'activated', type: 'boolean', label: 'Actif'});
    this.userFilters.push({id: 'email', type: 'string', label: 'Email'});
    this.userFilters.push({id: 'identifiant', type: 'string', label:  this.translate.instant('PAGE-UTILISATEUR.LABELS.IDENTIFIANT')});
    this.userFilters.push({id: 'prenom', type: 'string', label: this.translate.instant('PAGE-UTILISATEUR.LABELS.PRENOM')});
    this.userFilters.push({id: 'nom', type: 'string', label:  this.translate.instant('PAGE-UTILISATEUR.LABELS.NOM')});
    this.userFilters.push({id: 'createdDate', type: 'date', label: this.translate.instant('CHAMPS-COMMUN.CREE-LE')});
    this.userFilters.push({id: 'lastModifiedDate', type: 'date', label: this.translate.instant('CHAMPS-COMMUN.MODIFIE-LE')});
    this.userFilters.push({id: 'createdBy', type: 'string', label: this.translate.instant('CHAMPS-COMMUN.CREE-PAR')});
    this.userFilters.push({id: 'lastModifiedBy', type: 'string', label: this.translate.instant('CHAMPS-COMMUN.MODIFIE-PAR')});

    this.hasFastSearch = this.permissionService.HasPermission('Recherche-Rapide');

    this.allSelectedUsers = new Set();
  }

  loadUsers(event: LazyLoadEvent) {
    this.loading = true;
    this.lastIndex = event.first;
    this.userService.findUserByCriteria(this.userCriteria, event).subscribe(response => {
        this.currentPage = response;
        this.users = this.currentPage.content;
        this.selectedUser = [];

        for (const p of this.users) {
          for (const ps of this.allSelectedUsers) {
            if (p.id === ps.id) {
              this.selectedUser.push(p);
              break;
            }
          }
        }
        this.loading = false;
      },
      () => {
      });
  }

// double click sur ligne du tableau pour aller en mode visualisation

  onRowSelect(event, user: User) {
    this.viewUser(user);
  }

  findUserBy(criteria: any) {
    this.userCriteria = criteria;
    this.loadUsers({first: 0, rows: this.rows});
  }

  editUser(user: User) {
    if (hasAuthorities(this.currentUser.authorities, PermissionsEnum.UpdateUser)) {
      this.router.navigate([user.id, 'edit'], {relativeTo: this.route}).then(() => {
      });
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Autorisation',
        detail: 'Vous n’avez pas l’autorisation d’accéder à cette fonction'
      });
    }
  }

  viewUser(user: User) {
    if (hasAuthorities(this.currentUser.authorities, PermissionsEnum.ConsulterUser)) {
      this.router.navigate([user.id, 'view'], {relativeTo: this.route}).then(() => {
      } );
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Autorisation',
        detail: 'Vous n’avez pas l’autorisation d’accéder à cette fonction'
      });
    }
  }

  public getUserImgResource(link: string): any {
    if (!link) {
      return '../../../assets/layout/images/anonymous-user.png';
    }
    return environment.apiUrl + link;
  }


  hasAuthoritieCreate() {
    return hasAuthorities(this.currentUser.authorities, PermissionsEnum.CreateUser);
  }

  onUserSelected(event: any) {
    for (const u of this.allSelectedUsers) {
      if (u.id === event.data.id ) {
        return;
      }
    }
    this.allSelectedUsers.add(event.data);
  }

  onUserUnselected(event: any) {
    if (!this.allSelectedUsers.delete(event.data)) {
      for (const p of this.allSelectedUsers) {
        if (p.id === event.data.idClient) {
          this.allSelectedUsers.delete(p);
          break;
        }
      }
    }
  }

  allUsersSelected(event: any) {

    if (event.checked === false) {
      // remove only items of the current page
      for (const p of this.users) {
        for (const ps of this.allSelectedUsers) {
          if (p.id === ps.id) {
            this.allSelectedUsers.delete(ps);
            break;
          }
        }
      }
      return;
    }
    for (const p of this.selectedUser) {
      let userAlreadyExist = false;

      for (const ps of this.allSelectedUsers) {
        if (ps.id === p.id) {

          userAlreadyExist = true;
          break;
        }
      }

      if (userAlreadyExist === false) {
        this.allSelectedUsers.add(p);
      }
    }
  }

  sendEmails($event: MouseEvent) {
  console.log('selected users', Array.from(this.allSelectedUsers));
  if (Array.from(this.allSelectedUsers).length === 0) {
    this.msgs = [];
    this.msgs.push({
      severity: 'error',
      summary: '',
      detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.VIDE-SELECTION')
    });
    setTimeout(function () {
      this.displayMessage = true;
    }.bind(this), 5000);
    // the setTimeout callback lose a scope to the "displayMessage" variable, You should use arrow function or use bind =>
    // to pass proper context to the setTimeout callback function
    this.displayMessage = false;
  } else {
    const selection = this.allSelectedUsers;
    this.resetSelection();
    return this.userService.sendEmails(Array.from(selection)).subscribe();

  }

  }

  hasAuthoritieSendMails() {
    return hasAuthorities(this.currentUser.authorities, PermissionsEnum.SendEmails);
  }

  resetSelection() {

    this.allSelectedUsers = new Set();
    this.selectedUser = [];
    this.loadUsers({first: this.lastIndex, rows: this.rows});

  }

}
