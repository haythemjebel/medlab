import {Component, OnInit, ViewChild} from '@angular/core';
import {Message, MessageService, SelectItem} from 'primeng/api';
import {ActivatedRoute} from '@angular/router';
import {UserService} from 'src/app/shared/service/user.service';
import {User} from 'src/app/shared/model/user';
import {FileUpload} from 'primeng/fileupload';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Profil} from 'src/app/shared/model/profil';
import {ProfilCriteria} from 'src/app/shared/model/profilCriteria';
import {TranslateService} from '@ngx-translate/core';
import {ProfilService} from '../../../shared/service/profil.service';
import {PermissionsEnum} from '../../../shared/helpers/permissions.enum';
import {hasAuthorities} from '../../../shared/functions/hasAuthorities.function';

declare let QB: any;

@Component({
  selector: 'app-user-management-update',
  templateUrl: './user-management-update.component.html',
  styleUrls: ['./user-management-update.component.sass']
})
export class UserManagementUpdateComponent implements OnInit {
  msgs: Message[] = [];
  userform: FormGroup;
  readonly: boolean;
  user: User = null;
  profilSuggestions: Profil[];
  isSaving: boolean;
  src: string = '../../../assets/layout/images/anonymous-user.png';
  activated: boolean = true;
  pc: any;
  public reg = '[A-Z]*';
  currentUser: any;
  loading: boolean;
  appartenances: SelectItem[];

  constructor(private userService: UserService, private messageService: MessageService,
              private route: ActivatedRoute, private fb: FormBuilder, private profilService: ProfilService,
              public translate: TranslateService) {

  }

  @ViewChild('pic', {static: false}) fileUpload: FileUpload;

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.route.data.subscribe(data => {
      this.readonly = data.readonly;
    });

    this.buildForm();
    this.route.params.subscribe(data => {
      if (data.id) {
        this.loading = true;
        this.userService.findOne(data.id).subscribe(response => {
          this.loading = false;
          this.user = response;
          this.updateForm(this.user); // remplir les champs par les data user
        });
      } else {
        this.user = new User();
        this.updateForm(this.user); // reset form
      }
    });
  }


  buildForm() {

    this.userform = this.fb.group({
      id: new FormControl(null),
      nonLocked: new FormControl(true),
      prenom: new FormControl('', Validators.required),
      nom: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      activated: new FormControl(true, Validators.required),
      profile: new FormControl(null, Validators.required),
      identifiant: new FormControl('', [Validators.required, Validators.maxLength(20), Validators.pattern(this.reg)]),
    });

  }

  public back() {
    window.history.back();
  }

  public save() {
    this.isSaving = true;
    this.updateUser(); // get value from userform
    if (this.user.id) { // cas update
      if (this.userform.touched) {
        this.userService.update(this.user).subscribe(response => {
          this.updateForm(response);
          this.isSaving = false;
          this.readonly = true;
          this.msgs = [];
          this.msgs.push({
            severity: 'success',
            summary: '',
            detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.SUCCESS-ADD', {nom: this.user.nom , prenom: this.user.prenom})
          });
        }, (error) => {
          this.isSaving = false;
          this.onSaveError(error);
        });
      } else {
        this.isSaving = false;
      }
    } else { // save new user
      this.userService.create(this.user).subscribe(response => {
        this.isSaving = false;
        this.updateForm(response);
        this.msgs = [];
        this.readonly = true;
        this.msgs.push({
          severity: 'success',
          summary: '',
          detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.SUCCESS-ADD', {nom: this.user.nom , prenom: this.user.prenom})
        });
      }, (error) => {
        this.isSaving = false;
        this.onSaveError(error);
      });
    }
  }

  public onPicUploadError(event: any) {
    if (event.error.status && event.error.status !== 200) {
      this.msgs.push({severity: 'warn', summary: '', detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.ERROR-UPLOAD-PICTURE')});
    }
  }

  public onPicUploadSuccess(event: any) {
    if (event.error.status && event.error.status !== 200) {
      this.msgs.push({severity: 'info', summary: '', detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.SUCCESS-UPLOAD-PICTURE')});
    }
  }


  public onPicSelect(event: any) {
    this.src = event.files[0].objectURL;
  }

  public chooseFile() {
    this.fileUpload.advancedFileInput.nativeElement.click();
  }


  private updateForm(user: User): void {
    this.userform.reset();
    this.userform.setValue({
      id: user.id,
      nonLocked: user.activated,
      email: user.email,
      nom: user.nom,
      prenom: user.prenom,
      activated: user.activated,
      profile: user.profile,
      identifiant: user.identifiant,
    });
  }

  private updateUser(): void {
    this.user.id = this.userform.get(['id']).value;
    this.user.email = this.userform.get(['email']).value;
    this.user.nom = this.userform.get(['nom']).value;
    this.user.prenom = this.userform.get(['prenom']).value;
    this.user.activated = this.userform.get(['activated']).value == null ? false : this.userform.get(['activated']).value;
    this.user.profile = this.userform.get(['profile']).value;
    this.user.identifiant = this.userform.get(['identifiant']).value;
    this.user.nonLocked = this.user.activated;

  }


  private onSaveError(error: any) {
    const erreur = error.headers.get('X-validation-error');
    switch (erreur) {
      case 'error.emailAlreadyUsed' :
        this.msgs = [];
        this.msgs.push({severity: 'error', detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.EMAIL-ALREADY-USED')});
        break;
      case 'error.idexists' :
        this.msgs = [];
        this.msgs.push({severity: 'error', detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.ID-EXISTS')});
        break;
      case 'error.userDoesNotExists' :
        this.msgs = [];
        this.msgs.push({severity: 'error', detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.USER-DOES-NOT-EXIST')});
        break;
      case 'error.identifiantAlreadyUsed' :
        this.msgs = [];
        this.msgs.push({severity: 'error', detail: this.translate.instant('PAGE-UTILISATEUR.MESSAGES.IDENTIFIANT-ALREADY-USED')});
        break;

    }
  }

  edit(user: User) {
    this.readonly = false;
    this.user = user;
    this.updateForm(this.user); // remplir les champs par les data user
  }

  searchProfiles(event: any) {
    const pc = new ProfilCriteria();
    pc.codeMetier = event.query;
    this.profilService.findProfilByCriteria(pc, {first: 0, rows: 100}).subscribe(res => {
      this.profilSuggestions = res.content;
    });
  }

  resetPassword(){
    this.isSaving = true;
    this.userService.resetPassword(this.user.id).subscribe(res=>{
      this.msgs = [];
      this.msgs.push({severity: 'success', detail: this.translate.instant('PAGE-USER.MSG-SUCCESS-PWD')});
      this.isSaving = false;
    }, error=>{
      this.msgs = [];
      this.msgs.push({severity: 'warn', detail: this.translate.instant('PAGE-USER.MSG-ERROR-PWD')});
      this.isSaving = false;
    });

  }

  hasAuthoritieCreate() {
    return hasAuthorities(this.currentUser.authorities, PermissionsEnum.CreateUser);
  }

  hasAuthoritieUpdateProfile() {
    return hasAuthorities(this.currentUser.authorities, PermissionsEnum.UpdateProfil) && this.readonly === false;
  }

  hasAuthoritiesResetPassword(){
    return hasAuthorities(this.currentUser.authorities, PermissionsEnum.ResetPassword)
  }

  saveUserQuickBlox() {
    const userCreated = {
      login: this.userform.get(['email']).value,
      full_name: this.userform.get(['prenom']).value + ' ' + this.userform.get(['nom']).value,
      password: 'quickblox12'
    };
    const userConnected = {
      login: sessionStorage.getItem('loginDetails'),
      password: 'quickblox12'
    };

    console.log('userConnected', userConnected, 'userAdded', userCreated);
    QB.createSession(userConnected, function(err, result): void {
      // callback function
      QB.users.create(userCreated, function(err, res): void {
        if (err) {
          console.log(err);
        } else {
          console.log('user is created');
        }
      });
    });
  }


}

