import {Routes} from '@angular/router';
import {UserManagementUpdateComponent} from './user-management-update/user-management-update.component';
import {UserManagementComponent} from './user-management.component';
import {AuthGuard} from '../../shared/helpers/auth.guard';
import {PermissionsEnum} from '../../shared/helpers/permissions.enum';

export const userManagementRoute: Routes = [
    {
      path: '',
      pathMatch: 'full',
      component : UserManagementComponent,
      canActivate: [AuthGuard],
      data: {
        permission: [
          PermissionsEnum.ConsulterUser
        ]
      }
    },
    {
      path: ':id/view',
      component: UserManagementUpdateComponent,
      canActivate: [AuthGuard],
      data: {
        readonly: true,
        permission: [
          PermissionsEnum.ConsulterUser
        ]
        }
    },
    {
        path: 'new',
        component: UserManagementUpdateComponent,
        canActivate: [AuthGuard],
        data: {
         permission: 
         [PermissionsEnum.CreateUser]
        }
      },
      {
        path: ':id/edit',
        component: UserManagementUpdateComponent,
        canActivate: [AuthGuard],
        data: {
          readonly: false,
          permission: [PermissionsEnum.UpdateUser]
        }
      }
];
