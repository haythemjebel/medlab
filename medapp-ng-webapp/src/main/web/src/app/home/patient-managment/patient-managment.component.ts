import {Component, OnInit} from '@angular/core';
import {ConfirmationService, Message, MessageService} from 'primeng/api';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from 'src/app/shared/service/patient.service';
import {PermissionService} from 'src/app/shared/service/permission.service';
import {TranslateService} from '@ngx-translate/core';
import {Patient} from 'src/app/shared/model/patient';
import {User} from 'src/app/shared/model/user';
import {PermissionsEnum} from 'src/app/shared/helpers/permissions.enum';
import {hasAuthorities} from 'src/app/shared/functions/hasAuthorities.function';
import { RendezVous } from 'src/app/shared/model/rendez_vous';
import { RenduVousService } from 'src/app/shared/service/renduvous.service';

@Component({
  selector: 'app-patient-managment',
  templateUrl: './patient-managment.component.html',
  styleUrls: ['./patient-managment.component.sass'],
  providers: [MessageService, ConfirmationService]
})
export class PatientManagmentComponent implements OnInit {

  msgs: Message[] = [];
  
  productDialog: boolean;

  products: Patient[];

  ListRennduvous: RendezVous[];

  product: Patient;

  selectedProducts: Patient[];

  submitted: boolean;

  statuses: any[];

  currentUser: User;

  constructor(private router: Router, private patientService: PatientService, private renduvousService: RenduVousService ,
    private route: ActivatedRoute, private permissionService: PermissionService,
    public translate: TranslateService, private messageService: MessageService, private confirmationService: ConfirmationService) {
  }
  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.patientService.findAllPatients().subscribe(data => {
      this.products = data
    });
  }

  onRowSelect(event, patient: Patient) {
    this.viewPatient(patient);
  }
  afficheRendu($event , op , product){
    this.renduvousService.findAllRenduvousByIdPatient(product.id).subscribe(res=>{
      this.ListRennduvous=res
      op.toggle($event)
    })
  }

  viewPatient(patient: Patient) {
    if (hasAuthorities(this.currentUser.authorities, PermissionsEnum.ConsulterUser)) {
      this.router.navigate([patient.id, 'view'], { relativeTo: this.route }).then(() => {
      });
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Autorisation',
        detail: 'Vous n’avez pas l’autorisation d’accéder à cette fonction'
      });
    }
  }
  openNew() {
    this.product = new Patient();
    this.submitted = false;
    this.productDialog = true;
  }

  deleteProduct(product: Patient) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + product.nom + " " + product.prenom + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.patientService.deleteOnePatient(product.id).subscribe(data=>{
          this.products = this.products.filter(val => val.id !== product.id);
          this.product = new Patient();
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Patient Deleted', life: 3000 });
        })

      }
    });
  }
  hideDialog() {
    this.productDialog = false;
    this.submitted = false;
  }
  onUpload(event) {
    console.log("file upload");
    for(let file of event.files) {
      this.patientService.pushFileToStorage(file).subscribe(res=>{
        console.log("file upload")
      })
    }
  }
  saveProduct() {
    this.submitted = true;
    if (this.product.nom.trim()) {
      if (this.product.id) {
        this.patientService.updatePatient(this.product).subscribe(res=>{
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Patient Updated', life: 3000 });
        })

      }
      else {
        this.patientService.createPatient(this.product).subscribe(data=>{
          this.products.push(this.product);
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Patient Created', life: 3000 });
        })

      }

      this.products = [...this.products];
      this.productDialog = false;
      this.product = new Patient()
    }
  }

}
