import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/helpers/auth.guard';
import { PermissionsEnum } from '../../shared/helpers/permissions.enum';
import { PatientManagmentUpdateComponent } from './patient-managment-update/patient-managment-update.component';
import { PatientManagmentComponent } from './patient-managment.component';

export const patientManagementRoute: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PatientManagmentComponent,
    canActivate: [AuthGuard],
    data: {
      permission: [
        PermissionsEnum.ConsulterUser
      ]
    }
  }, {
    path: 'new',
    component: PatientManagmentUpdateComponent,
    canActivate: [AuthGuard],
    data: {
      permission:
        [PermissionsEnum.ConsulterUser]
    }
  },
  {
    path: ':id/edit',
    component: PatientManagmentUpdateComponent,
    canActivate: [AuthGuard],
    data: {
      readonly: false,
      create:false,
      permission: [PermissionsEnum.ConsulterUser]
    }
  },{
    path: ':id/view',
    component: PatientManagmentUpdateComponent,
    canActivate: [AuthGuard],
    data: {
      readonly: true,
      permission: 
        [PermissionsEnum.ConsulterUser
        ]
    }
  },
];
