import {Component, OnInit} from '@angular/core';
import {MenuItem, Message, MessageService, SelectItem} from 'primeng/api';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Column} from 'src/app/shared/helpers/column';
import {PageResponse} from 'src/app/shared/support/paging';
import {TranslateService} from '@ngx-translate/core';
import {PermissionsEnum} from 'src/app/shared/helpers/permissions.enum';
import {hasAuthorities} from 'src/app/shared/functions/hasAuthorities.function';
import {Patient} from 'src/app/shared/model/patient';
import {PatientService} from 'src/app/shared/service/patient.service';
import {RendezVous} from "../../../shared/model/rendez_vous";
import {RenduVousService} from 'src/app/shared/service/renduvous.service';

@Component({
  selector: 'app-patient-managment-update',
  templateUrl: './patient-managment-update.component.html',
  styleUrls: ['./patient-managment-update.component.sass']
})
export class PatientManagmentUpdateComponent implements OnInit {
  patientColumns: Column[] = [];
  cmItems: MenuItem[] = [];
  patientCriteria: Patient = new Patient();
  currentPage: PageResponse<Patient> = new PageResponse<Patient>(0, 0, []);
  rows: number = 30;
  loading: boolean;
  selectedPatient: Patient[];
  selectedPatientShow: Patient;
  currentUser: any;
  currentPatient: any;
  private allSelectedPatients: Set<Patient>;
  Patients: Patient[];
  lastIndex: number;
  msgs: Message[] = [];
  checked: boolean = true;
  update: boolean = false;
  isSaving: boolean;
  error: any;
  readonly: boolean;
  create: boolean;
  success: any;
  options: SelectItem[];
  patientform: FormGroup;
  patient: Patient = new Patient();
  ListRennduvous: RendezVous[];
  valuedateNaissa : Date;
  constructor(public translate: TranslateService, private fb: FormBuilder,
    private patientService: PatientService, private router: Router,
    private messageService: MessageService, private route: ActivatedRoute,
  private renduvousService: RenduVousService ) {

  }

  ngOnInit() {
    this.buildForm();
    this.route.data.subscribe(data => {
      this.readonly = data.readonly;
      this.create = data.create;
    });
    this.route.params.subscribe(data => {
      if (data.id) {
        this.currentPatient= data.id
      } 
    });
    this.searchePatientById();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.renduvousService.findAllRenduvousByIdPatient(this.currentPatient).subscribe(res=>{
      this.ListRennduvous=res;

    })
  }
  buildForm() {
    this.patientform = this.fb.group({
      idpatient: new FormControl(''),
      nom: new FormControl(''),
      prenom: new FormControl(''),
      diagnostic: new FormControl(''),
      atcds: new FormControl(''),
      profession: new FormControl(''),
      adresse: new FormControl(''),
      tel: new FormControl(''),
      dateNaissance: new FormControl(''),
      email: new FormControl('',[Validators.email]),
      cnamNum: new FormControl(''),
      adressePar: new FormControl(''),
      ficheMedicaleNumber: new FormControl(''),
    });
  }
  searchePatientById() {
    this.route.params.subscribe(data => {
      if (data.id) {
        this.loading = true;
        this.patientService.findOnePatient(data.id).subscribe(response => {
          this.loading = false;
          this.patient = response;
          this.updateForm(this.patient);
        });
      } else {
        this.patient = new Patient();
        this.updateForm(this.patient); // reset form
      }
      console.log(this.patient);

    });

  }
  public back() {
    window.history.back();
  }
  public save() {
    this.isSaving = true;
    if (this.patient.id != null) { // cas update
      this.updatePatientForUpdate(); // get value from userform
      if (this.patientform.touched) {
        console.log(this.patient)
        this.patientService.updatePatient(this.patient).subscribe(response => {
          this.updateForm(response);
          this.isSaving = false;
          this.readonly = true;
          this.msgs = [];
          this.msgs.push({ severity: 'success', summary: '', detail: 'Le patient est modifié avec succès' });
        }, (error) => {
          this.isSaving = false;
          console.log(error)
        });
      } else {
        this.isSaving = false;
      }
    } else {
      // save new compagnie
      this.recuperePatientForAdd();
    }
  }
  private updatePatientForUpdate(): void {
    this.patient.email = this.patientform.get(['email']).value;
    this.patient.nom = this.patientform.get(['nom']).value;
    this.patient.prenom = this.patientform.get(['prenom']).value;
    this.patient.cnamNum = this.patientform.get(['cnamNum']).value;
    this.patient.tel = this.patientform.get(['tel']).value;
    this.patient.adresse = this.patientform.get(['adresse']).value;
    this.patient.diagnostic = this.patientform.get(['diagnostic']).value;
    this.patient.atcds = this.patientform.get(['atcds']).value;
    this.patient.dateNaissance = this.patientform.get(['dateNaissance']).value;
    this.patient.adressePar = this.patientform.get(['adressePar']).value;
  }
  private recuperePatientForAdd(): void {
    this.patient.adresse = this.patientform.value.adresse;
    this.patient.adressePar = this.patientform.value.adressePar;
    this.patient.cnamNum = this.patientform.value.cnamNum;
    this.patient.nom = this.patientform.value.nom;
    this.patient.prenom = this.patientform.value.prenom;
    this.patient.profession = this.patientform.value.profession;
    this.patient.tel = this.patientform.value.tel;
    this.patient.email = this.patientform.value.email;
    this.patient.atcds = this.patientform.value.atcds;
    this.patient.diagnostic = this.patientform.value.diagnostic;
    this.patient.dateNaissance = this.patientform.value.dateNaissance;
    console.log(this.patient)
    this.patientService.createPatient(this.patient).subscribe(response => {
      this.isSaving = false;
      this.readonly = true;
      this.router.navigate(['home/patient-management']);
    }, (error) => {
      console.log(error);
      this.isSaving = false;
    });

  }
  private updateForm(patient: Patient): void {
    this.valuedateNaissa = new Date(patient.dateNaissance);
    this.patientform.patchValue({
      nom: patient.nom,
      prenom: patient.prenom,
      diagnostic: patient.diagnostic,
      atcds: patient.atcds,
      profession: patient.profession,
      adresse: patient.adresse,
      tel: patient.tel,
      email: patient.email,
      cnamNum: patient.cnamNum,
      adressePar: patient.adressePar
    });

    console.log(patient)
  }
  edit(patient: Patient) {
    this.readonly = false;
    this.patient = patient;
    this.updateForm(this.patient); // remplir les champs par les data compagnie
  }
  hasAuthoritieCreate() {
    return hasAuthorities(this.currentUser.authorities, PermissionsEnum.ConsulterUser);
  }

}
