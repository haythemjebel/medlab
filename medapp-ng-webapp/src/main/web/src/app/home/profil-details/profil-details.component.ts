import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProfilService} from 'src/app/shared/service/profil.service';
import {Profil} from 'src/app/shared/model/profil';
import {UserService} from 'src/app/shared/service/user.service';
import {PermissionService} from 'src/app/shared/service/permission.service';
import {Permission} from 'src/app/shared/model/permission';
import {Message, MessageService, TreeNode} from 'primeng/api';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-profil-details',
  templateUrl: './profil-details.component.html',
  styleUrls: ['./profil-details.component.sass']
})
export class ProfilDetailsComponent implements OnInit, AfterViewInit {

  readonly: boolean;
  msgs: Message[] = [];
  profilForm: FormGroup;
  cols: any[];
  colspermission: any[];
  isSaving: boolean = false;
  id: string;
  create: boolean = false;
  profil: Profil = null;
  permissions: Permission[];
  allPermissions: Permission[];
  permissionsGrouped: GroupPermission[] = [];
  myTableNode: TreeNode[] = [];
  selected = [];
  private selectedPermissions: Permission[] = [];
  selectedRolesPermissions: number[] = [];
  selectedNode: TreeNode[] = [];
  private profilPermissions: Permission[] = [];
  userColumns: any[];
  rows: number = 30;
  colsPer = [
    {field: 'name', header: 'Name'}
  ];
  currentUser: any;
  loading: boolean;

  constructor(private routeactive: ActivatedRoute, private messageService: MessageService,
              private router: Router, private profilService: ProfilService, private userService: UserService,
              private permissionService: PermissionService, private fb: FormBuilder, private cd: ChangeDetectorRef,
              public translate: TranslateService) {
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.profilForm = this.fb.group({
      id: new FormControl(null),
      codeMetier: new FormControl('', Validators.required),
      description: new FormControl('')
    });

    this.routeactive.data.subscribe(data => {
      this.readonly = data.readonly;
    });
    this.userColumns = [
      {field: 'prenom', header: 'Prénom', width: '150px', type: 'string', sortable: true},
      {field: 'nom', header: 'Nom', width: '150px', type: 'string', sortable: true},
      {field: 'createdDate', header: 'Date de Création', width: '150px', type: 'datetime', sortable: true},
      {field: 'createdBy', header: 'Créé Par', width: '150px', type: 'string', sortable: true}

    ];
    this.cols = [
      {field: 'nom', header: 'Nom'},
      {field: 'prenom', header: 'Prenom'},
      {field: 'email', header: 'Email'},
      {field: 'region', header: 'Region'}
    ];

    this.colspermission = [
      {field: 'role', header: 'Role'},
      {field: 'description', header: 'Description'}
    ];

    this.routeactive.paramMap.subscribe(params => {
      this.id = params.get('id');

      if (this.id) {
        // edit or view mode
        this.selectPermission();
        this.updateForm(this.profil);

      } else {
        // create mode
        this.getallPermission();
        this.profil = new Profil();
        this.updateForm(this.profil);
      }

    });

  }

  getallPermission() {
    this.loading = true;
    this.permissionService.getAllPermission({first: 0, rows: 50}).subscribe(
      result => {
        this.loading = false;
        this.allPermissions = result.content;
        this.groupedPermissions(this.allPermissions);
      });
  }

  selectPermission() {
    this.loading = true;
    this.profilService.getProfilById(this.id).subscribe(response => {
      this.loading = false;
      this.profil = response;
      this.profil.permissions.forEach((obj) => {
        this.selectedRolesPermissions.push(obj.id);
        this.selectedNode.push({data: {id: obj.id, name: obj.role, grouped: obj.grouped}});
      });
      this.updateForm(this.profil);
      this.getallPermission();
    });
  }

  private updateForm(profil: Profil): void {

    this.profilForm.patchValue({
      id: profil.id,
      codeMetier: profil.codeMetier,
      description: profil.description
    });
  }

  private updateProfil(): void {
    this.profil.id = this.profilForm.get(['id']).value;
    this.profil.codeMetier = this.profilForm.value.codeMetier;
    this.profil.description = this.profilForm.value.description;
    this.profil.permissions = [];
    this.selectedNode.forEach(node => {
      if (node.data.id !== -1) {
        this.profil.permissions.push(new Permission({id: node.data.id, role: '', description: ''}));
      }
    });

  }

  saveProfil() {
    this.isSaving = true;
    this.updateProfil();
    if (this.profil.id) {
      if (this.profilForm.touched || this.comparer(this.profil.permissions, this.profilPermissions)) {
        this.profilService.updateProfil(this.profil).subscribe(res => {
          this.updateForm(res);
          this.readonly = true;
          this.profilService.getProfilById(this.id).subscribe(response => {
            this.profil = response;
          });
          this.isSaving = false;
          this.msgs = [];
          this.msgs.push({severity: 'success', summary: '', detail: `Le profil ${this.profil.codeMetier}, est enregistré avec succès`});

        }, (error) => {
          this.isSaving = false;
          this.onSaveError(error);
        });
      } else {
        this.isSaving = false;
      }

    } else {
      this.profilService.addProfil(this.profil).subscribe(res => {
        this.selectedNode.forEach(node => {
          if (node.data.id !== -1) {
            this.profil.permissions.push(new Permission({id: node.data.id, role: '', description: ''}));

          }
        });

        this.profil.permissions = this.selectedPermissions;
        this.isSaving = false;
        this.updateForm(res);
        this.readonly = true;
        this.msgs = [];
        this.msgs.push({severity: 'success', summary: '', detail: `Le profil ${this.profil.codeMetier}, est enregistré avec succès`});

      }, (error) => {
        this.isSaving = false;
        this.onSaveError(error);
      });

    }

  }

  private onSaveError(error: any) {
    const errorP = error.headers.get('X-validation-error');
    switch (errorP) {
      case 'error.codeMetierAlreadyUsed':
        this.msgs = [];
        this.msgs.push({severity: 'error', detail: `Code Metier existe déjà`});
        break;
      case 'error.idexists':
        this.msgs = [];
        this.msgs.push({severity: 'error', detail: `un nouveau profil ne peut pas avoir un id`});
        break;
      case 'error.profileDoesNotExists':
        this.msgs = [];
        this.msgs.push({severity: 'error', detail: `Le profil n'existe pas`});
        break;
    }
  }

  public back() {
    window.history.back();
  }

  edit() {
    this.readonly = false;
    this.updateForm(this.profil);
  }

  // Permission grouped
  groupedPermissions(list: Permission[]) {
    this.permissionService.getAllGroupPermission().subscribe(res => {
      res.forEach(p => {
        this.permissionsGrouped.push({groupName: p == null ? 'Autre' : p, permissions: this.getPermissionByGroup(list, p)});
        this.myTableNode.push({data: p, children: []});
      });
      this.myTableNode = this.ConvertToTreeTable(this.permissionsGrouped);
      this.selectGroupForView(this.myTableNode, this.selectedNode);

    });
  }

  getPermissionByGroup(list: Permission[], group: string): Permission[] {
    return list.filter(y => {
      return y.grouped === group;
    });
  }

  ConvertToTreeTable(list: GroupPermission[]): TreeNode[] {
    const treeTable: TreeNode[] = [];
    let nodeChildren: TreeNode[] = [];
    list.forEach((group) => {
      nodeChildren = [];
      group.permissions.forEach(per => {
        nodeChildren.push({data: {id: per.id, name: per.role, grouped: per.grouped}});
      });
      treeTable.push({data: {id: -1, name: group.groupName}, children: nodeChildren});
    });

    return treeTable;
  }

  selectGroupForView(tree: TreeNode[], select: TreeNode[]) {
    tree.forEach(data => {
      const x = select.filter(res => {
        return res.data.grouped === data.data.name;
      }).length;

      if (x === data.children.length) {
        this.selectedNode.push(data);
      }
    });
  }

  // pour tester si la liste des permissions de profil est touched
  comparer(arrayA: Permission[], arrayB: Permission[]) {
    let diff: Permission[] = [];
    arrayA.forEach((permissionA) => {
      diff = arrayB.filter(permissionB => permissionA.id === permissionB.id);
    });

    return diff.length === 0;
  }

  ngAfterViewInit(): void {
    this.cd.detectChanges();
  }

  viewType(rowdata) {
    const isAllChildSelected = this.selectedNode.find(node => {
      return node.data.name === rowdata.name;
    });
    if (isAllChildSelected) {
      return false;
    }
    const ischildSelected = this.selectedNode.find(node => {
      return node.data.grouped === rowdata.name;
    });
    return !!ischildSelected;
  }

  hasAuthorities(authorities, authority): boolean {
   return  authorities.find(auth => {
      return auth.authority === authority;
    });
  }

}

interface GroupPermission {
  groupName: string;
  permissions: Permission[];
}


