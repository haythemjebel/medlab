import {AfterViewInit, Component, Renderer2, ViewChild} from '@angular/core';
import {ScrollPanel} from 'primeng/scrollpanel';
import {TranslateService} from '@ngx-translate/core';
import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements AfterViewInit {
  title = 'web';
 layoutMode = 'slim';
 menuMode = 'light';
 profileMode = 'top';
 topbarMenuActive: boolean;
 overlayMenuActive: boolean;
 staticMenuDesktopInactive: boolean;
 staticMenuMobileActive: boolean;
 menuClick: boolean;
 topbarItemClick: boolean;
 activeTopbarItem: any;
 resetMenu: boolean;
 menuHoverActive: boolean;
 rightPanelActive: boolean;
 rightPanelClick: boolean;
  usermenuActive: boolean;
  usermenuClick: boolean;
  activeProfileItem: any;
  megaMenuActive: boolean;
  megaMenuClick: boolean;
  profil: string;


  @ViewChild('layoutMenuScroller', {static: true}) layoutMenuScrollerViewChild: ScrollPanel;

  constructor(public renderer: Renderer2,
              public translate: TranslateService) {

    // ---------- get Environment Profile --------------------
   this.profil = environment.environment;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.layoutMenuScrollerViewChild.moveBar();
    }, 100);
  }

  onLayoutClick() {
    if (!this.topbarItemClick) {
      this.activeTopbarItem = null;
      this.topbarMenuActive = false;
    }

    if (!this.rightPanelClick) {
      this.rightPanelActive = false;
    }

    if (!this.megaMenuClick) {
      this.megaMenuActive = false;
    }

    if (!this.usermenuClick && this.isSlim()) {
      this.usermenuActive = false;
      this.activeProfileItem = null;
    }

    if (!this.menuClick) {
      if (this.isHorizontal() || this.isSlim()) {
        this.resetMenu = true;
      }

      if (this.overlayMenuActive || this.staticMenuMobileActive) {
        this.hideOverlayMenu();
      }

      this.menuHoverActive = false;
    }

    this.topbarItemClick = false;
    this.menuClick = false;
    this.rightPanelClick = false;
    this.megaMenuClick = false;
    this.usermenuClick = false;

  }

  onMenuButtonClick(event) {
    this.menuClick = true;
    this.topbarMenuActive = false;

    if (this.layoutMode === 'overlay') {
      this.overlayMenuActive = !this.overlayMenuActive;
    } else {
      if (this.isSlim()) {
        this.overlayMenuActive = !this.overlayMenuActive;

        if (this.isDesktop()) {
        }
      }

      if (!this.isDesktop()) {
        this.staticMenuMobileActive = !this.staticMenuMobileActive;
      }


    }

    event.preventDefault();
  }

  onMenuClick($event) {
    this.menuClick = true;
    this.resetMenu = false;
  }

  onTopbarMenuButtonClick(event, disabled: boolean) {
    if (disabled) {
      event.preventDefault();
    }
    this.topbarItemClick = true;
    this.topbarMenuActive = !this.topbarMenuActive;

    this.hideOverlayMenu();

    event.preventDefault();
  }

  onTopbarItemClick(event, item) {
    this.topbarItemClick = true;

    if (this.activeTopbarItem === item) {
      this.activeTopbarItem = null;
    } else {
      this.activeTopbarItem = item;
    }

    event.preventDefault();
  }

  onTopbarSubItemClick(event) {
    event.preventDefault();
  }

  onRightPanelButtonClick(event) {
    this.rightPanelClick = true;
    this.rightPanelActive = !this.rightPanelActive;
    event.preventDefault();
  }

  onRightPanelClick() {
    this.rightPanelClick = true;
  }

  onMegaMenuButtonClick(event) {
    this.megaMenuClick = true;
    this.megaMenuActive = !this.megaMenuActive;
    event.preventDefault();
  }

  onMegaMenuClick() {
    this.megaMenuClick = true;
  }

  hideOverlayMenu() {
    this.overlayMenuActive = false;
    this.staticMenuMobileActive = false;
  }

  isTablet() {
    const width = window.innerWidth;
    return width <= 1024 && width > 640;
  }

  isDesktop() {
    return window.innerWidth > 1024;
  }

  isMobile() {
    return window.innerWidth <= 640;
  }

  isHorizontal() {
    return this.layoutMode === 'horizontal';
  }

  isSlim() {
    return this.layoutMode === 'slim';
  }

  isOverlay() {
    return this.layoutMode === 'overlay';
  }


}

