import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent, MenuItem, MessageService} from 'primeng/api';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ProfilService } from 'src/app/shared/service/profil.service';
import { ProfilCriteria } from 'src/app/shared/model/profilCriteria';
import { Filter } from 'src/app/layouts/filters-panel/filter';
import {PageResponse} from '../../shared/support/paging';
import { PermissionService } from 'src/app/shared/service/permission.service';
import { TranslateService } from '@ngx-translate/core';
import {Profil} from '../../shared/model/profil';
import {PermissionsEnum} from '../../shared/helpers/permissions.enum';
import {User} from '../../shared/model/user';
import {hasAuthorities} from '../../shared/functions/hasAuthorities.function';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.sass']
})
export class ProfilComponent implements OnInit {
  profilFilters: Filter[] ;
  profilColumns: any[];
  rows: number = 11;
  loading: boolean;
  selectedProfil: Profil;
  items: MenuItem[];
  cmItems: MenuItem[];
  profilCriteria: ProfilCriteria = new ProfilCriteria();
  currentPage: PageResponse<Profil> = new PageResponse<Profil>(0, 0, []);
  hasFastSearch: boolean = false;
  currentUser: User;

  constructor(private router: Router, private fb: FormBuilder,
              private profilService: ProfilService, private permissionService: PermissionService,
              public translate: TranslateService, private messageService: MessageService) {

      }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.profilColumns = [
      { field: 'codeMetier', header: 'Nom', width: '150px', type: 'string', sortable: true },
      { field: 'description', header: 'Description', width: '150px', type: 'string', sortable: true },
      { field: 'createdDate', header: 'Date de Création', width: '150px', type: 'datetime', sortable: true },
      { field: 'lastModifiedDate', header: 'Modifié Le', width: '150px', type: 'datetime', sortable: true },
      { field: 'createdBy', header: 'Créé Par', width: '150px', type: 'string', sortable: true },
      { field: 'lastModifiedBy', header: 'Modifié Par', width: '150px', type: 'string', sortable: true }
    ];

    this.cmItems = [
      {label: 'Visualiser', icon: 'pi pi-eye', command: () => {this.navigateIntoShow(this.selectedProfil); } },
      {label: 'Editer', icon: 'pi pi-pencil', command: () => {this.navigateIntoEdit(this.selectedProfil); } }
    ];

    this.profilFilters = [
      {id : 'codeMetier', type : 'string', label: 'Code métier'},
      {id : 'createdBy', type : 'string', label: 'Créé par'},
      {id : 'createdDate', type : 'date', label: 'Créé le'},
      {id : 'description', type : 'string', label: 'Description'},
      {id : 'lastModifiedBy', type : 'string', label: 'Modifié par'},
      {id : 'lastModifiedDate', type : 'date', label: 'Modifié le'},
      {id : 'permissionDescs', type : 'string', label: 'Perm. Desc.'},
      {id : 'permissionRoles', type : 'string', label: 'Perm. rôle'}
    ];

    this.hasFastSearch = this.permissionService.HasPermission('Recherche-Rapide');
  }

  loadProfils(event: LazyLoadEvent) {
    this.loading = true;
    this.profilService.findProfilByCriteria(this.profilCriteria, event).subscribe(response => {
        this.currentPage = response;
        this.loading = false;
      },
      () => {});
  }
  onRowSelect() {
    this.navigateIntoShow(this.selectedProfil);
  }
  navigateIntoShow(profil: Profil) {
    if (hasAuthorities(this.currentUser.authorities, PermissionsEnum.ConsulterProfil)) {
      this.router.navigate(['home/profile/' + profil.id + '/show']);
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('CHAMPS-COMMUN.AUTORISATION-SUMMARY'),
        detail: this.translate.instant('CHAMPS-COMMUN.AUTORISATION')});
    }
  }
  navigateIntoEdit(profil: Profil ) {
    if (hasAuthorities(this.currentUser.authorities, PermissionsEnum.UpdateProfil)) {
      this.router.navigate(['home/profile/' + profil.id + '/edit']);
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: this.translate.instant('CHAMPS-COMMUN.AUTORISATION-SUMMARY'),
        detail: this.translate.instant('CHAMPS-COMMUN.AUTORISATION')
      });
    }
    }


  findProfilBy(event: any) {
    this.profilCriteria = event;
    this.loadProfils({rows: 10, first: 0});
  }
  hasAuthoritieCreateProfil() {
     return hasAuthorities(this.currentUser.authorities, PermissionsEnum.CreateProfil);
  }
}
