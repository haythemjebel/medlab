import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/helpers/auth.guard';
import { PermissionsEnum } from '../../shared/helpers/permissions.enum';
import { HistoryPatientComponent } from './history-patient.component';

export const HistoryRoute: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HistoryPatientComponent,
    canActivate: [AuthGuard],
    data: {
      permission: [
        PermissionsEnum.ConsulterUser
      ]
    }
  }
];
