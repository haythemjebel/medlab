import {Component, OnInit} from '@angular/core';
import {PatientImportConfig} from 'src/app/shared/model/patientimportconfig';
import {PatientImportConfigService} from 'src/app/shared/service/patientimportconfig.service';

@Component({
  selector: 'app-history-patient',
  templateUrl: './history-patient.component.html',
  styleUrls: ['./history-patient.component.sass']
})
export class HistoryPatientComponent implements OnInit {

  customers: PatientImportConfig[];

  statuses: any[];

  loading: boolean = true;

  activityValues: number[] = [0, 100];


  constructor(private PatientImportConfigService:PatientImportConfigService) { }

  ngOnInit() {
    this.PatientImportConfigService.findAllPatientImportConfigs().subscribe(data=>{
      this.customers = data
      this.loading = false;
    })

    this.statuses = [
      {label: 'COMPLETED', value: 'COMPLETED'},
      {label: 'FAILED', value: 'FAILED'}
  ]
  }
  public back() {
    window.history.back();
  }

}
