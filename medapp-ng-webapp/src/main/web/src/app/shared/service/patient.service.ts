import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import {map} from 'rxjs/operators';
import {LazyLoadEvent} from 'primeng/api';
import {PageRequestByCriteria, PageResponse} from '../support/paging';
import {Patient} from '../model/patient';
import { IExcelSheetInfo } from '../support/IExcelSheetInfo';
type EntityResponseType = HttpResponse<IExcelSheetInfo>;
@Injectable({
  providedIn: 'root'
})
export class PatientService {
  url = environment.apiUrl + `/api/patients`;
  urlbatch = environment.apiUrl + `/api/patients/run`;

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
  }

  findAllPatients(): Observable<any> {
    return this.httpClient.get(`${this.url}`);
  }

  findAllRenduvousByIdPatient(PatientId): Observable<any> {
    const url = this.url + `/renduvous/${PatientId}`;
    return this.httpClient.get<Patient>(url);  }

  createPatient(Patient: Patient): Observable<Patient> {

    return this.httpClient.post<Patient>(this.url, Patient, {headers: new HttpHeaders({'X-action': 'create'})});
  }

  updatePatient(Patient: Patient): Observable<Patient> {
    return this.httpClient.put<Patient>(this.url, Patient);
  }


  findOnePatient(PatientId): Observable<Patient> {
    const url = this.url + `/${PatientId}`;
    return this.httpClient.get<Patient>(url);
  }

  deleteOnePatient(PatientId): Observable<Patient> {
    const url = this.url + `/${PatientId}`;
    return this.httpClient.delete<Patient>(url);
  }

  pushFileToStorage(file: File): Observable<EntityResponseType> {
    const formdata: FormData = new FormData();
    
    formdata.append('file', file);
    
    // const req = new HttpRequest('POST', '/api/post', formdata, {
    // reportProgress: true,
    // responseType: 'text'
    // });
    return this.httpClient.post<IExcelSheetInfo>(this.urlbatch, formdata, { observe: 'response' });
    }
}
