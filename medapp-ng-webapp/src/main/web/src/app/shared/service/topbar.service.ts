import {Injectable} from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TopBarService{
    public clientSearch = new BehaviorSubject<string>('');
    clientSearchObservable = this.clientSearch.asObservable();
    globalClientSearch(filters: any): void {
        this.clientSearch.next(filters);
    }
}
