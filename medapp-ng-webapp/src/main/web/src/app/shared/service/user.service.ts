import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../model/user';
import {environment} from 'src/environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import {map} from 'rxjs/operators';
import {LazyLoadEvent} from 'primeng/api';
import {PageRequestByCriteria, PageResponse} from '../support/paging';
import {AppSettings} from '../helpers/constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
  }

  create(user: User): Observable<User> {
    const url = environment.apiUrl + `/api/users`;
    return this.httpClient.post<User>(url, user, {headers: new HttpHeaders({'X-action': 'create'})});
  }

  update(user: User): Observable<User> {
    const url = environment.apiUrl + `/api/users`;
    return this.httpClient.put<User>(url, user);
  }


  findOne(userId): Observable<User> {
    const url = environment.apiUrl + `/api/users/${userId}`;
    return this.httpClient.get<User>(url);
  }



  downloadUserPicture(resourceUri: string): Observable<any> {
    return this.httpClient.get(environment.apiUrl + resourceUri, {
      observe: 'response' as 'response',
      responseType: 'blob'
    }).pipe(
      map(response => {
          const blob = new Blob([response.body], {type: 'application/octet-stream'});
          return this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(blob)));
        },
        () => {
          return '../../../assets/layout/images/anonymous-user.png';
        })
    );
  }

  findUserByCriteria(userCriteria: User, event: LazyLoadEvent): Observable<PageResponse<User>> {
    const body = new PageRequestByCriteria(userCriteria, event);
    return this.httpClient.post<PageResponse<any>>(environment.apiUrl + '/api/users', body, {headers: new HttpHeaders({'X-action': 'search'})})
      .pipe(
        map(pr => {
            return new PageResponse<User>(pr.totalPages, pr.totalElements, pr.content);
          },
          () => { /*hadle error*/
          }
        ));
  }

  savePassword(newPassword: string, currentPassword: string): Observable<any> {
    return this.httpClient.post(environment.apiUrl + '/api/account/change-password', {
      currentPassword,
      newPassword
    });
  }

  resetPassword(id: number) {
    return this.httpClient.put<User>(environment.apiUrl + AppSettings.API_ENDPOINT + `users/${id}/pwd` , null);
  }

  sendEmails(selectedUsers: User[]) {
    return this.httpClient.post(environment.apiUrl + '/api/users/sendmailtousers', selectedUsers);
  }
}
