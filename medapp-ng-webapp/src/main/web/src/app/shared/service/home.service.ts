import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor() {
  }



  private handleError(error: HttpErrorResponse) {
    // TODO: seems we cannot use messageService from here...
    const errMsg = (error.message) ? error.message : 'Server error';
    console.error(errMsg);
    if (error.status === 401) {
      window.location.href = '/';
    }
    return Observable.throw(errMsg);
  }

}
