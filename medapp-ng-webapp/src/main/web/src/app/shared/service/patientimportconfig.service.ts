import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import { RendezVous } from '../model/rendez_vous';
@Injectable({
  providedIn: 'root'
})
export class PatientImportConfigService {
  url = environment.apiUrl + `/api/ImportConfigs`;

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
  }

  findAllPatientImportConfigs(): Observable<any> {
    return this.httpClient.get(`${this.url}`);
  }



}
