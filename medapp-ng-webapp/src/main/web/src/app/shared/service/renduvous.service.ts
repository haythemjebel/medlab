import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import { RendezVous } from '../model/rendez_vous';
@Injectable({
  providedIn: 'root'
})
export class RenduVousService {
  url = environment.apiUrl + `/api/rendezVous`;

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
  }

  findAllRenduvousByIdPatient(PatientId): Observable<any> {
    const url = this.url + `/${PatientId}`;
    return this.httpClient.get<RendezVous>(url);  }



}
