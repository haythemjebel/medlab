import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { map } from 'rxjs/operators';
import {User} from '../model/user';
import {environment} from '../../../environments/environment';
import {LegacySettings} from '../helpers/constants';


@Injectable({ providedIn: 'root' })
export class AuthentificationService {
  constructor(private http: HttpClient) {
  }

  login(username: string, password: string) {
    const fd = new FormData();

    fd.append('username', username);
    fd.append('password', password);
    let body = new HttpParams();
    body = body.set('username', username);
    body = body.set('password', password);
    return this.http.post<any>(environment.apiUrl + '/api/authenticate', body , {observe: 'response'})
      .pipe(
        map((response: HttpResponse<User>) => {
            return response;
        }
        )
      );
  }

  loginFromLegacy(identifiant: string, legacyToken: string) {
    const url = environment.apiUrl  + '/api/legacy?TOKEN=' + legacyToken + '&identifiant=' + identifiant;
    return this.http.get<any>(url, {observe: 'response'}).pipe(
      map((response: HttpResponse<User>) => {
          return response;
        }
      )
    );
  }


  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentCompagnie');
    localStorage.removeItem('token');
    localStorage.removeItem('region');
    localStorage.removeItem(('portefeuille'));
    localStorage.removeItem('projects');
    sessionStorage.removeItem('appId');
    sessionStorage.removeItem('authKey');
    sessionStorage.removeItem('authSecret');
    sessionStorage.removeItem('loginDetails');
    sessionStorage.removeItem('sessionDetails');
  }
}
