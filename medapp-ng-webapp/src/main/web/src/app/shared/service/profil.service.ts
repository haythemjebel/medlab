import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {PageRequestByCriteria, PageResponse} from '../support/paging';
import {catchError, map} from 'rxjs/operators';
import {Profil} from '../model/profil';
import {Observable} from 'rxjs';
import {LazyLoadEvent} from 'primeng/api';
import {ProfilCriteria} from '../model/profilCriteria';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  url = environment.apiUrl + '/api';

  constructor(private httpClient: HttpClient) {}

  findProfilByCriteria(profileCriteria: ProfilCriteria, event: LazyLoadEvent): Observable<PageResponse<Profil>> {
    const body = new PageRequestByCriteria(profileCriteria, event);
    return this.httpClient.post<PageResponse<any>>(this.url + '/profiles', body, { headers : new HttpHeaders({'X-action': 'search'})}).pipe(
      map(pr => new PageResponse<Profil>(pr.totalPages, pr.totalElements, pr.content),
          catchError(this.handleError.bind(this))
        )
    );
  }

  getProfilById(id) {
    const url = this.url + '/profiles/' + id;
    return this.httpClient.get<Profil>(url);
  }

  addProfil(profile: Profil) {
    const url = this.url + '/profiles';
    return this.httpClient.post<Profil>(url, profile, { headers : new HttpHeaders({'X-action': 'create'})});
  }

  updateProfil(profile: Profil): Observable<Profil>  {
    const url = this.url + `/profiles`;
    return this.httpClient.put<Profil>(url , profile);
  }


  private handleError(error: HttpErrorResponse) {
    // TODO: seems we cannot use messageService from here...
    const  errMsg = (error.message) ? error.message : 'Server error';
    console.error(errMsg);
    if (error.status === 401) {
      window.location.href = '/';
    }
    return Observable.throw(errMsg);
  }


}
