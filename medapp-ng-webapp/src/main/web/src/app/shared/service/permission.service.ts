import { Injectable } from '@angular/core';
import { PageResponse, PageRequestByCriteria } from '../support/paging';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Permission } from '../model/permission';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  url = environment.apiUrl + '/api';

  constructor(private httpClient: HttpClient) { }

  getAllPermission( event) {
    const body = new PageRequestByCriteria(null, event);
    return this.httpClient.post<PageResponse<Permission>>(this.url + '/permissions', body, { headers : new HttpHeaders({'X-action': 'search'})});
  }

  getAllGroupPermission() {
    const url = this.url + '/permissionsgroup';
    return this.httpClient.get<string[]>(url);
  }

  HasPermission(permission: string): boolean {
    const currentUser = JSON.parse(localStorage.getItem('currentUser')) ;
    for (const auth of currentUser.authorities) {
      if (auth.authority === permission) {
        return true;
      }
    }
    return false;

  }

}
