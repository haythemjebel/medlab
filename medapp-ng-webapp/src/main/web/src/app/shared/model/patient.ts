export class Patient {
  id: number;
  nom: string;
  prenom: string;
  email: string;
  ficheMedicaleNumber: string;
  adressePar: string;
  cnamNum: number;
  profession: string;
  adresse: string;
  tel: string;
  dateNaissance: Date;
  atcds: string;
  diagnostic: string;


  constructor(json?: any) {
    if (json != null) {
      this.id = json.id;
      this.nom = json.nom;
      this.prenom = json.prenom;
      this.email = json.email;
      this.ficheMedicaleNumber = json.ficheMedicaleNumber;
      this.adressePar = json.adressePar;
      this.cnamNum = json.cnamNum;
      this.tel = json.tel;
      this.dateNaissance = json.dateNaissance;
      this.atcds = json.atcds;
      this.diagnostic = json.diagnostic;
    }
  }
}
