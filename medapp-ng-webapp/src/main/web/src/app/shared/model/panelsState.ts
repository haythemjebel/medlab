// donner l'etats de tous les panels dans le composant client
export class PanelsState {
  filterPanelIsOpened: boolean;
  cockpitPanelIsOpened: boolean;
  listClientIsOpened: boolean;

  PanelsState(filterPanelIsOpened, cockpitPanelIsOpened, listClientPanelIsOpened) {
    this.filterPanelIsOpened = filterPanelIsOpened;
    this.cockpitPanelIsOpened = cockpitPanelIsOpened;
    this.listClientIsOpened = listClientPanelIsOpened;
  }


}
