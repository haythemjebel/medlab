import { User } from './user';
import { Permission } from './permission';


export class Profil {
    id: number;
    codeMetier: string;
    description: string;
    permissions: Permission[];
    users: User[];

    constructor(json?: any) {
      if (json != null) {
        this.id = json.id;
        this.codeMetier = json.codeMetier;
        this.description = json.description;
        this.permissions = json.permissions;
               this.users = json.users;
      }
    }
}
