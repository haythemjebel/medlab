export class User {
  id: number;
  username: string;
  nom: string;
  prenom: string;
  email: string;
  activated: boolean;
  nonLocked: boolean;
  profile;
  authorities: any[];
  identifiant: string;



  constructor(json?: any ) {
    if (json != null) {
      this.username = json.username;
      this.id = json.id;
      this.nom = json.nom;
      this.prenom = json.prenom;
      this.activated = json.activated;
      this.email = json.email;
      this.profile = json.profile;
      this.authorities = json.authorities;
      this.identifiant = json.identifiant;
    }
  }
}

