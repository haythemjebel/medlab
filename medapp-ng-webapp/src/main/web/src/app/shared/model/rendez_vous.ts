export class RendezVous {
  id: number;
  dateRendezVous: Date;
  typeRendezVous: string;
  dureeRendezVous: number;
  diagnosticAndTraittement: Text;


  constructor(json?: any) {
    if (json != null) {
      this.id = json.id;
      this.dateRendezVous = json.dateRendezVous;
      this.typeRendezVous = json.typeRendezVous;
      this.dureeRendezVous = json.diagnosticAndTraittement;
      this.diagnosticAndTraittement = json.diagnosticAndTraittement;

    }
  }
}
