export class ProfilCriteria {
    codeMetier: string;
    createdBy: string;
    createdDate: Date;
    description: string;
    lastModifiedBy: string;
    lastModifiedDate: Date;
    permissionDescs: string;
    permissionRoles: string;
}
