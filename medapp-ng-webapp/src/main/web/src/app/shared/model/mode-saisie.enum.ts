export enum ModeSaisieEnum {
  CREATE = 'create' ,
  UPDATE = 'update',
  DELETE = 'delete',
}
