export class PatientImportConfig {
    id: number;
    dateImport: Date;
    urlFile: string;
    status: string;
    nom: string;
  
  
    constructor(json?: any) {
      if (json != null) {
        this.id = json.id;
        this.dateImport = json.dateImport;
        this.urlFile = json.urlFile;
        this.status = json.status;
        this.nom = json.nom;
  
      }
    }
  }
  