import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor() { }

  // function which will be called for all http calls
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    if (localStorage.getItem('token')) {
      if (this.verifyHeaders(request.headers)) {
       request = request.clone({

       headers: this.addExtraHeaders(request.headers)
       });
     }
  }

    return next.handle(request);

  }


  private addExtraHeaders(headers: HttpHeaders): HttpHeaders {

    headers = headers.append('Authorization', localStorage.getItem('token'));
    return headers;
  }

  private verifyHeaders(headers: HttpHeaders): boolean {
    return headers.get('qb-token') == null;
  }
}

