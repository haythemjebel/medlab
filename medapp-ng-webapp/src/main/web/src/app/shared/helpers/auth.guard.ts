import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AuthentificationService} from '../service/authentification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    {
      const currentUser = localStorage.getItem('currentUser');
      const token = localStorage.getItem('currentUser');

      if (currentUser) {

        const user = JSON.parse(localStorage.getItem('currentUser'));
        // check if route is restricted by role

        const auth = user.authorities.find( authorisation => this.hasPermission(authorisation.authority, route.data.permission));

        if (route.data.permission && auth === undefined  ) {
          // role not authorised so redirect to home page
          this.router.navigate(['/access-denied']);
          return false;
        }
        // logged in so return true
        return true;
      }

      // not logged in so redirect to login page with the return url
      this.router.navigate(['login']);
      return false;
    }
  }

  hasPermission(authority:string,permission:any[]){
    if(permission){
      return permission.filter(data=>{
        return data == authority
      }).length > 0
    }
    
  }
}
