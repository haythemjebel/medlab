// tous les autorisations possible d'un utilisateur
export enum PermissionsEnum {
  ActiverUser = 'Activer/Desactiver-Utilisateur',
  ConfigureCategorie = 'Configurer-Catégories',
  ConfigureReport = 'Configurer-Rapport',
  ConnecterWithLogin = 'Connecter-Avec-Login',
  ConsulterFicheClient = 'Consulter-Fiche-Client',
  ConsulterPiece = 'Consulter-Piece',
  ConsulterProfil = 'Consulter-Profil',
  ConsulterSousTypo = 'Consulter-Sous-Typologies',
  ConsulterReports = 'Consulter-Tableau-Rapports',
  ConsulterUser = 'Consulter-Utilisateur',
  CreateProfil = 'Créer-Profil',
  CreateQualif = 'Créer-Qualification',
  CreateRegroupement = 'Créer-Regroupement',
  CreateTypeProject = 'Créer-Type-Projet',
  CreateUser = 'Créer-Utilisateur',
  ExporterPiece = 'Exporter-Piece',
  BlackListClient = 'Liste-Noire-Client',
  BlackListPiece = 'Liste-Noire-Piece',
  UpdateComment = 'Modifier-Commentaire',
  UpdatePersonelInformation = 'Modifier-Informations-Personnelles',
  UpdateUser = 'Modifier-Utilisateur',
  UpdateProfil = 'Modifier/Supprimer-Profil',
  UpdateTypeProject = 'Modifier/Supprimer-Type-Projet',
  RechercheRapide = 'Recherche-Rapide',
  SearchClient = 'Rechercher-Client',
  SearchPiece = 'Rechercher-Piece',
  SearchUser = 'Rechercher-Utilisateur',
  CreateContact = 'Créer',
  UpdateContact = 'Modifier',
  DeleteContact = 'Supprimer',
  ConsulterFactureSerensia = 'Consulter-Factures-Serensia',
  VisualiserCourriers= 'Visualiser-Courriers',
  ManipulateModeReg= 'Gérer-Modes-Règlement',
  ManageMemoHercule= 'Gérer-Mémo-Hercule',
  GererPiecesJointes= 'Gérer-Pièces-Jointes',
  RegleIM = 'Gérer-Règle-Interet-Moratoire',
  GererMemoClient= 'Gérer-Mémo-Client',
  GererProvision = 'Gérer-Provision',
  ResetPassword = 'Réinitialiser-Mot-Passe',
  PermissionChat = 'Permission_chat',
  PermissionVisualiserComp = 'Permission_Visualiser_Compagnie',
  PermissionModifierComp = 'Permission_Editer_Compagnie',
  PermissionModifierNBLicen = 'Permission_Modifier_NbLicence',
  PermissionAdd_Compagnie_User = 'Permission_Ajouter_Compagnie_User',
  PermissionAdd_Contact = 'Permission_Ajouter_Contact',
  PermissionUpdate_ParametreQuick = 'Permission_Modifier_ParametreQuick',
  PermissionGestion_stock_lunette_intelligente = 'Permission_Gestion_stock_lunette_intelligente',
  SendEmails = 'Envoyer-Emails'
}
