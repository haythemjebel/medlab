export enum ModesBalanceEnum {
  Destinateur = 'destinataire',
  Siren = 'siren',
  Compte416 = 'comptes416',
  Compte416Destinataire = 'comptes416dest',
  Totale = 'totale'
}
