export class Column {
  field: string;
  header: string;
  width: string;
  type: string;
  sortable: boolean;
  tooltip: string;

}

