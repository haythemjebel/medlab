export class AppSettings {
  public static API_ENDPOINT = '/api/';
}


export class PieceJointeSettings {
  public static PJ_MAX_SIZE = 50000000;
}

export class LegacySettings {
  public static TOKEN = 'YWhhbWlsdG9uQGFwaWdlZS5jb206bXlwYXNzdzByZAo';
}
