export interface IExcelSheetInfo {
    nom?: string;
    fileUrl?: string;
   }
    
   export class ExcelSheetInfo implements IExcelSheetInfo {
    constructor(public nom?: string, public fileUrl?: string) {}
   }