import {
  Directive, ChangeDetectorRef, EventEmitter, ElementRef,
  Input, NgZone, OnChanges, OnDestroy, Optional, Output, SkipSelf, OnInit
} from '@angular/core';

@Directive({
  selector: '[appDisabled]'
})
export class DisabledDirective implements OnInit, OnChanges {
  @Output() disabledChange: EventEmitter<boolean> = new EventEmitter<boolean>(false);
  @Input() appDisabled: boolean = false;
  @Input() disabledStopPropagation: boolean = false;

  disabled: boolean = false;
  private element: HTMLElement;

  constructor(
    private elementRef: ElementRef,
    @SkipSelf() @Optional() private optParent: DisabledDirective,
    @Optional() private changeDetector: ChangeDetectorRef,
    @Optional() private zone: NgZone,
  ) {
    this.disabledChange = new EventEmitter<boolean>(false);

    if (optParent) {
      optParent.onChange(this, () => this.checkForChanges());
    }
  }

  ngOnInit() {
    this.element = this.elementRef.nativeElement;
  }

  private checkForChanges() {
    setTimeout(() => {
      let newValue = false;

      if (this.disabledStopPropagation || !this.optParent) {
        newValue = !!this.appDisabled;
      } else {
        newValue = !!this.appDisabled || this.optParent.disabled;
      }

      if (this.zone && newValue !== this.disabled) {
        this.zone.run(() => {
          if (this.changeDetector) {
            this.changeDetector.markForCheck();
          }
          this.disabled = newValue;
          this.disabledChange.emit(newValue);
        });
      }
    }, 0);
  }

  ngOnChanges() {
    this.checkForChanges();
  }

  /**
   * Alerts the callback when the disabled state changes
   */
  onChange(directive: DisabledDirective, callback: (opt?: boolean) => void) {
    const result = this.disabledChange.subscribe(callback);
  }
}

const defaultDisabled = new DisabledDirective(null, null, null, null);
export function resolve(optDisabled?: DisabledDirective): DisabledDirective {
  return optDisabled || defaultDisabled;
}

const DISABLED_OPACITY = 0.3;

@Directive({
  selector: '[appDefaultDisabled]',
  host: {
    '[style.opacity]': 'disabled ? ' + DISABLED_OPACITY + ' : undefined',
    '[style.pointerEvents]': 'disabled ? "none" : undefined',
  },
})
export class DefaultDisabledStateDirective {
  disabledDirective: DisabledDirective;
  get disabled(): boolean {
    return this.disabledDirective.disabled;
  }

  constructor(@Optional() optDisabled: DisabledDirective, changeDetector: ChangeDetectorRef) {
    this.disabledDirective = resolve(optDisabled);
  }
}
