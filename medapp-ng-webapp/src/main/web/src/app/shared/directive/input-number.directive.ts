import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[inputNumber]'
})
export class InputNumberDirective {
  private regex: RegExp = new RegExp(/^-?\d*\,?\d{0,2}$/g);
  private specialKeys: Array<string> = ['Backspace', 'End', 'Tab', 'Home', 'ArrowLeft', 'ArrowRight', 'Del', 'Delete'];
 
  constructor(private _el: ElementRef) { }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    let current: string = this._el.nativeElement.value;
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    } 
    const position = this._el.nativeElement.selectionStart;
    const next: string = 
    [current.slice(0, position), event.key == 'Decimal' ? ',' : event.key, current.slice(position+1)].join('');
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }
}
