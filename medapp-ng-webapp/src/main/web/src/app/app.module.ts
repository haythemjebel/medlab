import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_INITIALIZER, Injector, LOCALE_ID, NgModule} from '@angular/core';
import {FilterService, MessageService, PrimeNGConfig, SharedModule, TreeDragDropService} from 'primeng/api';
import {AppRoutes} from './app.routes';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SliderModule} from 'primeng/slider';
import {TimelineModule} from 'primeng/timeline';
import {BreadcrumbService} from './layouts/breadcrumb/breadcrumb.service';
import {DropdownModule} from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {VirtualScrollerModule} from 'primeng/virtualscroller';
import {DataViewModule} from 'primeng/dataview';
import {MultiSelectModule} from 'primeng/multiselect';
import {PanelModule} from 'primeng/panel';
import {CalendarModule} from 'primeng/calendar';
import {TriStateCheckboxModule} from 'primeng/tristatecheckbox';
import {CardModule} from 'primeng/card';
import {FieldsetModule} from 'primeng/fieldset';
import {InputTextModule} from 'primeng/inputtext';
import {ChipsModule} from 'primeng/chips';
import {AppComponent} from './app.component';
import {TopbarComponent} from './layouts/topbar/topbar.component';
import {AppMenuComponent, AppSubMenuComponent} from './layouts/menu/menu.component';
import {AppProfileComponent} from './app.profile.component';
import {MainComponent} from './layouts/main/main.component';
import {BreadcrumbComponent} from './layouts/breadcrumb/breadcrumb.component';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputMaskModule} from 'primeng/inputmask';
import {SpinnerModule} from 'primeng/spinner';
import {SidebarModule} from 'primeng/sidebar';


import {DragDropModule} from 'primeng/dragdrop';
import {FileUploadModule} from 'primeng/fileupload';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ListboxModule} from 'primeng/listbox';
import {MenuModule} from 'primeng/menu';
import {MessageModule} from 'primeng/message';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {SelectButtonModule} from 'primeng/selectbutton';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {CheckboxModule} from 'primeng/checkbox';
import {ContextMenuModule} from 'primeng/contextmenu';
import {InplaceModule} from 'primeng/inplace';
import {HashLocationStrategy, LOCATION_INITIALIZED, LocationStrategy, registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {TooltipModule} from 'primeng/tooltip';
import {LoginComponent} from './connexion/login/login.component';
import {HomeComponent} from './home/home.component';
import {BlockUIModule} from 'primeng/blockui';
import {JwtInterceptor} from './shared/helpers/jwt.interceptor';

import {BlockableDivComponent} from './layouts/blockable-div/blockable-div.component';
import {ToastModule} from 'primeng/toast';
import {InputNumberDirective} from './shared/directive/input-number.directive';
import {ClickOutsideDirective} from './shared/directive/click-outside.directive';
import {UppercaseDirective} from './shared/directive/uppercase.directive';

import {ProfilComponent} from './home/profil/profil.component';
import {ProfilDetailsComponent} from './home/profil-details/profil-details.component';
import {TabViewModule} from 'primeng/tabview';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {SecurePipe} from './shared/helpers/secure.pipe';
import {UserManagementComponent} from './home/user-management/user-management.component';
import {UserManagementUpdateComponent} from './home/user-management/user-management-update/user-management-update.component';
import {MessagesModule} from 'primeng/messages';
import {FiltersPanelComponent} from './layouts/filters-panel/filters-panel.component';
import {UserProfileComponent} from './home/user-management/user-profile/user-profile.component';
import {TreeModule} from 'primeng/tree';
import {RadioButtonModule} from 'primeng/radiobutton';
import {AccessDeniedComponent} from './access-denied/access-denied.component';
import {TreeTableModule} from 'primeng/treetable';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {EditableComponent} from './layouts/editable/editable.component';
import {ViewModeDirective} from './shared/directive/view-mode.directive';
import {EditModeDirective} from './shared/directive/edit-mode.directive';
import {EditableOnEnterDirective} from './shared/directive/editable-on-enter.directive';
import {DefaultDisabledStateDirective, DisabledDirective} from './shared/directive/disabled.directive';
import {FadeComponent} from './home/fade/fade.component';
import {ActionsButtonsComponent} from './layouts/actions-buttons/actions-buttons.component';
import {ErrorComponent} from './error/error.component';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {PatientManagmentComponent} from './home/patient-managment/patient-managment.component';
import {PatientManagmentUpdateComponent} from './home/patient-managment/patient-managment-update/patient-managment-update.component';
import {DashboardComponent} from './home/dashboard/dashboard.component';
import {HistoryPatientComponent} from './home/history-patient/history-patient.component';
import {AccordionModule} from 'primeng/accordion'; //accordion and accordion tab
import {ToolbarModule} from 'primeng/toolbar';
import {RatingModule} from 'primeng/rating';
import {InputNumberModule} from 'primeng/inputnumber';

registerLocaleData(localeFr);

@NgModule({
  declarations: [

    AppComponent,
    TopbarComponent,
    AppMenuComponent,
    AppSubMenuComponent,
    AppProfileComponent,
    MainComponent,
    BreadcrumbComponent,
    LoginComponent,
    HomeComponent,
    InputNumberDirective,
    ClickOutsideDirective,
    UppercaseDirective,
    BlockableDivComponent,
    SecurePipe,
    UserManagementComponent,
    UserManagementUpdateComponent,
    ProfilComponent,
    ProfilDetailsComponent,
    FiltersPanelComponent,
    UserProfileComponent,
    AccessDeniedComponent,
    EditableComponent,
    ViewModeDirective,
    EditModeDirective,
    EditableOnEnterDirective,
    DisabledDirective,
    DefaultDisabledStateDirective,
    FadeComponent,
    ActionsButtonsComponent,
    ErrorComponent,
    PatientManagmentComponent,
    PatientManagmentUpdateComponent,DashboardComponent, HistoryPatientComponent
    ,
  ],
  imports: [

    MenuModule,
  SharedModule,
    AccordionModule,
    BrowserModule,
    ToolbarModule,
    InputMaskModule,
    InputNumberModule,
    BrowserAnimationsModule,
    DropdownModule,
    HttpClientModule,
    RatingModule,
    SidebarModule,
    AppRoutes,
    TimelineModule,
    TooltipModule,
    SliderModule,
    ScrollPanelModule,
    TableModule,
    VirtualScrollerModule,
    FormsModule,
    DataViewModule,
    MultiSelectModule,
    PanelModule,
    CalendarModule,
    FieldsetModule,
    SpinnerModule,
    TriStateCheckboxModule,
    ContextMenuModule,

    ToggleButtonModule,
    SelectButtonModule,
    CardModule,
    InputTextModule,
    TooltipModule,
    ReactiveFormsModule,
    MessageModule,
    ToastModule,
    ChipsModule,
    BlockUIModule,
    CheckboxModule,
    MessagesModule,
    FileUploadModule,
    CheckboxModule,
    TabViewModule,
    AutoCompleteModule,
    TreeModule,
    RadioButtonModule,
    DragDropModule,
    ListboxModule,
    TreeTableModule,
    InputSwitchModule,
    DialogModule,
    ConfirmDialogModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    InputTextareaModule,
    OverlayPanelModule,
    InplaceModule,
    ProgressSpinnerModule,
    DynamicDialogModule,
  ],
  providers: [
    // our application services

PrimeNGConfig,
    BreadcrumbService,
    MessageService,
FilterService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
     TreeDragDropService,

    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [TranslateService, Injector],
      multi: true
    }

  ],
  bootstrap: [AppComponent],
  entryComponents: [

  ],
  exports: []
})

export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


export function appInitializerFactory(translate: TranslateService, injector: Injector) {
  return () => new Promise<any>((resolve: any) => {
    const locationInitialized = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
    locationInitialized.then(() => {
      const langToSet = 'fr';
      translate.setDefaultLang('fr');
      translate.use(langToSet).subscribe(() => {
        console.info(`Successfully initialized '${langToSet}' language.'`);
      }, err => {
        console.error(`Problem with '${langToSet}' language initialization.'`);
      }, () => {
        resolve(null);
      });
    });
  });
}

