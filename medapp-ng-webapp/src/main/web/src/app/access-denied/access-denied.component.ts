import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.sass']
})
export class AccessDeniedComponent implements OnInit {

  constructor(public translate: TranslateService) {
    translate.setDefaultLang('fr'); // fr est le nom du fichier json dans le dossier i18n
  }

  ngOnInit() {
  }

}
