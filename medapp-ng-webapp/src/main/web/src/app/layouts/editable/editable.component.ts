import {Component, ContentChild, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
import {ViewModeDirective} from '../../shared/directive/view-mode.directive';
import {EditModeDirective} from '../../shared/directive/edit-mode.directive';
import {fromEvent, Subject} from 'rxjs';
import {filter, switchMapTo, take} from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
@Component({
  selector: 'app-editable',
  templateUrl: './editable.component.html',
  styleUrls: ['./editable.component.sass']
})
export class EditableComponent implements OnInit {

  //  an update output which is emitted whenever an update has occurred.
  @Output() update = new EventEmitter();

  // We’re using the ContentChild decorator to obtain a reference to the ViewMode and EditMode directives
  @ContentChild(ViewModeDirective, { static: false }) viewModeTpl: ViewModeDirective;
  @ContentChild(EditModeDirective,  { static: false }) editModeTpl: EditModeDirective;

  mode: 'view' | 'edit' = 'view';

  editMode = new Subject();
  editMode$ = this.editMode.asObservable();


  constructor(private host: ElementRef ) { }


  ngOnInit() {
    this.viewModeHandler();
    this.editModeHandler();
  }

  private get element() {
    return this.host.nativeElement;
  }

  toViewMode() {
    this.update.next();
    this.mode = 'view';
  }

  private viewModeHandler() {
    fromEvent(this.element, 'dblclick').pipe(
      untilDestroyed(this)
    ).subscribe(() => {
      // change the mode property to edit, which causes the edit view to be rendered
      this.mode = 'edit';
      // create a subject that fires when we change the edit mode.
      this.editMode.next(true);
    });
  }

  private editModeHandler() {
    //  create an observable that fires when we click outside the current element.
    // We subscribe to the editMode$ observable. When this observable emits
    // it means that we’ve entered edit mode and we should activate the clickOutside$ observable.
    const clickOutside$ = fromEvent(document, 'click').pipe(
      filter(({ target }) => this.element.contains(target) === false),
      take(1)
    );

    // When the clickOutside$ observable fires, we emit the update event and switch back to view mode.
    this.editMode$.pipe(
      switchMapTo(clickOutside$),
      untilDestroyed(this)
    ).subscribe(event => {
      this.update.next();
      this.mode = 'view';
    });
  }

  get currentView() {
    // based on the mode property (which has a default value of view), we decide which template should be rendered by the ngTemplateOutlet directive.
    return this.mode === 'view' ? this.viewModeTpl.tpl : this.editModeTpl.tpl;
  }
  ngOnDestroy() {
  }



}
