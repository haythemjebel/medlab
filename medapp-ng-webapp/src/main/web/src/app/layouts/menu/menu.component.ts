import {ChangeDetectorRef, Component, Input, OnInit, Optional} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MenuItem} from 'primeng/api';
import {HomeComponent} from '../../home/home.component';
import {PermissionService} from 'src/app/shared/service/permission.service';
import {DisabledDirective} from '../../shared/directive/disabled.directive';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-menu',
  template: `
    <ul app-submenu [item]="model" root="true" class="layout-menu"
        [reset]="reset" visible="true" parentActive="true"></ul>

  `
})
export class AppMenuComponent implements OnInit {

    @Input() reset: boolean;
    model: Model[] = [];
    currentUser: any;
    currentCompagnie: any;
    listOfAuthorise: any[] = [];
    UserProfil;
    haslience : boolean

    constructor(public app: HomeComponent, private permissionService: PermissionService,
                changeDetector: ChangeDetectorRef, @Optional() optDisabled: DisabledDirective,
                 public translate: TranslateService) {
                    this.currentCompagnie = localStorage.getItem('currentCompagnie');

    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentCompagnie = localStorage.getItem('currentCompagnie');
        this.listOfAuthorise = this.currentUser.authorities;
        this.UserProfil = this.currentUser.idProfile;



      if (this.menuAuthorise('Créer-Utilisateur') || this.menuAuthorise('Consulter-Sous-Typologies') || this.menuAuthorise('Configurer-Rapport') || this.menuAuthorise('Modifier-Utilisateur') || this.menuAuthorise('Créer-Profil') || this.menuAuthorise('Modifier/Supprimer-Profil') || this.menuAuthorise('Modifier/Supprimer-Type-Projet') || this.menuAuthorise('Créer-Type-Projet')) {
        this.model.push({
          label: this.translate.instant('MENU.ADMINISTRATION'),
          tooltip: this.translate.instant('MENU.ADMINISTRATION'),
          icon: 'fa fa-fw fa-key',
          items: []
        });

        if (this.menuAuthorise('Créer-Utilisateur') || this.menuAuthorise('Modifier-Utilisateur')) {
          this.model[0].items.push({
            label: this.translate.instant('MENU.ADMINISTRATION-UTILISATEUR'),
            tooltip: this.translate.instant('MENU.ADMINISTRATION-UTILISATEUR'),
            icon: 'pi pi-user-plus',
            routerLink: ['/home/user-management']
          });
        }
        if (this.menuAuthorise('Créer-Profil') || this.menuAuthorise('Modifier/Supprimer-Profil')) {
          this.model[0].items.push({
            label: this.translate.instant('MENU.ADMINISTRATION-RROFIL'),
            tooltip: this.translate.instant('MENU.ADMINISTRATION-RROFIL'),
            icon: 'pi pi-th-large',
            routerLink: ['/home/profile']
          });
        }
      }
      if (this.menuAuthorise('Créer-Utilisateur')) {
        this.model.push({
          label: this.translate.instant('MENU.DASHBOARD-PATIENTS'),
          tooltip: this.translate.instant('MENU.DASHBOARD-PATIENTS'),
          icon: 'pi pi-chart-bar',
          routerLink: ['/home/dashboard']
        });
      }
      if (this.menuAuthorise('Créer-Utilisateur')) {
        this.model.push({
          label: this.translate.instant('MENU.ADMINISTRATION-PATIENTS'),
          tooltip: this.translate.instant('MENU.ADMINISTRATION-PATIENTS'),
          icon: 'fa fa-fw fa-address-card',
          routerLink: ['/home/patient-management']
        });
      }
      if (this.menuAuthorise('Créer-Utilisateur')) {
        this.model.push({
          label: this.translate.instant('MENU.HISTORY-PATIENTS'),
          tooltip: this.translate.instant('MENU.HISTORY-PATIENTS'),
          icon: 'pi pi-clone',
          routerLink: ['/home/history-patient']
        });
      }




    }

  menuAuthorise(auth: string) {
    const exist = this.listOfAuthorise.find(authorisation => {
      return authorisation.authority === auth;
    });
    return exist ? true : false;
  }

    changeTheme(theme: string, scheme: string) {
        const layoutLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('layout-css');
        layoutLink.href = 'assets/layout/css/layout-' + theme + '.css';

        const themeLink: HTMLLinkElement = <HTMLLinkElement> document.getElementById('theme-css');
        themeLink.href = 'assets/theme/theme-' + theme + '.css';

        this.app.menuMode = scheme;
    }

}

export class Model {
    label?: string;
    icon?: string;
    routerLink?: string[];
    items?: Model[];
    tooltip?: string;

}

@Component({
    /* tslint:disable:component-selector */
    selector: '[app-submenu]',
    /* tslint:enable:component-selector */
    template: `
        <ng-template ngFor let-child let-i="index" [ngForOf]="(root ? item : item.items)">
            <li  [ngClass]="{'active-menuitem': isActive(i)}" [class]="child.badgeStyleClass" *ngIf="child.visible === false ? false : true">
                <a  [href]="child.url||'#'" (click)="itemClick($event,child,i)" (mouseenter)="onMouseEnter(i)"
                   class="ripplelink" *ngIf="!child.routerLink"
                   [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target">
                    <i [ngClass]="child.icon" [pTooltip]="child.tooltip"></i><span>{{child.label}}</span>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                    <i class="fa fa-fw fa-angle-down layout-menuitem-toggler" *ngIf="child.items"></i>
                </a>
                <a  (click)="itemClick($event,child,i)" (mouseenter)="onMouseEnter(i)" class="ripplelink" *ngIf="child.routerLink"
                   [routerLink]="child.routerLink" [queryParams]="child.queryParams" routerLinkActive="active-menuitem-routerlink"
                   [routerLinkActiveOptions]="{exact: true}" [attr.tabindex]="!visible ? '-1' : null" [attr.target]="child.target">
                    <i [ngClass]="child.icon" [pTooltip]="child.tooltip"></i><span>{{child.label}}</span>
                    <span class="menuitem-badge" *ngIf="child.badge">{{child.badge}}</span>
                    <i class="fa fa-fw fa-angle-down layout-menuitem-toggler" *ngIf="child.items"></i>
                </a>
                <div class="submenu-arrow" *ngIf="child.items"></div>
                <ul app-submenu [item]="child" *ngIf="child.items" [visible]="isActive(i)" [reset]="reset" [parentActive]="isActive(i)"
                    [@children]="(app.isSlim()||app.isHorizontal())&&root ? isActive(i) ?
                    'visible' : 'hidden' : isActive(i) ? 'visibleAnimated' : 'hiddenAnimated'"></ul>
            </li>
        </ng-template>


    `, styles: [`

  `],
    animations: [
        trigger('children', [
            state('hiddenAnimated', style({
                height: '0px',
                opacity: 0
            })),
            state('visibleAnimated', style({
                height: '*',
                opacity: 1
            })),
            state('visible', style({
                height: '*',
                'z-index': 100,
                opacity: 1
            })),
            state('hidden', style({
                height: '0px',
                'z-index': '*',
                opacity: 0
            })),
            transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppSubMenuComponent {

    @Input() item: MenuItem;

    @Input() root: boolean;

    @Input() visible: boolean;

    _reset: boolean;

    _parentActive: boolean;

    activeIndex: number;
    private disabledDirective: DisabledDirective;
    currentUser: any;
    currentCompagnie: any;


    constructor(public app: HomeComponent, changeDetector: ChangeDetectorRef,
                 public translate: TranslateService) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.currentCompagnie = localStorage.getItem('currentCompagnie');

    }

    itemClick(event: Event, item: MenuItem, index: number) {
        if (this.root) {
            this.app.menuHoverActive = !this.app.menuHoverActive;
        }

        // avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        // activate current item and deactivate active sibling if any
        this.activeIndex = (this.activeIndex === index) ? null : index;

        // execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }

        // prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            setTimeout(() => {
                this.app.layoutMenuScrollerViewChild.moveBar();
            }, 450);
            event.preventDefault();
        }

        // hide menu
        if (!item.items) {
            this.app.resetMenu = this.app.isHorizontal() || this.app.isSlim();

            this.app.overlayMenuActive = false;
            this.app.staticMenuMobileActive = false;
            this.app.menuHoverActive = !this.app.menuHoverActive;
        }
    }


    onMouseEnter(index: number) {
        if (this.root && this.app.menuHoverActive && (this.app.isHorizontal() || this.app.isSlim())
            && !this.app.isMobile() && !this.app.isTablet()) {
            this.activeIndex = index;
        }
    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }

    @Input() get reset(): boolean {
        return this._reset;
    }

    set reset(val: boolean) {
        this._reset = val;

        if (this._reset && (this.app.isHorizontal() || this.app.isSlim())) {
            this.activeIndex = null;
        }
    }

    @Input() get parentActive(): boolean {
        return this._parentActive;
    }

    set parentActive(val: boolean) {
        this._parentActive = val;

        if (!this._parentActive) {
            this.activeIndex = null;
        }
    }

}
