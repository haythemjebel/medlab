import {Component, OnDestroy, OnInit, ChangeDetectorRef, Optional} from '@angular/core';
import {Router} from '@angular/router';
import {AuthentificationService} from '../../shared/service/authentification.service';
import {HomeComponent} from '../../home/home.component';
import { TopBarService } from 'src/app/shared/service/topbar.service';
import {Subscription} from 'rxjs';
import {User} from '../../shared/model/user';
import { UserService } from 'src/app/shared/service/user.service';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { PermissionService } from 'src/app/shared/service/permission.service';
import {DisabledDirective} from '../../shared/directive/disabled.directive';
import {TranslateService} from '@ngx-translate/core';



@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.sass']
})
export class TopbarComponent implements OnInit, OnDestroy {

  currentUserpicture: string;
  private subscriptionChangeName: Subscription;
  currentUser: User;



  constructor(public app: HomeComponent, private router: Router, private authenticationService: AuthentificationService,
             private topbarService: TopBarService, private userService: UserService,
              private sanitizer: DomSanitizer,
              private permissionService: PermissionService,
              changeDetector: ChangeDetectorRef, @Optional() optDisabled: DisabledDirective,
              public translate: TranslateService) {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));


    this.getUserPicture();
  }

  ngOnInit() {

  }

  logout() {
      this.authenticationService.logout();
  }


  ngOnDestroy() {

  }

  getUserPicture(): SafeUrl {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    return this.userService.downloadUserPicture(`/api/users/${user.id}/picture`).subscribe(
      res => this.currentUserpicture = res,
      error => this.currentUserpicture = '../../../assets/layout/images/anonymous-user.png'
    );
  }


  hasAuthorities(authorities, authority): boolean {
    const has = authorities.find(auth => {
      return auth.authority == authority;
    });
    return !!has;
  }
}
