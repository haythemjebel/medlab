import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Filter} from './filter';
import {Calendar} from 'primeng/calendar';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'filters-panel',
  templateUrl: './filters-panel.component.html',
  styleUrls: ['./filters-panel.component.sass']
})
export class FiltersPanelComponent implements OnInit {

  @Input() filters: Filter[];
  criteria: any = {};
  @Output() onSearch: EventEmitter<any> = new EventEmitter();

  constructor(public translate: TranslateService) {
    translate.setDefaultLang('fr');
  }

  find(event: any) {
    if(event.code == "Enter" || event.type == "blur" || event.originalEvent || event.element){
      if (event.target) { // native input blur event
        if ('INPUT' === event.target.nodeName) {
          const oldval = event.target['defaultValue'];
          const property = event.target.getAttribute('data-prop');
          if ((oldval === '' && !this.criteria[property]) || oldval === this.criteria[property] + '') {
            return;
          } else {
            event.target['defaultValue'] = this.criteria[property];
          }
        }
      } else if (event.element && 'DIV' === event.element.nodeName) { // calendar close event --> event.element != undefined
        const changed = event.element.parentElement.parentElement.getAttribute('data-changed');
        if (changed === 'true') {
          event.element.parentElement.parentElement.setAttribute('data-changed', false);
        } else {
          return;
        }
      }
      this.onSearch.emit(this.criteria);
    }


  }

  onInputFilterReset(el: Element) {
    const prop = el.getAttribute('data-prop');
    this.criteria[prop] = null;
    el['defaultValue'] = null;
    event.stopPropagation();
    this.onSearch.emit(this.criteria);
  }

  onCalendarFilterSelect(cal: Calendar) {
    cal.el.nativeElement.setAttribute('data-changed', true);
    cal.inputfieldViewChild.nativeElement.focus();
  }

  onCalendarFilterReset(cal: Calendar) {
    cal.onClearButtonClick(event);
    cal.el.nativeElement.setAttribute('data-changed', true);
    this.onSearch.emit(this.criteria);
  }

  resetSearch() {
    this.criteria = {};
    this.onSearch.emit(this.criteria);
  }

  ngOnInit() {
  }

}
