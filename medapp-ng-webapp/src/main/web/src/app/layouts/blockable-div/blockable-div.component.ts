import {Component, ElementRef, Input} from '@angular/core';
import {BlockableUI} from 'primeng/api';

@Component({
  selector: 'blockable-div',
  template: `
    <ng-content></ng-content>
  `,
  styles: ['div { position: relative; }']
})
export class BlockableDivComponent implements BlockableUI {

  @Input() id: any;
  @Input() styleClass: string;
  constructor(private el: ElementRef) {
  }

  getBlockableElement(): HTMLElement {
      return this.el.nativeElement.children[0];
  }

}
