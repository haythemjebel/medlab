import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {BreadcrumbService} from './breadcrumb.service';
import {Subscription} from 'rxjs';
import {MenuItem} from 'primeng/api';


@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.sass']
})
export class BreadcrumbComponent implements OnDestroy {

    subscription: Subscription;

    items: MenuItem[];
    @Input() add: boolean;
    @Input() edit: boolean;
    @Input() save: boolean;
    @Input() cancel: boolean;
    @Input() show: boolean;

    @Output() addEvent: EventEmitter<any> = new EventEmitter();
    @Output() editEvent: EventEmitter<any> = new EventEmitter();

    constructor(public breadcrumbService: BreadcrumbService) {
        this.subscription = breadcrumbService.itemsHandler.subscribe(response => {
            this.items = response;
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    editProfile() {
        this.editEvent.emit();
    }

    addProfile() {
        this.addEvent.emit();
    }

}
