import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Filter} from '../filters-panel/filter';

@Component({
  selector: 'app-actions-buttons',
  templateUrl: './actions-buttons.component.html',
  styleUrls: ['./actions-buttons.component.sass']
})
export class ActionsButtonsComponent implements OnInit {
  @Input() updateMode: boolean;
  @Input() canSave: boolean;
  @Input() canDelete: boolean;
  @Output() clickSave: EventEmitter<any> = new EventEmitter();
  @Output() clickAbandonner: EventEmitter<any> = new EventEmitter();
  @Output() clickDelete: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  save() {
    this.clickSave.next();
  }

  abandonner() {
    this.clickAbandonner.next();
  }

  delete() {
    this.clickDelete.next();
  }
}
