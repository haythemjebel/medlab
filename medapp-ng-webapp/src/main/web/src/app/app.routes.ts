import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {LoginComponent} from './connexion/login/login.component';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from './shared/helpers/auth.guard';
import {userManagementRoute} from './home/user-management/user-management-route';
import {UserProfileComponent} from './home/user-management/user-profile/user-profile.component';
import {AccessDeniedComponent} from './access-denied/access-denied.component';
import {ProfilDetailsComponent} from './home/profil-details/profil-details.component';
import {ProfilComponent} from './home/profil/profil.component';
import {ErrorComponent} from './error/error.component';
import {patientManagementRoute} from './home/patient-managment/patient-managment-route';
import { DashboardRoute } from './home/dashboard/dashboard-route';
import { HistoryRoute } from './home/history-patient/history-patient-route';

export const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'access-denied', component: AccessDeniedComponent},
  { path: 'error', component: ErrorComponent },
  { path : 'home',  component: HomeComponent , canActivate: [AuthGuard] ,
  children : [
    {
      path: 'user-management',
      canActivate: [AuthGuard],
      children: userManagementRoute
    },
    {
      path: 'patient-management',
      canActivate: [AuthGuard],
      children: patientManagementRoute
    },
    {
      path: 'dashboard',
      canActivate: [AuthGuard],
      children: DashboardRoute
    },
    {
      path: 'history-patient',
      canActivate: [AuthGuard],
      children: HistoryRoute
    },
    {
      path: 'profile', component: ProfilComponent,
      canActivate: [AuthGuard],
      data: {
        permission: ['Créer-Profil']
      }
    },
    {
      path: 'profile/create',
      component: ProfilDetailsComponent,
      canActivate: [AuthGuard],
      data: {
          permission: ['Créer-Profil']
        }
      },
      { path: 'profile/:id/edit',
        component: ProfilDetailsComponent,
        canActivate: [AuthGuard],
        data: {
          readonly: false,
          permission: ['Modifier/Supprimer-Profil']

       }
     },
    {
      path: 'profile/:id/show',
      component: ProfilDetailsComponent ,
      canActivate: [AuthGuard],
      data: {
        readonly: true,
        permission: ['Consulter-Profil']
      }},
      { path : 'user-management/user-profile',  component: UserProfileComponent },
    ]},

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'});
