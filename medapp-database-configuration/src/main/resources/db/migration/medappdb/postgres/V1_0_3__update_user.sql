
CREATE OR REPLACE FUNCTION public.unaccent_string(text)
RETURNS text
IMMUTABLE
STRICT
LANGUAGE SQL
AS $$
SELECT translate(
    $1,
    'âãäåÁÂÃÄÅèééêëÈÉÉÊËìíîïìÌÍÎÏÌóôõöÒÓÔÕÖùúûüÙÚÛÜ',
    'aaaaAAAAAeeeeeEEEEEiiiiiIIIIIooooOOOOOuuuuUUUU'
);
$$;

Alter table public."user" add column identifiant character varying(8);
update public."user" set identifiant = upper(unaccent_string(substring(prenom from 1 for 2) || substring(nom from 1 for 6)));



alter table public."user" alter column identifiant set not null;

CREATE UNIQUE INDEX idx_unique_user_identifiant
ON public."user"(identifiant);