CREATE SEQUENCE public.profile_profile_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.profile (
    profile_id bigint DEFAULT nextval('public.profile_profile_id_seq'::regclass) NOT NULL,
    codemetier character varying(256)  NOT NULL,
    description character varying(256),
    created_by character varying(50) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    last_modified_by character varying(50),
    last_modified_date timestamp without time zone,
    PRIMARY KEY (profile_id)
);

CREATE UNIQUE INDEX idx_unique_profile_code_metier
ON public.profile(codemetier);




CREATE SEQUENCE public.permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.permission(
    permission_id bigint NOT NULL DEFAULT nextval('public.permission_id_seq'::regclass),
    description character varying(256) COLLATE pg_catalog."default",
    role character varying(255) COLLATE pg_catalog."default" NOT NULL,
    created_by character varying(50) COLLATE pg_catalog."default" NOT NULL,
    created_date timestamp without time zone NOT NULL,
    last_modified_by character varying(50) COLLATE pg_catalog."default",
    last_modified_date timestamp without time zone,
    grouped varchar(255),
    PRIMARY KEY (permission_id)
  
);


CREATE TABLE public.profile_permission
(
    profile_id bigint NOT NULL,
    permission_id bigint NOT NULL
);

INSERT INTO public.permission(permission_id,grouped,role,description,created_date, created_by, last_modified_date, last_modified_by) values
(nextval('public.permission_id_seq'),'Utilisateurs - Administration','Consulter-Utilisateur','Accès à la gestion de utilisateurs',CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(nextval('public.permission_id_seq'),'Utilisateurs - Administration','Créer-Utilisateur',null,CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(nextval('public.permission_id_seq'),'Utilisateurs - Administration','Modifier-Utilisateur',null,CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(nextval('public.permission_id_seq'),'Profil - Administration','Créer-Profil',null,CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(nextval('public.permission_id_seq'),'Profil - Administration','Modifier/Supprimer-Profil',null,CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(nextval('public.permission_id_seq'),'Profil - Administration','Consulter-Profil','Accès à la page de profil',CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(nextval('public.permission_id_seq'),'Utilisateurs - Administration','Connect_with_Login',null,CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(nextval('public.permission_id_seq'),'Réinitialiser-Mot-Passe','Utilisateurs - Administration',null,CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP,'system');

INSERT INTO public.profile_permission(profile_id,permission_id) values
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6),
(1,7),
(1,8);


INSERT INTO public.profile(profile_id,codeMetier,created_date, created_by, last_modified_date, last_modified_by) VALUES
(1,'ADMIN',CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system'),
(2,'USER',CURRENT_TIMESTAMP,'system',CURRENT_TIMESTAMP, 'system');


