alter table  public."rendez_vous" add column patient_id bigint;
alter table  public."rendez_vous" add
    CONSTRAINT rendez_vous_ibfk_1 FOREIGN KEY (patient_id) REFERENCES Patient (id);