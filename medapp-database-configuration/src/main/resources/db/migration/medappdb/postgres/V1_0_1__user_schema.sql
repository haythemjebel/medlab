CREATE SEQUENCE public.hibernate_sequence   
	INCREMENT BY 1
    START WITH 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE SEQUENCE public.user_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public."user" (
                               id                 bigint DEFAULT nextval('public.user_id_seq'::regclass) NOT NULL,
                               activated          boolean NOT NULL,
                               email              character varying(320),
                               password_hash      character varying(64),
                               nom                character varying(80),
                               prenom             character varying(80),
                               nb_attempts        integer,
                               non_locked         boolean ,
                               created_by         character varying(50),
                               created_date       timestamp without time zone,
                               last_modified_by   character varying(50),
                               last_modified_date timestamp without time zone,
                               profile_id         bigint,
                               PRIMARY KEY (id)
);

CREATE
UNIQUE INDEX idx_unique_user_email
ON public."user"(email);

INSERT INTO public."user" (id, profile_id, nom, prenom, email, password_hash, activated, non_locked, nb_attempts,
                           created_date, created_by, last_modified_date, last_modified_by)
VALUES (1, 1, 'Admin', 'Admin', 'admin@medapp.com', '$2a$10$1JpuJTbHxlaO4iSZ4V71w.CZlcQLl.HBG1gVzGfbazL6DkCI3GPUK',
        true, true, 0, CURRENT_TIMESTAMP, 'system', CURRENT_TIMESTAMP, 'system');
