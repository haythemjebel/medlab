CREATE SEQUENCE public.patient_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;

CREATE TABLE public."patient"
(
    id                    bigint DEFAULT nextval('public.patient_id_seq'::regclass) NOT NULL,

    email                 character varying(320),
    fiche_Medicale_Number character varying(80),
    adresse_Par           character varying(80),
    cnam_num              character varying(80),
    profession            character varying(80),
    adresse               character varying(80),
    tel                   character varying(80),
    date_naissance        date,
    atcds                 character varying(100),
    diagnostic            character varying(100),

    nom                   character varying(80),
    prenom                character varying(80),

    created_by            character varying(50),
    created_date          timestamp without time zone,
    last_modified_by      character varying(50),
    last_modified_date    timestamp without time zone,
    PRIMARY KEY (id)
);
