CREATE SEQUENCE public.rendez_vous_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;

CREATE TABLE public."rendez_vous"
(
    id                     bigint DEFAULT nextval('public.rendez_vous_id_seq'::regclass) NOT NULL,


    date_Rendez_Vous       date,
    type_Rendez_Vous       character varying(50),
    duree_Rendez_Vous      int,
    diagnostic_Traittement text,
    created_by             character varying(50),
    created_date           timestamp without time zone,
    last_modified_by       character varying(50),
    last_modified_date     timestamp without time zone,
    PRIMARY KEY (id)
);
