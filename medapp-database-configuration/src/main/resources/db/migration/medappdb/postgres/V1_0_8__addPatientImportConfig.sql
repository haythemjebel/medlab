CREATE SEQUENCE public.patient_import_config_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE CACHE 1;

CREATE TABLE public."patient_import_config"
(
    id                    bigint DEFAULT nextval('public.patient_import_config_id_seq'::regclass) NOT NULL,


    nom               character varying(80),
    date_Import        date,
    url_File character varying(100),
        status character varying(50),
    created_by            character varying(50),
    created_date          timestamp without time zone,
    last_modified_by      character varying(50),
    last_modified_date    timestamp without time zone,
    PRIMARY KEY (id)
);
