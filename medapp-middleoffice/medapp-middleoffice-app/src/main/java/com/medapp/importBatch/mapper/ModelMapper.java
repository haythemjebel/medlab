package com.medapp.importBatch.mapper;

import java.util.HashMap;
import java.util.Map;

public class ModelMapper {
    private Map<String, String> attributs = new HashMap<>();
    private String name;

    public Map<String, String> getAttributs() {
        return attributs;
    }

    public void setAttributs(Map<String, String> attributs) {
        this.attributs = attributs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
