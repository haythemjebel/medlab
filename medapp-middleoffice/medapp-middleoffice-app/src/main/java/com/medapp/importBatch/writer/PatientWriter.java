package com.medapp.importBatch.writer;


import com.medapp.domain.Patient;
import com.medapp.importBatch.processor.PatientComponent;
import com.medapp.service.PatientService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
public class PatientWriter implements ItemWriter<List<Patient>> {
	@Autowired 
	PatientComponent patientComponent;
	@Inject
    private PatientService patientService;
    @Override
    public void write(List<? extends List<Patient>> items) throws Exception {
        Patient p = new Patient();
        p.setNom(((Patient) items.get(3)).getNom());
        p.setPrenom(((Patient) items.get(5)).getNom());
       String age = ((Patient) items.get(7)).getNom();
        p.setAgePAtient(age.isEmpty()?0:calculateAge(age));
        p.setCnamNum(((Patient) items.get(9)).getNom());
        p.setProfession(((Patient) items.get(11)).getNom());
        p.setAdresse(((Patient) items.get(13)).getNom());
        p.setAdressePar(((Patient) items.get(15)).getNom());
        String numtem = ((Patient) items.get(17)).getNom();
        p.setTel(numtem.replace(".","").substring(0,8));
        p.setEmail("email@test.com");
        p.setAtcds(((Patient) items.get(21)).getNom());
        p.setDiagnostic(((Patient) items.get(27)).getNom());
       Patient patient =patientService.createPatientbatch(p);
       patientComponent.setPatient(patient);

    }
    public int calculateAge(String age){
   if(age.contains("ans")){

      return Integer.parseInt( age.substring(0,2));
   }
   else{
       Date date = new Date();
       return date.getYear() - Integer.parseInt( age.substring(0,4));
   }
    }
}
