package com.medapp.importBatch.listener;

import com.medapp.importBatch.param.JobParams;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class ImportJobListener implements JobExecutionListener {

    private final JobParams jobParams;

    public ImportJobListener(JobParams jobParams) {

        this.jobParams = jobParams;
    }


    @Override
    public void beforeJob(JobExecution jobExecution) {

        String urlFile =jobExecution.getJobParameters().getString("urlFile");
        jobParams.setUrlFile(urlFile);

    }






    @Override
    public void afterJob(JobExecution jobExecution) {
        // TODO Auto-generated method stub

    }





}
