package com.medapp.importBatch.writer;


import com.medapp.domain.RendezVous;
import com.medapp.importBatch.processor.PatientComponent;
import com.medapp.service.RendezVousService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import java.util.List;
public class RendezVousWriter implements ItemWriter<List<RendezVous>> {
	@Autowired 
	PatientComponent patientComponent;
	@Inject
    private RendezVousService rendezVousService;
    @Override
    public void write(List<? extends List<RendezVous>> items) throws Exception {
        for (int i= 0;i<items.size();i++) {

            if(((RendezVous) items.get(i)).getDateRendezVous()!=null){
                RendezVous rendezVous = new RendezVous();
                rendezVous.setDateRendezVous(((RendezVous) items.get(i)).getDateRendezVous());
                rendezVous.setDiagnosticAndTraittement(((RendezVous) items.get(i)).getDiagnosticAndTraittement());
                rendezVous.setPatient(patientComponent.getPatient());
                rendezVousService.createRendezVous(rendezVous);
            }

        }


    }
}
