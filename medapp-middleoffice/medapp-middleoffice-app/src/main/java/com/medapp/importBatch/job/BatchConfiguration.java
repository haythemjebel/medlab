package com.medapp.importBatch.job;

import com.medapp.domain.Patient;
import com.medapp.domain.RendezVous;
import com.medapp.importBatch.listener.ImportJobListener;
import com.medapp.importBatch.mapper.PatientExcelRowMapper;
import com.medapp.importBatch.mapper.RendezVousExcelRowMapper;
import com.medapp.importBatch.param.JobParams;
import com.medapp.importBatch.processor.PatientComponent;
import com.medapp.importBatch.writer.PatientWriter;
import com.medapp.importBatch.writer.RendezVousWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;


@Configuration
@EnableBatchProcessing
public class BatchConfiguration extends DefaultBatchConfigurer {
    @Autowired
    @Qualifier("medappDataSource")
    private DataSource dataSource;

    
    @Autowired
    @Qualifier("medappJpaTransactionManagerBatch")
    private PlatformTransactionManager transactionManager;

    @Override
    public PlatformTransactionManager getTransactionManager() {
        return transactionManager;
    }

    @Override
    protected JobRepository createJobRepository() throws Exception {
        JobRepositoryFactoryBean factoryBean = new JobRepositoryFactoryBean();
        factoryBean.setIsolationLevelForCreate("ISOLATION_REPEATABLE_READ");
        factoryBean.setDataSource(dataSource);
        factoryBean.setTransactionManager(transactionManager);

        factoryBean.afterPropertiesSet();
        return factoryBean.getObject();
    }
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    @JobScope
    ImportJobListener importJobListener(JobParams jobParamsinit) {
        return new ImportJobListener(jobParamsinit);
    }

    @Bean
    @StepScope
    public ItemStreamReader<Patient> reader(@Autowired JobParams jobParamsinit) throws Exception {

    PoiItemReader reader = new PoiItemReader();


    reader.setResource(new FileSystemResource(jobParamsinit.getUrlFile()));


    reader.setLinesToSkip(2);

    PatientExcelRowMapper rs=new PatientExcelRowMapper();
    reader.setRowMapper(rs);
    return reader;

}



    
    @Bean
    @StepScope
    public ItemStreamReader<RendezVous> readerRendezVous(@Autowired JobParams jobParamsinit) throws Exception {

        PoiItemReader readerRendezVous = new PoiItemReader();


        readerRendezVous.setResource(new FileSystemResource(jobParamsinit.getUrlFile()));
        readerRendezVous.setLinesToSkip(38);
       
        readerRendezVous.setRowMapper(new RendezVousExcelRowMapper());
        return readerRendezVous;


    }



    @Bean
    @StepScope
    public PatientWriter writeDate(@Autowired PatientComponent patientComponent) {
        PatientWriter importWriter = new PatientWriter();
     
        return importWriter;
    }
    @Bean
    @StepScope
    public RendezVousWriter writeRendezVous(@Autowired PatientComponent patientComponent) {
    	RendezVousWriter rendezVousWriter = new RendezVousWriter();
     
        return rendezVousWriter;
    }

    @Bean

    public Step stepImport(ItemReader<Patient> reader, ItemWriter writeDate) throws Exception {
        return stepBuilderFactory.get("stepImport").chunk(100)
                .reader(reader)
                .writer(writeDate)
                .build();
    }
    
    @Bean

    public Step stepImportRendezVous(ItemReader<RendezVous> readerRendezVous, ItemWriter writeRendezVous) {
        return stepBuilderFactory.get("stepImportRendezVous").chunk(100)
                .reader(readerRendezVous).writer(writeRendezVous).build();
    }


    @Bean
    public Job job(Step stepImport, ImportJobListener importJobListener,Step stepImportRendezVous) {
        return jobBuilderFactory.get("jobImportf").
                listener(importJobListener).
                start(stepImport)
                .next(stepImportRendezVous)
                .build();
    }

}
