
package com.medapp.importBatch.mapper;

import com.medapp.domain.RendezVous;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RendezVousExcelRowMapper implements RowMapper<RendezVous> {

    @Override
    public RendezVous mapRow(RowSet rowSet) throws Exception {
    	RendezVous rendezVous = new RendezVous();

        System.out.println(rowSet.getCurrentRow() == null ? "" : rowSet.getColumnValue(0));
        
      
        System.out.println(rowSet.getCurrentRow() == null ? "" : rowSet.getColumnValue(1));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String dateString = format.format( new Date()   );
    if(rowSet.getCurrentRow() != null) {
    	  String dateStringCell = rowSet.getColumnValue(0);
    	  if(!dateStringCell.equals("")){
              Date   date      = format.parse (dateStringCell);
              rendezVous.setDateRendezVous(date);
              rendezVous.setDiagnosticAndTraittement(rowSet.getCurrentRow() == null ? "" : rowSet.getColumnValue(1));
          }


    }
      
        
      
        rendezVous.setDiagnosticAndTraittement(rowSet.getCurrentRow() == null ? "" :rowSet.getColumnValue(1));
        return rendezVous;
    }
}