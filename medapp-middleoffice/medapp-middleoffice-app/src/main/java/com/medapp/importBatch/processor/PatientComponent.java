package com.medapp.importBatch.processor;

import com.medapp.domain.Patient;
import org.springframework.stereotype.Component;

@Component
public class PatientComponent {
private Patient patient;

public Patient getPatient() {
	return patient;
}

public void setPatient(Patient patient) {
	this.patient = patient;
}
}
