package com.medapp.importBatch.mapper;


import com.medapp.domain.Patient;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

public class PatientMapperImpl implements RowMapper<Patient> {
    @Override
    public Patient mapRow(RowSet rs) throws Exception {

        Patient student = new Patient();

        student.setNom(rs.getColumnValue(0));

        return student;
    }
}

