package com.medapp.importBatch.param;

import org.springframework.stereotype.Component;

@Component
public class JobParams {
    private String urlFile;

    public String getUrlFile() {
        return urlFile;
    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }
}
