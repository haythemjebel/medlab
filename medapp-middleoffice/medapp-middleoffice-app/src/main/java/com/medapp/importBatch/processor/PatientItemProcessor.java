package com.medapp.importBatch.processor;

import com.medapp.domain.Patient;
import org.springframework.batch.item.ItemProcessor;

public class PatientItemProcessor implements ItemProcessor<Patient, Patient> {


    @Override
    public Patient process(final Patient person) throws Exception {
        final String firstName = person.getNom();


        final Patient transformedPerson = new Patient();
        transformedPerson.setNom(firstName);


        return transformedPerson;
    }

}
