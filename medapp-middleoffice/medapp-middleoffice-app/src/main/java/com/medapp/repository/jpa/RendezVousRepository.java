package com.medapp.repository.jpa;

import com.medapp.domain.RendezVous;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface RendezVousRepository extends JpaSpecificationExecutor<RendezVous>, JpaRepository<RendezVous, Long> {
	Page<RendezVous> findAll(Pageable pageable);
	List<RendezVous> findByPatientId(Long id);

}
