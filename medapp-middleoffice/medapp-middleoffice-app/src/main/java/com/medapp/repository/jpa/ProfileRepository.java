package com.medapp.repository.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.medapp.domain.Profile;

public interface ProfileRepository extends JpaRepository<Profile, Long>, JpaSpecificationExecutor<Profile> {


	Optional<Profile>  findOneByCodeMetier(String codeMetier);
	
	@EntityGraph(value = "Profile.permissions")
	Optional<Profile> findOneById(Long id);

	Profile findBycodeMetierIgnoreCase(String string);
	

}
