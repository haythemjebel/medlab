package com.medapp.repository.jpa;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.medapp.domain.User;

@Transactional
public interface UserLoginRepository extends JpaRepository< User, Long>{
	@EntityGraph(value = "User.profile")
	User findOneByEmail(final String email);
}
