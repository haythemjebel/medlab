package com.medapp.repository.jpa;

import com.medapp.domain.PatientImportConfig;
import com.medapp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface PatientImportConfigRepository extends JpaRepository<PatientImportConfig, Long>, JpaSpecificationExecutor<User> {
	

		
}

