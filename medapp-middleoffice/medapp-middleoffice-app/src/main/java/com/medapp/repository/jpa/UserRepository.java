package com.medapp.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.medapp.domain.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
	
	Page<User> findAll(Pageable pageable);
			 
	List<User> findByNom(String nom);

	List<User> findByPrenom(String prenom);
	
	Optional<User> findOneByEmail(String email);
	
	Optional<User> findOneByIdentifiant(String identifiant);
	
	Optional<User> findOneById(Long id);
	
	long deleteByEmail(String email);
		
}

