package com.medapp.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedEntityGraph(name = "Profile.permissions",
    attributeNodes = @NamedAttributeNode("permissions")
)
@Table(name = "Profile",schema = "public")
public class Profile extends AbstractAuditingEntity{
  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "profile_id", nullable = false, unique = true)
	private Long id;

	@NotBlank
	@Column(name = "codemetier", nullable = false, unique = true)
	private String codeMetier;

	@Column(name = "description", length = 256)
	private String description;
	
	@ManyToMany(fetch = FetchType.LAZY,targetEntity = Permission.class)
	@JoinTable(name = "Profile_Permission",schema = "public", joinColumns = {
			@JoinColumn(name = "profile_id", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "permission_id", nullable = false, unique = false) }, uniqueConstraints = {
							@UniqueConstraint(columnNames = { "profile_id", "permission_id" }) })
	private List<Permission> permissions = new ArrayList<Permission>();

	
	@OneToMany(mappedBy = "profile")
	private List<User> users = new ArrayList<>();

	@JsonManagedReference
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		if (!users.isEmpty()) {
			for (User user : users) {
				user.setProfile(this);
			}
		}
		this.users = users;
	}

	public void addUser(User user) {
		getUsers().add(user);
		user.setProfile(this);
	}

	public void removeUser(User user) {
		if (getUsers().contains(user)) {
			getUsers().remove(user);
			user.setProfile(null);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodeMetier() {
		return codeMetier;
	}

	public void setCodeMetier(String codeMetier) {
		this.codeMetier = codeMetier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	public Profile(@NotBlank String codeMetier, String description, List<Permission> permissions, List<User> users) {
		super();
		this.codeMetier = codeMetier;
		this.description = description;
//		this.permissions = permissions;
//		this.users = users;
	}

	public Profile() {
		super();
	}
	
	
	
	
	
	

}
