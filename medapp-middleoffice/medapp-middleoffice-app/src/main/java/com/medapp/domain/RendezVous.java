package com.medapp.domain;


import com.medapp.support.RendezVousType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "rendez_vous", schema = "public")
public class RendezVous extends AbstractAuditingEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "rendez_vous_sequence", sequenceName = "rendez_vous_id_seq", schema = "public", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rendez_vous_sequence")
	@Column(insertable = false, updatable = false)
	private Long id;


	@Temporal(TemporalType.DATE)
	@Column(name = "date_rendez_vous")
	private Date dateRendezVous;


	@Column(name = "type_rendez_vous")
	@Enumerated(EnumType.STRING)
	private RendezVousType typeRendezVous;

	@Column(name = "duree_rendez_vous")
	private int dureeRendezVous;


	@Column(name = "diagnostic_traittement", columnDefinition = "TEXT")
	private String diagnosticAndTraittement;



	@ManyToOne
	@JoinColumn(name="patient_id", nullable=false)
	private Patient patient;

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public RendezVous() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateRendezVous() {
		return dateRendezVous;
	}

	public void setDateRendezVous(Date dateRendezVous) {
		this.dateRendezVous = dateRendezVous;
	}

	public RendezVousType getTypeRendezVous() {
		return typeRendezVous;
	}

	public void setTypeRendezVous(RendezVousType typeRendezVous) {
		this.typeRendezVous = typeRendezVous;
	}

	public int getDureeRendezVous() {
		return dureeRendezVous;
	}

	public void setDureeRendezVous(int dureeRendezVous) {
		this.dureeRendezVous = dureeRendezVous;
    }

    public String getDiagnosticAndTraittement() {
        return diagnosticAndTraittement;
    }

    public void setDiagnosticAndTraittement(String diagnosticAndTraittement) {
        this.diagnosticAndTraittement = diagnosticAndTraittement;
    }

    public RendezVous(Long id, Date dateRendezVous, RendezVousType typeRendezVous, int dureeRendezVous,
                      String diagnosticAndTraittement) {
        super();
        this.id = id;
        this.dateRendezVous = dateRendezVous;
        this.typeRendezVous = typeRendezVous;
        this.dureeRendezVous = dureeRendezVous;
        this.diagnosticAndTraittement = diagnosticAndTraittement;
    }


}
