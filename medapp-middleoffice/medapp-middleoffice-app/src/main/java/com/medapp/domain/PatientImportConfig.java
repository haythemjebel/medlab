package com.medapp.domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "patient_import_config", schema = "public")
public class PatientImportConfig extends AbstractAuditingEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "patient_import_config_sequence", sequenceName = "patient_import_config_id_seq", schema = "public", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "patient_import_config_sequence")
	@Column(insertable = false, updatable = false)
	private Long id;

	@NotNull
	@Column(name = "nom", nullable = false)
	private String nom;


    @Temporal(TemporalType.DATE)
	@Column(name = "date_Import")
	private Date dateImport;

	@Column(name = "url_file")
	private String urlFile;
	@Column(name = "status")
	private String status;

	public Long getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateImport() {
		return dateImport;
	}

	public void setDateImport(Date dateImport) {
		this.dateImport = dateImport;
	}

	public String getUrlFile() {
		return urlFile;
	}

	public void setUrlFile(String urlFile) {
		this.urlFile = urlFile;
	}
}
