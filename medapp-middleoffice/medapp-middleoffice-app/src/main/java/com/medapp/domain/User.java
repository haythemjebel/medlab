package com.medapp.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.medapp.service.dto.UserDTO;

@Entity
@NamedEntityGraph(name = "User.profile",attributeNodes = @NamedAttributeNode("profile"))
@Table(name = "user", schema = "public")
public class User extends AbstractAuditingEntity{

	private static final long serialVersionUID = 1L;

	@Id
    @SequenceGenerator(name = "user_sequence", sequenceName = "user_id_seq", schema = "public",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_sequence")
    @Column(insertable = false, updatable = false)
	private Long id;
	
	@NotNull
	@Column(name = "nom", nullable = false)
	private String nom;
	
	@NotNull
	@Column(name = "prenom", nullable = false)
	private String prenom;
	
	@NotNull
	@Size(min = 1, max = 50)
	@Column(length = 50, unique = true, nullable = false)
	private String email;
	
	@JsonIgnore
    @NotNull
	@Column(name = "password_hash", nullable = false)
	private String password;
	

	@NotNull
	@Column(nullable = false)
	private Boolean activated = false;

	@Column(name = "nb_attempts", nullable = false)
	private Integer nbAttempts;
	
	@NotNull
	@Column(name = "non_locked", nullable = false)
	private Boolean nonLocked;


	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "profile_id", nullable = true)
	private Profile profile;
	
	
	@NotNull
	@Size(min = 1, max = 50)
	@Column(name="identifiant", unique = true, nullable = false)
	private String identifiant;
	

	public Boolean getNonLocked() {
		return nonLocked;
	}
	public void setNonLocked(Boolean nonLocked) {
		this.nonLocked = nonLocked;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public Integer getNbAttempts() {
		return nbAttempts;
	}

	public void setNbAttempts(Integer nb_attempts) {
		this.nbAttempts = nb_attempts;
	}

	

	@JsonBackReference
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	

	public User() {
		super();
	}
	
	public User( User user) {
		this.id = user.id;

		this.email = user.email;
		this.nbAttempts = user.nbAttempts;
		this.nonLocked = user.nonLocked;
		this.password = user.password;
		this.profile = user.profile;
		this.identifiant = user.identifiant;
	
	}
	public User(String nom,  String prenom,String email,
			String password, String region,boolean activated, boolean estMultiRegion , String identifiant) {
		super();
	
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		this.activated = activated;
		this.identifiant = identifiant;
	}
	public User(Long id,String nom,  String prenom,String email,
			String password, String region,boolean activated, boolean estMultiRegion , String identifiant) {
		super();
	    this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		this.activated = activated;
		this.identifiant = identifiant;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", activated=" + activated + ", nb_attempts=" + nbAttempts + ",identifiant=" + identifiant + "]";
	}
	

	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	
	public UserDTO userTouserDTO() {
		UserDTO uu = new UserDTO();
		uu.setEmail(this.email);
		uu.setNom(this.nom);
		uu.setPrenom(this.prenom);
		uu.setIdentifiant(this.identifiant);
		uu.setActivated(this.activated);
		return uu;
		
	}
}
