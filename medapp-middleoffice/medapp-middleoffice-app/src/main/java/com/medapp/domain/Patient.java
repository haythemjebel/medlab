package com.medapp.domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "patient", schema = "public")
public class Patient extends AbstractAuditingEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "patient_sequence", sequenceName = "patient_id_seq", schema = "public", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "patient_sequence")
	@Column(insertable = false, updatable = false)
	private Long id;

	@NotNull
	@Column(name = "nom", nullable = false)
	private String nom;

	@NotNull
	@Column(name = "prenom", nullable = false)
	private String prenom;

	@NotNull
	@Size(min = 1, max = 50)
	@Column(length = 50, unique = true, nullable = false)
	private String email;


	@Size(min = 1, max = 50)
	@Column(name = "fiche_Medicale_Number", unique = true, nullable = false)
	private String ficheMedicaleNumber;



	@Column(name = "adresse_Par")
	private String adressePar;


	@Size(min = 1, max = 50)
	@Column(name = "cnam_num")
	private String cnamNum;

	@Size(min = 1, max = 50)
	@Column(name = "profession")
	private String profession;


	@Column(name = "adresse")
	private String adresse;

	@Column(name = "tel")
	private String tel;

    @Temporal(TemporalType.DATE)
	@Column(name = "date_naissance")
	private Date dateNaissance;

	@Column(name = "atcds")
	private String atcds;

    @Column(name = "diagnostic")
	private String diagnostic;

    @Transient
	private int agePAtient;


	@OneToMany(mappedBy="patient")
	private List<RendezVous> listRendezVous = new ArrayList<RendezVous>();

	public List<RendezVous> getListRendezVous() {
		return listRendezVous;
	}

	public void setListRendezVous(List<RendezVous> listRendezVous) {
		this.listRendezVous = listRendezVous;
	}

	public Patient() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFicheMedicaleNumber() {
		return ficheMedicaleNumber;
	}

	public void setFicheMedicaleNumber(String ficheMedicaleNumber) {
		this.ficheMedicaleNumber = ficheMedicaleNumber;
	}

	public String getAdressePar() {
		return adressePar;
	}

	public void setAdressePar(String adressePar) {
		this.adressePar = adressePar;
	}

	public String getCnamNum() {
		return cnamNum;
	}

	public void setCnamNum(String cnamNum) {
		this.cnamNum = cnamNum;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getAtcds() {
		return atcds;
	}

	public void setAtcds(String atcds) {
		this.atcds = atcds;
	}

	public String getDiagnostic() {
		return diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
    }

    public int getAgePAtient() {
        return agePAtient;
    }

    public void setAgePAtient(int agePAtient) {
        this.agePAtient = agePAtient;
    }

    public Patient(Long id, @NotNull String nom, @NotNull String prenom, @NotNull @Size(min = 1, max = 50) String email,
                   @Size(min = 1, max = 50) String ficheMedicaleNumber, @Size(min = 1, max = 50) String adressePar,
                   @Size(min = 1, max = 50) String cnamNum, @Size(min = 1, max = 50) String profession, String adresse,
                   String tel, Date dateNaissance, String atcds, String diagnostic) {
        super();
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.ficheMedicaleNumber = ficheMedicaleNumber;
        this.adressePar = adressePar;
        this.cnamNum = cnamNum;
        this.profession = profession;
        this.adresse = adresse;
        this.tel = tel;
        this.dateNaissance = dateNaissance;
        this.atcds = atcds;
        this.diagnostic = diagnostic;
    }


}
