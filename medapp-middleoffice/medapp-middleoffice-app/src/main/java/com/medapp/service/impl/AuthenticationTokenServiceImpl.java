package com.medapp.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medapp.common.UserDetailsWithId;
import com.medapp.repository.jpa.UserRepository;
import com.medapp.service.AuthenticationTokenService;
import com.medapp.service.dto.ProfileDTO;
import com.medapp.service.dto.UserDTO;



@Service
public class AuthenticationTokenServiceImpl implements AuthenticationTokenService{

	@Inject
	UserRepository userRepository;
	
	@Override
	@Transactional
	public UserDetailsWithId findUserByIdentifiant(String identifiant) {
		
		 Optional<UserDTO> res =  Optional.of(userRepository
		        	.findOneByIdentifiant(identifiant))
		            .filter(Optional::isPresent)
		            .map(Optional::get)
		            .map(UserDTO::new);
		UserDTO user = res.isPresent() ? res.get() : null; 
		if(user != null) {
		return new UserDetailsWithId(identifiant, identifiant, user.getActivated(), true, true, user.getNonLocked(), 
				getGrantedAuthorities(getPrivileges(user.getProfile())), user.getId(), user.getNom(), user.getPrenom(), user.getProfile().getId(), identifiant);
		}
		return null;
	}
	
	private List<String> getPrivileges(ProfileDTO profile) {
		return profile != null ? profile.getPermissions().stream()
				.map(permission -> permission.getRole()).collect(Collectors.toList())
				: Collections.emptyList();
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (String privilege : privileges) {
			authorities.add(new SimpleGrantedAuthority(privilege));
		}
		return authorities;
	}

}
