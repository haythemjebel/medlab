package com.medapp.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medapp.domain.Patient;
import com.medapp.repository.jpa.PatientRepository;
import com.medapp.service.PatientService;

@Service
public class PatientServiceImpl implements PatientService {
    static private final Logger LOGGER = LoggerFactory.getLogger(PatientServiceImpl.class);

    @Autowired
    private PatientRepository patientRepository;


    @Override
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @Override
    public void createPatient(Patient patient) {
        patientRepository.save(patient);
    }

	@Override
	public Optional<Patient> findById(Long id) {
		return patientRepository.findById(id);
	}

	@Override
    @Transactional("medappJpaTransactionManagerBatch")
	public Patient createPatientbatch(Patient patient) {
		return patientRepository.save(patient);
	}

	@Override
	public void deleteById(Long id) {
		patientRepository.deleteById(id); ;
	}

}
