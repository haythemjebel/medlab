package com.medapp.service;

import java.util.List;

import com.medapp.domain.PatientImportConfig;

public interface PatientImportConfigService {
	List<PatientImportConfig> getAllImportConfigs();
	PatientImportConfig createPatientImportConfig(PatientImportConfig patientImportConfig);
}
