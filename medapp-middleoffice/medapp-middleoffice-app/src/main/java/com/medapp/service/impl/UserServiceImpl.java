package com.medapp.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medapp.domain.User;
import com.medapp.repository.jpa.UserRepository;
import com.medapp.repository.jpa.UserSpecifications;
import com.medapp.service.ProfileService;
import com.medapp.service.UserService;
import com.medapp.service.dto.UserDTO;
import com.medapp.web.rest.errors.InvalidPasswordException;

@Service
public class UserServiceImpl implements UserService {
	static private Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProfileService profileService;



	@Override
	public Page<UserDTO> getAllUsers(Pageable pageable, UserDTO userDTO) {
		return userRepository.findAll(UserSpecifications.byCriteriaSpec(userDTO.toUser()), pageable).map(UserDTO::new);
	}

	@Override
	public List<UserDTO> getAllUser() {
		return userRepository.findAll().stream().map(data -> {
			return new UserDTO(data);
		}).collect(Collectors.toList());
	}

	@Override
	public Long countAllUsers(UserDTO userDTO) {
		return userRepository.count(UserSpecifications.byCriteriaSpec(userDTO.toUser()));
	}

	public boolean isEmailUnique(UserDTO userDTO) {
		return false;
	}

	public boolean isLoginUnique(UserDTO userDTO) {
		return userRepository.findOneByEmail(userDTO.getEmail().toLowerCase()).isPresent();
	}

	public UserDTO updateUser(UserDTO userDTO) {
		Optional<UserDTO> res = Optional.of(userRepository.findOneById(userDTO.getId())).filter(Optional::isPresent)
				.map(Optional::get).map(user -> {
					LOG.debug("Changed Information for User: {}", user);
					if (userDTO.getActivated() != null)
						user.setActivated(userDTO.getActivated());
					if (userDTO.getNonLocked() != null)
						user.setNonLocked(userDTO.getNonLocked());
					if (StringUtils.isNotBlank(userDTO.getNom()))
						user.setNom(userDTO.getNom());
					if (StringUtils.isNotBlank(userDTO.getPrenom()))
						user.setPrenom(userDTO.getPrenom());
					if (StringUtils.isNotBlank(userDTO.getEmail()))
						user.setEmail(userDTO.getEmail());

					if (userDTO.getProfile() != null) {
						user.setProfile(userDTO.getProfile().toProfile());
					}

					if (userDTO.getIdentifiant() != null) {
						user.setIdentifiant(userDTO.getIdentifiant());
					}

					user.setNonLocked(userDTO.getNonLocked());
					return userRepository.saveAndFlush(user);
				}).map(UserDTO::new);
		return res.isPresent() ? res.get() : null;
	}

	@Override
	@Transactional(value = "medappJpaTransactionManager")
	public UserDTO createUser(UserDTO userDTO) {
		LOG.info("Creation d'un nouveau utilisateur {}, ", userDTO.getEmail());
		User user = userDTO.toUser();
		// init data
		String p = RandomStringUtils.randomAlphanumeric(10);
		user.setPassword(passwordEncoder.encode(p));
		user.setNbAttempts(0);

		User u = userRepository.save(user);
		LOG.debug("Created Information for User: {}", user);
		UserDTO userToReturn = new UserDTO(u);
		userToReturn.setPassword(p);

		// [TODO] send subscription Email here
		LOG.info("Envoi du mot de passe par email à l'utilisateur {}", userDTO.getEmail());

		return userToReturn;
	}

	public void deleteUser(String login) {
		userRepository.findOneByEmail(login).ifPresent(user -> {
			userRepository.delete(user);
			LOG.debug("Deleted User: {}", user);
		});
	}

	@Override
	public UserDTO findOneByEmail(String email) {
		Optional<UserDTO> res = userRepository.findOneByEmail(email).map(UserDTO::new);
		return res.isPresent() ? res.get() : null;
	}

	@Override
	public UserDTO findOneById(Long id) {
		Optional<UserDTO> res = userRepository.findOneById(id).map(UserDTO::new);
		return res.isPresent() ? res.get() : null;
	}


	public void changePassword(String currentClearTextPassword, String newPassword) {
		Optional.of(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).map(String.class::cast)
				.flatMap(userRepository::findOneByEmail).ifPresent(user -> {
					String currentEncryptedPassword = user.getPassword();
					if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
						throw new InvalidPasswordException();
					}
					String encryptedPassword = passwordEncoder.encode(newPassword);
					user.setPassword(encryptedPassword);
					userRepository.saveAndFlush(user);
				});
	}

	@Override
	public boolean isIdentifiantUnique(UserDTO userDTO) {
		return userRepository.findOneByIdentifiant(userDTO.getIdentifiant()).isPresent();
	}

	@Override
	public UserDTO findOneByIdentifiant(String identifiant) {
		Optional<UserDTO> res = userRepository.findOneByIdentifiant(identifiant).map(UserDTO::new);
		return res.isPresent() ? res.get() : null;
	}

	@Override
	public UserDTO resetPassword(UserDTO userDTO) {
		User user = userDTO.toUser();
		// init data
		String p = RandomStringUtils.randomAlphanumeric(10);
		user.setPassword(passwordEncoder.encode(p));
		User u = userRepository.saveAndFlush(user);

		u.setPassword(p);
		return new UserDTO(u);
	}

	@Override
	public void updateIdentifiantOfUsers() {

	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}



}
