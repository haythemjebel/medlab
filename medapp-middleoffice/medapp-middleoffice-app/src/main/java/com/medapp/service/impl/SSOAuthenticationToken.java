package com.medapp.service.impl;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class SSOAuthenticationToken  extends AbstractAuthenticationToken {
	/**
	 * @author Rabeb
	 */
	private final Object principal;
	private Object credentials;
	private static final long serialVersionUID = 1L;
	private final String identifiant;

	public SSOAuthenticationToken(Object principal, Object credentials, String identifiant) {
		super(null);
		this.identifiant = identifiant;
		this.principal = principal;
		this.credentials = credentials;
	}
	public SSOAuthenticationToken(Object principal, Object credentials, String identifiant,
			Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.identifiant = identifiant;
		this.principal = principal;
		this.credentials = credentials;
		super.setAuthenticated(true);
	}
	public SSOAuthenticationToken(String identifiant) {
		this(null, null, identifiant);
	}

	@Override
	public Object getCredentials() {
		return this.credentials;
	}

	@Override
	public Object getPrincipal() {
		return this.principal;
	}
	public String getIdentifiant() {
		return identifiant;
	}

}
