package com.medapp.service.impl;

import javax.inject.Inject;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.medapp.common.UserDetailsWithId;
import com.medapp.service.AuthenticationTokenService;



public class SSOAuthenticationProvider implements AuthenticationProvider {
	@Inject 
	AuthenticationTokenService authenticationTokenService;
	private UserDetailsService userDetailsService;
	
	protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		SSOAuthenticationToken ssoAuthenticationToken = (SSOAuthenticationToken) authentication;
		String identifiant = ssoAuthenticationToken.getIdentifiant();
		
		UserDetailsWithId user = authenticationTokenService.findUserByIdentifiant(identifiant);

		if (user == null) {
			throw new BadCredentialsException(
					messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "External system (Legacy) Authentication Failed"));

		}

		SSOAuthenticationToken authenticationToken = new SSOAuthenticationToken(user, user.getPassword(), user.getIdentifiant(), user.getAuthorities());
		return authenticationToken;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		// return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
		return (SSOAuthenticationToken.class).equals(authentication);
		//To indicate that this authenticationProvider can handle the auth request.
	}
	
	public UserDetailsService getUserDetailsService() {
	    return userDetailsService;
	}

	public void setUserDetailsService(UserDetailsService userDetailsService) {
	    this.userDetailsService = userDetailsService;
	}


}
