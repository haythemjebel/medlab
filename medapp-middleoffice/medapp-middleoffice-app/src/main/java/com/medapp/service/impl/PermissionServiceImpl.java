package com.medapp.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.medapp.domain.Permission;
import com.medapp.repository.jpa.PermissionRepository;
import com.medapp.service.PermissionService;
import com.medapp.service.dto.PermissionDTO;

@Service
public class PermissionServiceImpl implements PermissionService{

	@Autowired
	private PermissionRepository permissionRepository;

	@Override
	public Page<PermissionDTO> getAllPermissions(Pageable pageable) {
		return permissionRepository.findAll(pageable).map(PermissionDTO::new);
		}
	
	@Override
	public Long countAllPermissions() {
		return permissionRepository.count();
		}

	@Override
	public PermissionDTO findOneById(Long id) {
		Optional<Permission> permission= permissionRepository.findOneById(id);
		Optional<PermissionDTO> res = permission.map(PermissionDTO::new);
		
		return res.isPresent() ? res.get() : null;
	}

	@Override
	public List<String> findAllGroup() {
		return permissionRepository.findAllGroup();
		
		
		
	}

	@Override
	public List<PermissionDTO> findPermissionsByGroup(String group) {
		List<Permission> permissions = permissionRepository.findPermissionByGroup(group);
		List<PermissionDTO> perDTO = permissions.stream().map(mapper->{
			PermissionDTO data = new PermissionDTO();
			data.setId(mapper.getId());
			data.setDescription(mapper.getDescription());
			data.setRole(mapper.getRole());
			data.setGrouped(mapper.getGrouped());
			return data;
		}).collect(Collectors.toList());
		return perDTO;
	}

	


}
