package com.medapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medapp.domain.PatientImportConfig;
import com.medapp.repository.jpa.PatientImportConfigRepository;
import com.medapp.service.PatientImportConfigService;

@Service
public class PatientImportConfigServiceImpl implements PatientImportConfigService {
	static private final Logger LOGGER = LoggerFactory.getLogger(PatientImportConfigServiceImpl.class);

	@Autowired
	private PatientImportConfigRepository patientImportConfigRepository;

	@Transactional("medappJpaTransactionManagerBatch")
	@Override
	public PatientImportConfig createPatientImportConfig(PatientImportConfig patientImportConfig) {
		return patientImportConfigRepository.save(patientImportConfig);
	}

	@Override
	public List<PatientImportConfig> getAllImportConfigs() {
		return patientImportConfigRepository.findAll();
	}
}
