package com.medapp.service.dto;

import java.time.Instant;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.medapp.domain.Patient;
import com.medapp.support.RendezVousType;

public class RendezVousDTO {
	private Long id;

	private Date dateRendezVous;

	private RendezVousType typeRendezVous;

	private int dureeRendezVous;

	private String diagnosticAndTraittement;

	private Patient patient;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateRendezVous() {
		return dateRendezVous;
	}

	public void setDateRendezVous(Date dateRendezVous) {
		this.dateRendezVous = dateRendezVous;
	}

	public RendezVousType getTypeRendezVous() {
		return typeRendezVous;
	}

	public void setTypeRendezVous(RendezVousType typeRendezVous) {
		this.typeRendezVous = typeRendezVous;
	}

	public int getDureeRendezVous() {
		return dureeRendezVous;
	}

	public void setDureeRendezVous(int dureeRendezVous) {
		this.dureeRendezVous = dureeRendezVous;
	}

	public String getDiagnosticAndTraittement() {
		return diagnosticAndTraittement;
	}

	public void setDiagnosticAndTraittement(String diagnosticAndTraittement) {
		this.diagnosticAndTraittement = diagnosticAndTraittement;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}
