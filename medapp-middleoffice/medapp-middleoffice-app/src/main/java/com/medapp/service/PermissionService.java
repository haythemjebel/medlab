package com.medapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.medapp.service.dto.PermissionDTO;


public interface PermissionService {
	
	Page<PermissionDTO> getAllPermissions(Pageable pageable);
	Long countAllPermissions();
	PermissionDTO findOneById(Long id);
	List<String> findAllGroup();
	List<PermissionDTO> findPermissionsByGroup(String group);
	
}
