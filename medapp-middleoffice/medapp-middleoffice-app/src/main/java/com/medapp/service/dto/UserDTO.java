package com.medapp.service.dto;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.medapp.domain.User;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {

	private Long id;
	private String nom;
	private String prenom;
	private String email;
	private Boolean activated = true;
	private Boolean nonLocked = true;
	private String createdBy;
	private Instant createdDate;
	private String lastModifiedBy;
	private Instant lastModifiedDate;
	private String password;
	private String identifiant;
	@JsonIgnoreProperties(value = { "permissions", "users" })
	private ProfileDTO profile;



	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String login) {
		this.email = login;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public ProfileDTO getProfile() {
		return profile;
	}
	public void setProfile(ProfileDTO profile) {
		this.profile = profile;
	}
	

	

	
	public UserDTO(User user) {
		super();
		this.id = user.getId();
		this.nom = user.getNom();
		this.prenom = user.getPrenom();
		this.email = user.getEmail();
		this.activated = user.getActivated();
		this.nonLocked = user.getNonLocked();
		this.createdBy = user.getCreatedBy();
		this.createdDate = user.getCreatedDate();
		this.lastModifiedBy = user.getLastModifiedBy();
		this.lastModifiedDate = user.getLastModifiedDate();
		this.password = user.getPassword();
		this.identifiant = user.getIdentifiant();
		this.profile = new ProfileDTO(user.getProfile());

	} 
	
	public UserDTO() {
		
	}
	
	public User toUser() {
		User user = new User();
		user.setEmail(email);
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setActivated(activated);
		user.setNonLocked(nonLocked);
		user.setIdentifiant(identifiant);
		user.setId(id);
		user.setCreatedBy(createdBy);
		user.setCreatedDate(createdDate);
		user.setLastModifiedBy(lastModifiedBy);
		user.setLastModifiedDate(lastModifiedDate);
		if(profile != null) {
			user.setProfile(profile.toProfile());
		}
	
		return user;
	}
	public Boolean getActivated() {
		return activated;
	}
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}
	public Boolean getNonLocked() {
		return nonLocked;
	}
	public void setNonLocked(Boolean nonLocked) {
		this.nonLocked = nonLocked;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Instant getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public UserDTO(String nom, String prenom, String email, Boolean activated, Boolean nonLocked,
			String createdBy, Instant createdDate, String lastModifiedBy, Instant lastModifiedDate, String password,
			String identifiant, ProfileDTO profile) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.activated = activated;
		this.nonLocked = nonLocked;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.lastModifiedBy = lastModifiedBy;
		this.lastModifiedDate = lastModifiedDate;
		this.password = password;
		this.identifiant = identifiant;
		this.profile = profile;
	}	
	
	
	
	
}
