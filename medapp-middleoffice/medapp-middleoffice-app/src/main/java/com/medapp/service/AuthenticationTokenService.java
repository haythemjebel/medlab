package com.medapp.service;

import com.medapp.common.UserDetailsWithId;

public interface AuthenticationTokenService {
	UserDetailsWithId findUserByIdentifiant(String identifiant);

}
