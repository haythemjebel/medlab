package com.medapp.service.impl;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.medapp.domain.Profile;
import com.medapp.domain.User;
import com.medapp.repository.jpa.ProfileRepository;
import com.medapp.repository.jpa.ProfileSpecification;
import com.medapp.service.ProfileService;
import com.medapp.service.dto.ProfileCriteria;
import com.medapp.service.dto.ProfileDTO;
import com.medapp.service.dto.ProfileLazyDTO;


@Service
public class ProfileServiceImpl implements ProfileService{
	@Autowired
	private ProfileRepository profileRepository;

	static private Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Override
	public Page<ProfileLazyDTO> getAllProfiles(Pageable pageable, ProfileCriteria profileCriteria) {
		
		if(profileCriteria != null) {
			return profileRepository.findAll(ProfileSpecification.byCriteriaSpec(profileCriteria),pageable).map(ProfileLazyDTO::new);
		}
		return profileRepository.findAll(pageable).map(ProfileLazyDTO::new);
	}

	@Override
	public Long countAllProfiles(ProfileCriteria profileCriteria) {
		if(profileCriteria != null) {
		return profileRepository.count(ProfileSpecification.byCriteriaSpec(profileCriteria));
		}
		return profileRepository.count();
	}

	@Override
	public ProfileDTO findOneById(Long id) {
		Optional<Profile> profile= profileRepository.findOneById(id);
	Optional<ProfileDTO> res = profile.map(ProfileDTO::new);


 return res.isPresent() ? res.get() : null;
	
	
	}

	@Override
	public ProfileDTO findOneByCodeMetier(String codeMetier) {
		Optional<ProfileDTO> res = profileRepository.findOneByCodeMetier(codeMetier).map(ProfileDTO::new);
		return res.isPresent() ? res.get() : null;
	}

	@Override
	public ProfileDTO createProfile(ProfileDTO profileDTO) {
		Profile profile = new Profile();
        profile.setCodeMetier(profileDTO.getCodeMetier());
        profile.setDescription(profileDTO.getDescription());
        
        if (profileDTO.getPermissions() != null) {
        	profile.setPermissions(profileDTO.getPermissions());
        }
        
        if (profileDTO.getUsers() != null) {
        	profile.setUsers(profileDTO.getUsers());
        }
        for(User u : profile.getUsers()) {
        	u.setProfile(profile);
        }
       Profile p = profileRepository.saveAndFlush(profile);
        log.debug("Created Profile: {}", profile);
       return new ProfileDTO(p);
        
	}
	
	 /**
     * Update all information for a specific profile, and return the modified profile.
     *
     * @param profileDTO profile to update.
     * @return updated profile.
     */
	@Override
	public ProfileDTO updateProfile(ProfileDTO profileDTO) {
		 Optional<ProfileDTO> res =  Optional.of(profileRepository.findOneById(profileDTO.getId()))
		            .filter(Optional::isPresent)
		            .map(Optional::get)
		            .map(profile-> {
		                log.debug("Changed Information for Profile: {}", profile);
		                return profileRepository.saveAndFlush(profileDTO.toProfile());
		            })
		            .map(ProfileDTO::new);
		        return res.isPresent() ? res.get() : null;
	}

	@Override
	public boolean isCodeMetierUnique(@Valid ProfileDTO profileDTO) {
		return profileRepository.findOneByCodeMetier(profileDTO.getCodeMetier().toLowerCase()).isPresent();

	}
	
	
	
	

	
}
