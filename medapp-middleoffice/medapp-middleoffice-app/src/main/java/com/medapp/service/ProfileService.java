package com.medapp.service;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.medapp.service.dto.ProfileCriteria;
import com.medapp.service.dto.ProfileDTO;
import com.medapp.service.dto.ProfileLazyDTO;


public interface ProfileService {
	Long countAllProfiles(ProfileCriteria criteria);
	ProfileDTO findOneById(Long id);
	ProfileDTO findOneByCodeMetier(String codeMetier);
	ProfileDTO createProfile(ProfileDTO profileDTO);
	ProfileDTO updateProfile(ProfileDTO profileDTO);
	boolean isCodeMetierUnique(@Valid ProfileDTO profileDTO);
	Page<ProfileLazyDTO> getAllProfiles(Pageable pageable, ProfileCriteria profileLazyDTO);
	}
