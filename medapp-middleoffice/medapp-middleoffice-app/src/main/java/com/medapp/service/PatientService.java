package com.medapp.service;

import com.medapp.domain.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface PatientService {

	List<Patient> getAllPatients();

	void createPatient(Patient patient);
	
	Patient createPatientbatch(Patient patient);

	Optional<Patient> findById(Long id);
	
	void deleteById(Long id);

}
