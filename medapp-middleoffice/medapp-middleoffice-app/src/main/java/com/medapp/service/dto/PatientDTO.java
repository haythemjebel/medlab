package com.medapp.service.dto;

import java.time.Instant;
import java.util.Date;

public class PatientDTO {
    private Long id;

    private String nom;

    private String prenom;

    private String email;

    private String ficheMedicaleNumber;

    private String adressePar;

    private String cnamNum;

    private String profession;

    private String adresse;

    private String tel;

    private Date dateNaissance;

    private String atcds;

    private String diagnostic;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFicheMedicaleNumber() {
        return ficheMedicaleNumber;
    }

    public void setFicheMedicaleNumber(String ficheMedicaleNumber) {
        this.ficheMedicaleNumber = ficheMedicaleNumber;
    }

    public String getAdressePar() {
        return adressePar;
    }

    public void setAdressePar(String adressePar) {
        this.adressePar = adressePar;
    }

    public String getCnamNum() {
        return cnamNum;
    }

    public void setCnamNum(String cnamNum) {
        this.cnamNum = cnamNum;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getAtcds() {
        return atcds;
    }

    public void setAtcds(String atcds) {
        this.atcds = atcds;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }


}
