package com.medapp.service;

import com.medapp.domain.RendezVous;

import java.util.List;

public interface RendezVousService {


	RendezVous createRendezVous(RendezVous rendezVous);
	List<RendezVous> findRendezVousById(Long id);
}
