package com.medapp.service;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.medapp.service.dto.UserDTO;


public interface UserService {
	
	Page<UserDTO> getAllUsers(Pageable pageable, UserDTO userDTO);
	Long countAllUsers(UserDTO userDTO);
	boolean isLoginUnique(UserDTO userDTO);
	boolean isEmailUnique(UserDTO userDTO);
	boolean isIdentifiantUnique(UserDTO userDTO);
	UserDTO findOneByEmail(String email);
	UserDTO findOneByIdentifiant(String identifiant);
	UserDTO findOneById(Long id);
	UserDTO createUser(UserDTO userDTO);
	UserDTO updateUser(UserDTO userDTO);
	void deleteUser(String login);
	void changePassword(String currentClearTextPassword, String newPassword);
	UserDTO resetPassword(UserDTO user);
	void updateIdentifiantOfUsers();
	List<UserDTO> getAllUser();	
}
