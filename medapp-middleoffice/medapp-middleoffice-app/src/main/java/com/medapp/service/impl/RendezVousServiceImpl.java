package com.medapp.service.impl;

import com.medapp.domain.RendezVous;
import com.medapp.repository.jpa.RendezVousRepository;
import com.medapp.service.RendezVousService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RendezVousServiceImpl  implements RendezVousService {
    static private final Logger LOGGER = LoggerFactory.getLogger(RendezVousServiceImpl.class);

    @Autowired
    private RendezVousRepository rendezVousRepository;

   
  

	@Override
    @Transactional("medappJpaTransactionManagerBatch")
	public RendezVous createRendezVous(RendezVous rendezVous) {
		// TODO Auto-generated method stub
		return rendezVousRepository.save(rendezVous);
	}




	@Override
	public List<RendezVous> findRendezVousById(Long id) {
		// TODO Auto-generated method stub
		return rendezVousRepository.findByPatientId(id);
	}

}
