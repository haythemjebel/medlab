package com.medapp.configuration;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jdbc.repository.config.JdbcConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.medapp.repository.jpa", transactionManagerRef = "medappJpaTransactionManager", entityManagerFactoryRef = "medappEntityManagerFactoryRef")
@Import(JdbcConfiguration.class)
@EnableJpaAuditing
public class MedappDatabaseConfiguration {

	@Bean(name = "medappDataSource")
	@ConfigurationProperties(prefix = "datasources.medapp")
	public DataSource medappDataSource() {
		return DataSourceBuilder.create().build();
	}

	@DependsOn("flywayInitializerOne")
	@Bean("medappEntityManagerFactoryRef")
	public LocalContainerEntityManagerFactoryBean medappEntityManagerFactoryRef() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.POSTGRESQL);
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(medappDataSource());
		em.setPackagesToScan("com.medapp.domain");
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaPropertyMap(hibernateProperties());
		em.afterPropertiesSet();

		return em;
	}

	@DependsOn("flywayInitializerOne")
	@Bean("medappEntityManagerFactoryRef")
	public LocalContainerEntityManagerFactoryBean medappEntityManagerFactoryRefBatch() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.POSTGRESQL);
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(medappDataSource());
		em.setPackagesToScan("com.medapp.domain");
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaPropertyMap(hibernateProperties());
		em.afterPropertiesSet();

		return em;
	}
	private Map<String, Object> hibernateProperties() {

		Resource resource = new ClassPathResource("hibernate.properties");

		try {
			Properties properties = PropertiesLoaderUtils.loadProperties(resource);
			return properties.entrySet().stream()
					.collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue()));
		} catch (IOException e) {
			return new HashMap<String, Object>();
		}
	}

	// flyway config
	@Bean(name = "flywayConfiguration")
	@ConfigurationProperties(prefix = "spring.flyway.medapp")
	public ClassicConfiguration flywayConfiguration() {
		return new ClassicConfiguration();
	}

	@Bean(name = "flywaySyrusAdminDatabase")
	public Flyway flywaySyrusAdminDatabase(ClassicConfiguration flywayConfiguration) {
		return Flyway.configure().configuration(flywayConfiguration).dataSource(this.medappDataSource()).load();
	}

	@Bean
	public FlywayMigrationInitializer flywayInitializerOne(
			@Qualifier("flywaySyrusAdminDatabase") Flyway flywaySyrusAdminDatabase) {
		return new FlywayMigrationInitializer(flywaySyrusAdminDatabase, null);
	}

	@Bean("medappJpaTransactionManager")
	public JpaTransactionManager medappJpaTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(medappEntityManagerFactoryRef().getObject());

		return transactionManager;
	}
	@Bean("medappJpaTransactionManagerBatch")
	public JpaTransactionManager medappJpaTransactionManagerBatch() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(medappEntityManagerFactoryRefBatch().getObject());

		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Primary
	@Bean(name = "medappJpaTransactionManager")
	public PlatformTransactionManager medappTransactionManager() {
		return new DataSourceTransactionManager(medappDataSource());
	}

	@Bean
	public AuditorAware<String> securityBasedAuditorAware() {
		return new SpringSecurityAuditorAware();

	}

}

class SpringSecurityAuditorAware implements AuditorAware<String> {

	public Optional<String> getCurrentAuditor() {

		return Optional.ofNullable(SecurityContextHolder.getContext()).map(SecurityContext::getAuthentication)
				.filter(Authentication::isAuthenticated).map(Authentication::getPrincipal).map(String.class::cast);
	}
}