package com.medapp.configuration;

import com.medapp.configuration.security.JwtAuthenticationFilter;
import com.medapp.configuration.security.JwtAuthorizationFilter;
import com.medapp.service.impl.SSOAuthenticationProvider;
import com.medapp.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.LinkedList;
import java.util.List;


@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	private final UserDetailsServiceImpl userDetailsService;

	public WebSecurityConfiguration(UserDetailsServiceImpl userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
	
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
        http
	        .cors()
	        .and()
	        .csrf().disable()
	        .authorizeRequests()
	        .antMatchers("/api/legacy*",
					"/api/authenticate",
					"/swagger-ui.html",
					"/webjars/**",
					"/v2/**",
					"/swagger-resources/**",
					"/api/files/**")
	        .permitAll()
	        .anyRequest().authenticated()
	        .and()
	        .addFilter(new JwtAuthenticationFilter(authenticationManager()))
	        .addFilter(new JwtAuthorizationFilter(authenticationManager()))
	        // this disables session creation on Spring Security
	        .sessionManagement()
	        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
    
    
    /** a method where we defined a custom implementation of UserDetailsService to load user-specific data in the security
   	 *  framework. We have also used this method to set the encrypt method used by our application (BCryptPasswordEncoder).
   	 */
   
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
    	  auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    	  auth.authenticationProvider(ssoAuthenticationProvider());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    
     /** a method where we can allow/restrict our CORS support. 
	 * In our case we left it wide open by permitting requests from any source (/**).
	 */
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());

        return source;
    }
    
	@Bean
	public ProviderManager providerManager() {
		List<AuthenticationProvider> providers = new LinkedList<AuthenticationProvider>();
		providers.add(ssoAuthenticationProvider());
	

		ProviderManager pm = new ProviderManager(providers);
		pm.setAuthenticationEventPublisher(new DefaultAuthenticationEventPublisher(applicationEventPublisher));
		return pm;
	}

	@Bean
	public SSOAuthenticationProvider ssoAuthenticationProvider() {
		return new SSOAuthenticationProvider();
	}
	
	
	  
	   
}
    
	
 

   





