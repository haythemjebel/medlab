package com.medapp.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = true)
public class ApplicationProperties {
	private final Projet environnement = new Projet();
	private final Http http = new Http();
	private final Swagger swagger = new Swagger();
	private final CorsConfiguration cors = new CorsConfiguration();
	private final PowerbiConfiguration powerbiConfiguration = new PowerbiConfiguration();
	private final Email email = new Email();
	private static ApplicationProperties applicationProperties;
	private Document document = new Document();
	private SSO sso = new SSO();
	@Value("tmp-directory")
	private String urlUpload;

	public String getUrlUpload() {
		return urlUpload;
	}

	public void setUrlUpload(String urlUpload) {
		this.urlUpload = urlUpload;
	}

	public SSO getSso() {
		return sso;
	}

	public void setSso(SSO sso) {
		this.sso = sso;
	}

	public static ApplicationProperties getInstance() {
		return applicationProperties;
	}

	public ApplicationProperties() {
		applicationProperties = this;
	}

	public Projet getProjet() {
		return environnement;
	}

	public Http getHttp() {
		return http;
	}

	public Swagger getSwagger() {
		return swagger;
	}

	public Email getEmail() {
		return email;
	}

	public PowerbiConfiguration getPowerbiConfiguration() {
		return powerbiConfiguration;
	}

	public CorsConfiguration getCors() {
		return cors;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}


	public static class Projet {
		private String applicationName = ApplicationDefaults.Project.applicationName;
		private String applicationVersion = ApplicationDefaults.Project.applicationVersion;
		private String environnement = ApplicationDefaults.Project.environnement;

		public String getApplicationName() {
			return applicationName;
		}

		public void setApplicationName(String applicationName) {
			this.applicationName = applicationName;
		}

		public String getApplicationVersion() {
			return applicationVersion;
		}

		public void setApplicationVersion(String applicationVersion) {
			this.applicationVersion = applicationVersion;
		}

		public String getEnvironnement() {
			return environnement;
		}

		public void setEnvironnement(String environnement) {
			this.environnement = environnement;
		}

	}

	public static class Swagger {

		private final String title = ApplicationDefaults.Swagger.title;
		private final String version = ApplicationDefaults.Swagger.version;
		private final String description = ApplicationDefaults.Swagger.description;
		private final String termsOfServiceUrl = ApplicationDefaults.Swagger.termsOfServiceUrl;
		private final String contactName = ApplicationDefaults.Swagger.contactName;
		private final String licence = ApplicationDefaults.Swagger.license;
		private final String licenceUrl = ApplicationDefaults.Swagger.licenseUrl;
		private final String contactUrl = ApplicationDefaults.Swagger.contactUrl;
		private final String contactEmail = ApplicationDefaults.Swagger.contactEmail;
		private final String defaultIncludePattern = ApplicationDefaults.Swagger.defaultIncludePattern;

		public String getTitle() {
			return title;
		}

		public String getVersion() {
			return version;
		}

		public String getDescription() {
			return description;
		}

		public String getTermsOfServiceUrl() {
			return termsOfServiceUrl;
		}

		public String getContactName() {
			return contactName;
		}

		public String getLicense() {
			return licence;
		}

		public String getLicenseUrl() {
			return licenceUrl;
		}

		public String getContactUrl() {
			return contactUrl;
		}

		public String getContactEmail() {
			return contactEmail;
		}

		public String getDefaultIncludePattern() {
			return defaultIncludePattern;
		}
	}

	public static class Http {
		private final Cache cache = new Cache();

		public Cache getCache() {
			return cache;
		}

		public static class Cache {

			private int timeToLiveInDays = ApplicationDefaults.Http.Cache.timeToLiveInDays;

			public int getTimeToLiveInDays() {
				return timeToLiveInDays;
			}

			public void setTimeToLiveInDays(int timeToLiveInDays) {
				this.timeToLiveInDays = timeToLiveInDays;
			}
		}
	}

	public static class PowerbiConfiguration {
		private String authority;
		private String resource;
		private String clientId;
		private String username;
		private String password;

		public String getAuthority() {
			return authority;
		}

		public void setAuthority(String authority) {
			this.authority = authority;
		}

		public String getResource() {
			return resource;
		}

		public void setResource(String resource) {
			this.resource = resource;
		}

		public String getClientId() {
			return clientId;
		}

		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

	public static class Email {
		private String from;
		private String sender;

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public String getSender() {
			return sender;
		}

		public void setSender(String sender) {
			this.sender = sender;
		}
	}

	public static class SerensiaConfiguration {
		private String host;
		private String authentificationUri;
		private String factureUri;
		private String clientId;
		private String clientSecret;

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public String getClientId() {
			return clientId;
		}

		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

		public String getClientSecret() {
			return clientSecret;
		}

		public void setClientSecret(String clientSecret) {
			this.clientSecret = clientSecret;
		}

		public String getAuthentificationUri() {
			return authentificationUri;
		}

		public void setAuthentificationUri(String authentificationUri) {
			this.authentificationUri = authentificationUri;
		}

		public String getFactureUri() {
			return factureUri;
		}

		public void setFactureUri(String factureUri) {
			this.factureUri = factureUri;
		}

		public String getAuthentificationUrl() {
			return host + "/" + authentificationUri;
		}

		public String getFactureUrl() {
			return host + "/" + factureUri;
		}
	}

	public static class CourrierConfiguration {
		private String host;
		private String password;

		public String getHost() {
			return host;
		}

		public String getPassword() {
			return password;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public void setPassword(String password) {
			this.password = password;
		}

	}

	public static class Document {
		private String separator;

		public String getSeparator() {
			return separator;
		}

		public void setSeparator(String separator) {
			this.separator = separator;
		}
	}

	public static class SSO {
		private String token;

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

	}





}
