package com.medapp.configuration;

public interface ApplicationConstants {
    String SPRING_PROFILE_DEVELOPMENT = "dev";
    String SPRING_PROFILE_TEST = "test";
    String SPRING_PROFILE_PRODUCTION = "prod";
    String SPRING_PROFILE_AWS_ECS = "aws-ecs";
    String SPRING_PROFILE_SWAGGER = "swagger";  
}
