package com.medapp.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medapp.domain.PatientImportConfig;
import com.medapp.service.PatientImportConfigService;
import com.medapp.service.dto.PatientImportConfigDTO;
import com.medapp.web.mapper.Mapper;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api")
@Api(value = "/ImportConfigs", description = "gestion des Configurations Import Patients")
public class PatientImportConfigController {

	private final Logger logger = LoggerFactory.getLogger(PatientImportConfigController.class);

	@Autowired
	private Mapper<PatientImportConfig, PatientImportConfigDTO> PatientImportConfigMapper;
	
	@Inject
	private PatientImportConfigService patientImportConfigService;
	
	@GetMapping(value = "/ImportConfigs")
	public ResponseEntity<List<PatientImportConfigDTO>> getAllPatientImportConfigs() {
		logger.debug("List des patients...");

		 List<PatientImportConfig> allImportConfigs = patientImportConfigService.getAllImportConfigs();

		return ResponseEntity.ok().body(PatientImportConfigMapper.toDtoList(allImportConfigs));
	}
}
