package com.medapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medapp.service.ProfileService;
import com.medapp.service.dto.PageRequestByCriteria;
import com.medapp.service.dto.PageResponse;
import com.medapp.service.dto.ProfileCriteria;
import com.medapp.service.dto.ProfileDTO;
import com.medapp.service.dto.ProfileLazyDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api")
@Api(value="/profiles", description="gestion des profiles")
public class ProfileController {
	private static Logger LOG = LoggerFactory.getLogger(ProfileController.class);
	
	@Inject 
	private  ProfileService profileService;
	
	@ApiOperation(value="Retourner la list des profil en mode lazy", response=PageResponse.class)
	@ApiResponses(value={
			@ApiResponse( code=200, message="OK"),
			@ApiResponse( code=400, message="KO !!!")
			
	})
    @PostMapping(value = "/profiles", headers = "X-action=search")
    public PageResponse<ProfileLazyDTO> getAllProfiles(@RequestBody PageRequestByCriteria<ProfileCriteria> pageBycriteria) {
		final Long countTotal = profileService.countAllProfiles(pageBycriteria.criteria);
        final Page<ProfileLazyDTO> page = profileService.getAllProfiles(pageBycriteria.toPageable(), pageBycriteria.criteria);
        PageResponse<ProfileLazyDTO> response = new PageResponse<ProfileLazyDTO>(
				((countTotal)/ pageBycriteria.lazyLoadEvent.rows) + (countTotal  %  pageBycriteria.lazyLoadEvent.rows), 
				countTotal, page.getContent());
		return response;
    }
	
	
	 @PostMapping(value = "/profiles", headers = "X-action=create")
	 @ApiOperation(value="Create profile")
	 public ResponseEntity<ProfileDTO> createProfile(@Valid @RequestBody ProfileDTO profileDTO) throws URISyntaxException {
	        LOG.debug("REST request to save Profile : {}", profileDTO);

	        if (profileDTO.getId() != null) {
	            return ResponseEntity.badRequest().header("X-validation-error", "error.idexists").build();
	        } else if (profileService.isCodeMetierUnique(profileDTO)) {
	        	 return ResponseEntity.badRequest().header("X-validation-error", "error.codeMetierAlreadyUsed").build();
	             
	        } else {
	            ProfileDTO newProfile  = profileService.createProfile(profileDTO);
	   
	            return ResponseEntity.created(new URI("/api/profiles/" + newProfile.getId())).body(newProfile);
	        }
	    }
	 
	 @PutMapping("/profiles")
	 @ApiOperation(value="Update Profile")
	 public ResponseEntity<ProfileDTO> updateProfile(@Valid @RequestBody ProfileDTO profileDTO) {
	        LOG.debug("REST request to update Profile : {}", profileDTO);
	        ProfileDTO existingProfile = profileService.findOneById(profileDTO.getId());
	        if(existingProfile == null ) {
	        	return ResponseEntity.notFound().header("X-validation-error", "error.profileDoesNotExists").build();
	        }
	        if (existingProfile != null && (!existingProfile.getCodeMetier().equals(profileDTO.getCodeMetier()))) {
             if(profileService.isCodeMetierUnique(profileDTO)) {
     	        return ResponseEntity.badRequest().header("X-validation-error", "error.codeMetierAlreadyUsed").build();	 
             }  
	        }
	        ProfileDTO updatedProfile = profileService.updateProfile(profileDTO);
	        if(updatedProfile == null) ResponseEntity.notFound();
	        return ResponseEntity.status(HttpStatus.OK).body(updatedProfile);
	    }
	 
	 
	    @GetMapping("/profiles/{id}")
	    @ApiOperation(value="Requêter un profile par un id unique")
	    public ResponseEntity<ProfileDTO> getProfile(@PathVariable Long id) {
	        LOG.debug("REST request to get Profile : {}", id);
	        ProfileDTO res=  profileService.findOneById(id);
	        return res != null ? ResponseEntity.ok(res) : ResponseEntity.notFound().build(); 
	    }

	
}
