package com.medapp.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medapp.domain.RendezVous;
import com.medapp.service.RendezVousService;
import com.medapp.service.dto.RendezVousDTO;
import com.medapp.web.mapper.Mapper;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api")
@Api(value = "/rendezVous", description = "gestion des rendez vous")
public class RendezVousController {

	private final Logger logger = LoggerFactory.getLogger(RendezVousController.class);

	@Autowired
	private Mapper<RendezVous, RendezVousDTO> rendezVousMapper;

	@Inject
	private RendezVousService rendezVousService;
	
	
	@GetMapping("/rendezVous/{id}")
	public ResponseEntity<List<RendezVousDTO>> getPatient(@PathVariable Long id) {
		logger.debug("REST request to get rendez Vous : {}", id);

		List<RendezVous> allRendezVous = rendezVousService.findRendezVousById(id);

		return ResponseEntity.ok().body(rendezVousMapper.toDtoList(allRendezVous));
	}

	


}
