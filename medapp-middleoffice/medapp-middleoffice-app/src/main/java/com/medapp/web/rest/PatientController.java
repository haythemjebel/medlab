package com.medapp.web.rest;

import com.medapp.configuration.ApplicationProperties;
import com.medapp.domain.Patient;
import com.medapp.domain.PatientImportConfig;
import com.medapp.service.PatientImportConfigService;
import com.medapp.service.PatientService;
import com.medapp.service.StorageService;
import com.medapp.service.dto.PageRequestByCriteria;
import com.medapp.service.dto.PageResponse;
import com.medapp.service.dto.PatientDTO;
import com.medapp.support.ExcelSheetInfo;
import com.medapp.web.mapper.Mapper;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@Api(value = "/patients", description = "gestion des patients")
public class PatientController {

	private final Logger logger = LoggerFactory.getLogger(PatientController.class);

	@Autowired
	private Mapper<Patient, PatientDTO> PatientMapper;

	@Inject
	private PatientService patientService;
	@Inject
	private StorageService storageService;
	@Inject
	private PatientImportConfigService patientImportConfigService;
	@Autowired
	JobLauncher jobLauncher;
	@Autowired
	Job job;
	List<String> files = new ArrayList<String>();
	private final Path rootLocation = Paths.get("upload-dir");

	@GetMapping(value = "/patients")
	public ResponseEntity<List<PatientDTO>> getAllPatients() {
		logger.debug("List des patients...");

		List<Patient> allPatients = patientService.getAllPatients();

		return ResponseEntity.ok().body(PatientMapper.toDtoList(allPatients));
	}

	@PostMapping(value = "/patients", headers = "X-action=create")
	public ResponseEntity<Patient> createPatient(@RequestBody PatientDTO patientDTO) throws URISyntaxException {
		logger.debug("REST request to save Patient : {}", patientDTO);
		patientDTO.setFicheMedicaleNumber(
				patientDTO.getNom().substring(0, 3).concat(patientDTO.getCnamNum().substring(0, 3).toUpperCase()));

		patientService.createPatient(PatientMapper.fromOToI(patientDTO));

		return ResponseEntity.created(new URI("/api/patients/")).body(PatientMapper.fromOToI(patientDTO));
	}

	@PutMapping("/patients")
	public ResponseEntity<PatientDTO> updatePatient(@Valid @RequestBody PatientDTO patientDTO)
			throws URISyntaxException {
		logger.debug("REST request to update Patient : {}", patientDTO);
		if (patientDTO.getId() != null) {
			patientService.createPatient(PatientMapper.fromOToI(patientDTO));
		}
		return ResponseEntity.ok().body(patientDTO);
	}

	@GetMapping("/patients/{id}")
	public PatientDTO getPatient(@PathVariable Long id) {
		logger.debug("REST request to get patient : {}", id);

		Optional<Patient> Patient = patientService.findById(id);

		return PatientMapper.fromIToO(Patient.get());
	}

	@DeleteMapping("/patients/{id}")
	public void deletePatient(@PathVariable Long id) {
		logger.debug("REST request to delete patient : {}", id);

		patientService.deleteById(id);

	}

	@PostMapping("/patients/run")
	public ResponseEntity<ExcelSheetInfo> handleFileUpload(@RequestParam("file") List<MultipartFile> file)
			throws Exception {
		for (MultipartFile multipartFile : file) {
			String message = "";

			String currentDate = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
			MultipartFile newFile = getNewFile(currentDate + "_" + multipartFile.getOriginalFilename(), multipartFile);
			storageService.store(newFile);

			message = "You successfully uploaded " + multipartFile.getOriginalFilename() + "!";
			String resolve = ApplicationProperties.getInstance().getUrlUpload() + File.separatorChar
					+ newFile.getOriginalFilename();
			PatientImportConfig patientImportConfig = new PatientImportConfig();
			patientImportConfig.setDateImport(new Date());
			patientImportConfig.setUrlFile(resolve);
			patientImportConfig.setStatus("En cours");
			patientImportConfig.setNom(multipartFile.getOriginalFilename());
			PatientImportConfig patientImportConfigToSave = patientImportConfigService
					.createPatientImportConfig(patientImportConfig);

			System.out.println("Job Started at :" + new Date());

			JobParameters param = new JobParametersBuilder()
					.addString("ImportConfig", String.valueOf(System.currentTimeMillis())).addString("urlFile", resolve)
					.toJobParameters();

			JobExecution execution = jobLauncher.run(job, param);

			System.out.println("Job finished with status :" + execution.getStatus());

			patientImportConfigToSave.setStatus(execution.getStatus().toString());
			patientImportConfigService.createPatientImportConfig(patientImportConfigToSave);
		}
		return ResponseEntity.status(HttpStatus.OK).body(null);

	}

	private MultipartFile getNewFile(String fileName, MultipartFile currentFile) {
		return new MultipartFile() {
			@Override
			public String getName() {
				return currentFile.getName();
			}

			@Override
			public String getOriginalFilename() {
				return fileName;
			}

			@Override
			public String getContentType() {
				return currentFile.getContentType();
			}

			@Override
			public boolean isEmpty() {
				return currentFile.isEmpty();
			}

			@Override
			public long getSize() {
				return currentFile.getSize();
			}

			@Override
			public byte[] getBytes() throws IOException {
				return currentFile.getBytes();
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return currentFile.getInputStream();
			}

			@Override
			public void transferTo(File file) throws IOException, IllegalStateException {

			}
		};
	}

}
