package com.medapp.web.mapper;

import com.medapp.domain.Patient;
import com.medapp.domain.RendezVous;
import com.medapp.service.dto.PatientDTO;
import com.medapp.service.dto.RendezVousDTO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class RendezVousMapper implements Mapper<RendezVous, RendezVousDTO> {

	@Override
	public RendezVousDTO fromIToO(RendezVous i) {
		RendezVousDTO p = new RendezVousDTO();
		p.setId(i.getId());
		p.setDateRendezVous(i.getDateRendezVous());
		p.setDiagnosticAndTraittement(i.getDiagnosticAndTraittement());
		p.setDureeRendezVous(i.getDureeRendezVous());
		p.setTypeRendezVous(i.getTypeRendezVous());
		return p;
	}

	@Override
	public RendezVous fromOToI(RendezVousDTO i) {
		RendezVous p = new RendezVous();
		p.setId(i.getId());
		p.setDateRendezVous(i.getDateRendezVous());
		p.setDiagnosticAndTraittement(i.getDiagnosticAndTraittement());
		p.setDureeRendezVous(i.getDureeRendezVous());
		p.setTypeRendezVous(i.getTypeRendezVous());
		return p;
	}

	@Override
	public List<RendezVousDTO> toDtoList(List<RendezVous> i) {
		List<RendezVousDTO> list = new ArrayList<RendezVousDTO>();
		if (i != null) {
			for (int j = 0; j < i.size(); j++) {
				RendezVousDTO p = new RendezVousDTO();
				p.setId(i.get(j).getId());
				p.setDateRendezVous(i.get(j).getDateRendezVous());
				p.setDiagnosticAndTraittement(i.get(j).getDiagnosticAndTraittement());
				p.setDureeRendezVous(i.get(j).getDureeRendezVous());
				p.setTypeRendezVous(i.get(j).getTypeRendezVous());
				list.add(p);
			}
		}
		return list;
	}

	@Override
	public List<RendezVous> toEntityList(List<RendezVousDTO>i) {
		List<RendezVous> list = new ArrayList<RendezVous>();
		if (i != null) {
			for (int j = 0; j < i.size(); j++) {
				RendezVous c = new RendezVous();
				c.setId(i.get(j).getId());
				c.setDiagnosticAndTraittement(i.get(j).getDiagnosticAndTraittement());
				c.setDureeRendezVous(i.get(j).getDureeRendezVous());
				c.setTypeRendezVous(i.get(j).getTypeRendezVous());
				list.add(c);
			}
		}
		return list;
	}


}
