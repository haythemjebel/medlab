package com.medapp.web.rest;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medapp.service.PermissionService;
import com.medapp.service.dto.PageRequest;
import com.medapp.service.dto.PageResponse;
import com.medapp.service.dto.PermissionDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api")
@Api(value="/permissions", description="permissions")
public class PermissionController {
	private static Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Inject 
	private  PermissionService permissionService;
	
	
	@ApiOperation(value="Retourner la list des permissions en mode lazy", response=PageResponse.class)
	@ApiResponses(value={
			@ApiResponse( code=200, message="OK"),
			@ApiResponse( code=400, message="KO !!!")
		
	})
    @PostMapping(value = "/permissions", headers = "X-action=search")
	public PageResponse<PermissionDTO>  getAllPermissions(@RequestBody PageRequest pageRequest) {
		final Long countTotal = permissionService.countAllPermissions();
        final Page<PermissionDTO> page = permissionService.getAllPermissions(pageRequest.toPageable());
        PageResponse<PermissionDTO> response = new PageResponse<PermissionDTO>(
				((countTotal)/ pageRequest.lazyLoadEvent.rows) + (countTotal  %  pageRequest.lazyLoadEvent.rows), 
				countTotal, page.getContent());
		return response;
		
	}
	
	 @GetMapping("/permissions/{id}")
	    @ApiOperation(value="Requêter une permission par un id unique")
	    public ResponseEntity<PermissionDTO> getPermission(@PathVariable Long id) {
	        log.debug("REST request to get Permission : {}", id);
	        PermissionDTO res=  permissionService.findOneById(id);
	        return res != null ? ResponseEntity.ok(res) : ResponseEntity.notFound().build(); 
	    }
	 
	 @GetMapping("/permissionsgroup")
	 @ApiOperation("requeter les groupes des permissions")
	 public ResponseEntity<List<String>> getAllPermissionsGroup(){
		 List<String> res = permissionService.findAllGroup();
		 return res != null ? ResponseEntity.ok(res) : ResponseEntity.notFound().build(); 
	 }
	 
	 @GetMapping("/permissionbygroup")
	 public ResponseEntity<List<PermissionDTO>> getPermissionByGroup(@RequestParam String group){
		 List<PermissionDTO> res= permissionService.findPermissionsByGroup(group);
		 return res != null ?  ResponseEntity.ok(res) : ResponseEntity.notFound().build();
	 }
		

}
