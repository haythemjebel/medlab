package com.medapp.web.mapper;

import com.medapp.domain.Patient;
import com.medapp.service.dto.PatientDTO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class PatientMapper implements Mapper<Patient, PatientDTO> {

	@Override
	public PatientDTO fromIToO(Patient i) {
		PatientDTO p = new PatientDTO();
		p.setId(i.getId());
		p.setNom(i.getNom());
		p.setPrenom(i.getPrenom());
		p.setEmail(i.getEmail());
		p.setAdresse(i.getAdresse());
		p.setAdressePar(i.getAdressePar());
		p.setCreatedDate(i.getCreatedDate());
		p.setCreatedBy(i.getCreatedBy());
		p.setLastModifiedBy(i.getLastModifiedBy());
		p.setLastModifiedDate(i.getLastModifiedDate());
		p.setDateNaissance(i.getDateNaissance());
		p.setAtcds(i.getAtcds());
		p.setCnamNum(i.getCnamNum());
		p.setFicheMedicaleNumber(i.getFicheMedicaleNumber());
		p.setDiagnostic(i.getDiagnostic());
		p.setTel(i.getTel());
		p.setProfession(i.getProfession());
		return p;
	}

	@Override
	public Patient fromOToI(PatientDTO i) {
		Patient p = new Patient();
		p.setId(i.getId());
		p.setNom(i.getNom());
		p.setPrenom(i.getPrenom());
		p.setEmail(i.getEmail());
		p.setAdresse(i.getAdresse());
		p.setAdressePar(i.getAdressePar());
		p.setCreatedDate(i.getCreatedDate());
		p.setCreatedBy(i.getCreatedBy());
		p.setLastModifiedBy(i.getLastModifiedBy());
		p.setLastModifiedDate(i.getLastModifiedDate());
		p.setDateNaissance(i.getDateNaissance());
		p.setAtcds(i.getAtcds());
		p.setCnamNum(i.getCnamNum());
		p.setFicheMedicaleNumber(i.getFicheMedicaleNumber());
		p.setDiagnostic(i.getDiagnostic());
		p.setTel(i.getTel());
		p.setProfession(i.getProfession());
		return p;
	}

	@Override
	public List<PatientDTO> toDtoList(List<Patient> i) {
		List<PatientDTO> list = new ArrayList<PatientDTO>();
		if (i != null) {
			for (int j = 0; j < i.size(); j++) {
				PatientDTO c = new PatientDTO();
				c.setId(i.get(j).getId());
				c.setNom(i.get(j).getNom());
				c.setAdresse(i.get(j).getAdresse());
				c.setAdressePar(i.get(j).getAdressePar());
				c.setPrenom(i.get(j).getPrenom());
				c.setProfession(i.get(j).getProfession());
				c.setEmail(i.get(j).getEmail());
				c.setCreatedDate(i.get(j).getCreatedDate());
				c.setCreatedBy(i.get(j).getCreatedBy());
				c.setLastModifiedBy(i.get(j).getLastModifiedBy());
				c.setLastModifiedDate(i.get(j).getLastModifiedDate());
				c.setDateNaissance(i.get(j).getDateNaissance());
				c.setAtcds(i.get(j).getAtcds());
				c.setCnamNum(i.get(j).getCnamNum());
				c.setFicheMedicaleNumber(i.get(j).getFicheMedicaleNumber());
				c.setDiagnostic(i.get(j).getDiagnostic());
				c.setTel(i.get(j).getTel());
				list.add(c);
			}
		}
		return list;
	}

	@Override
	public List<Patient> toEntityList(List<PatientDTO>i) {
		List<Patient> list = new ArrayList<Patient>();
		if (i != null) {
			for (int j = 0; j < i.size(); j++) {
				Patient c = new Patient();
				c.setId(i.get(j).getId());
				c.setNom(i.get(j).getNom());
				c.setAdresse(i.get(j).getAdresse());
				c.setAdressePar(i.get(j).getAdressePar());
				c.setPrenom(i.get(j).getPrenom());
				c.setProfession(i.get(j).getProfession());
				c.setEmail(i.get(j).getEmail());
				c.setCreatedDate(i.get(j).getCreatedDate());
				c.setCreatedBy(i.get(j).getCreatedBy());
				c.setLastModifiedBy(i.get(j).getLastModifiedBy());
				c.setLastModifiedDate(i.get(j).getLastModifiedDate());
				c.setDateNaissance(i.get(j).getDateNaissance());
				c.setAtcds(i.get(j).getAtcds());
				c.setCnamNum(i.get(j).getCnamNum());
				c.setFicheMedicaleNumber(i.get(j).getFicheMedicaleNumber());
				c.setDiagnostic(i.get(j).getDiagnostic());
				c.setTel(i.get(j).getTel());
				list.add(c);
			}
		}
		return list;
	}


}
