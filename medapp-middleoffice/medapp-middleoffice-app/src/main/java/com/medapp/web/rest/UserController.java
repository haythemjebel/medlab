package com.medapp.web.rest;

import com.medapp.service.UserService;
import com.medapp.service.dto.PageRequestByCriteria;
import com.medapp.service.dto.PageResponse;
import com.medapp.service.dto.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
@Api(value = "/users", description = "gestion d'utilisateurs")
public class UserController {
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Inject
    private UserService userService;

    @ApiOperation(value = "Requêter la liste d'utilisateur en mode lazy", response = PageResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something bad happened !!!")

    })
    @PostMapping(value = "/users", headers = "X-action=search")
    public PageResponse<UserDTO> getAllUsers(@RequestBody PageRequestByCriteria<UserDTO> pageBycriteria) throws Exception {

        final Long countTotal = userService.countAllUsers(pageBycriteria.criteria);
        final Page<UserDTO> page = userService.getAllUsers(pageBycriteria.toPageable(), pageBycriteria.criteria);

        PageResponse<UserDTO> response = new PageResponse<UserDTO>(
                ((countTotal) / pageBycriteria.lazyLoadEvent.rows) + (countTotal % pageBycriteria.lazyLoadEvent.rows),
                countTotal, page.getContent());
        return response;

    }
	

    @PostMapping(value = "/users", headers = "X-action=create")
    @ApiOperation(value="Create user")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException {
        LOG.debug("REST request to save User : {}", userDTO);

        if (userDTO.getId() != null) {
            return ResponseEntity.badRequest().header("X-validation-error", "error.idexists").build();
        } else if (userService.isLoginUnique(userDTO)) {
            return ResponseEntity.badRequest().header("X-validation-error", "error.emailAlreadyUsed").build();
        } else if (userService.isIdentifiantUnique(userDTO)) {
            return ResponseEntity.badRequest().header("X-validation-error", "error.identifiantAlreadyUsed").build();
        } else {
        	UserDTO newUser = userService.createUser(userDTO);
            // [TODO] send subscription Email here
            return ResponseEntity.created(new URI("/api/users/" + newUser.getId())).body(newUser);
        }
    }


    @PutMapping("/users")
    //@PreAuthorize
    @ApiOperation(value="Requêter un utilisateur par u nid unique")
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) {
    	LOG.debug("REST request to update User : {}", userDTO);
        UserDTO existingUser = userService.findOneById(userDTO.getId());
        if(existingUser == null ) {
        	return ResponseEntity.notFound().header("X-validation-error", "error.userDoesNotExists").build();
        }
        if (existingUser != null && (!existingUser.getId().equals(userDTO.getId()))) {
        	return ResponseEntity.badRequest().header("X-validation-error", "error.emailAlreadyUsed").build();
        }
        UserDTO updatedUser = userService.updateUser(userDTO);
        if(updatedUser == null) ResponseEntity.notFound();
        return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
    }
    
    @PutMapping("/users/{id}/pwd")
    public ResponseEntity<UserDTO> resetUserPassword(@PathVariable Long id){
    	UserDTO user=  userService.findOneById(id);
    	UserDTO userPwdUpdated = userService.resetPassword(user);
        if(userPwdUpdated == null) return ResponseEntity.badRequest().header("X-password-error", "error.passwordNotChanged").build(); 
    	return ResponseEntity.ok().body(userPwdUpdated);
    }

    /**
     * Gets a list of all roles.
     * @return a string list of all roles.
     */
    @GetMapping("/users/authorities")
    @ApiOperation(value="Requêter un utilisateur par u nid unique")
    public List<String> getAuthorities() {
        return null;
    }

    /**
     * {@code GET /users/:login} : get the "login" user.
     *
     * @param login the login of the user to find.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the "login" user, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/users/{id}")
    @ApiOperation(value="Requêter un utilisateur par u nid unique")
    public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
        LOG.debug("REST request to get User : {}", id);
        UserDTO res=  userService.findOneById(id);
        return res != null ? ResponseEntity.ok(res) : ResponseEntity.notFound().build(); 
    }

    /**
     * {@code DELETE /users/:login} : delete the "login" User.
     *
     * @param login the login of the user to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/users/{id}")
    @ApiOperation(value="Requêter un utilisateur par u nid unique")
    public ResponseEntity<Void> deleteUser(@PathVariable String login) {
        LOG.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
        return ResponseEntity.noContent().build();
    }
    


    
}
		