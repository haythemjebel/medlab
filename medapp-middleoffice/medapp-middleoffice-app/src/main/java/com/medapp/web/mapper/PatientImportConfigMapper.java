package com.medapp.web.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.medapp.domain.PatientImportConfig;
import com.medapp.service.dto.PatientImportConfigDTO;

@Component
public class PatientImportConfigMapper implements Mapper<PatientImportConfig, PatientImportConfigDTO> {

	@Override
	public PatientImportConfigDTO fromIToO(PatientImportConfig i) {
		PatientImportConfigDTO p = new PatientImportConfigDTO();
		p.setId(i.getId());
		p.setCreatedBy(i.getCreatedBy());
		p.setCreatedDate(i.getCreatedDate());
		p.setDateImport(i.getDateImport());
		p.setLastModifiedBy(i.getLastModifiedBy());
		p.setLastModifiedDate(i.getLastModifiedDate());
		p.setUrlFile(i.getUrlFile());
		p.setStatus(i.getStatus());
		p.setNom(i.getNom());
		return p;
	}

	@Override
	public PatientImportConfig fromOToI(PatientImportConfigDTO i) {
		PatientImportConfig p = new PatientImportConfig();
		p.setId(i.getId());
		p.setCreatedBy(i.getCreatedBy());
		p.setCreatedDate(i.getCreatedDate());
		p.setDateImport(i.getDateImport());
		p.setLastModifiedBy(i.getLastModifiedBy());
		p.setLastModifiedDate(i.getLastModifiedDate());
		p.setUrlFile(i.getUrlFile());
		p.setStatus(i.getStatus());
		p.setNom(i.getNom());
		return p;
	}

	@Override
	public List<PatientImportConfigDTO> toDtoList(List<PatientImportConfig> i) {
		List<PatientImportConfigDTO> list = new ArrayList<PatientImportConfigDTO>();
		if (i != null) {
			for (int j = 0; j < i.size(); j++) {
				PatientImportConfigDTO p = new PatientImportConfigDTO();
				p.setId(i.get(j).getId());
				p.setCreatedBy(i.get(j).getCreatedBy());
				p.setCreatedDate(i.get(j).getCreatedDate());
				p.setDateImport(i.get(j).getDateImport());
				p.setLastModifiedBy(i.get(j).getLastModifiedBy());
				p.setLastModifiedDate(i.get(j).getLastModifiedDate());
				p.setUrlFile(i.get(j).getUrlFile());
				p.setStatus(i.get(j).getStatus());
				p.setNom(i.get(j).getNom());
				list.add(p);
			}
		}
		return list;
	}

	@Override
	public List<PatientImportConfig> toEntityList(List<PatientImportConfigDTO> i) {
		List<PatientImportConfig> list = new ArrayList<PatientImportConfig>();
		if (i != null) {
			for (int j = 0; j < i.size(); j++) {
				PatientImportConfig p = new PatientImportConfig();
				p.setId(i.get(j).getId());
				p.setCreatedBy(i.get(j).getCreatedBy());
				p.setCreatedDate(i.get(j).getCreatedDate());
				p.setDateImport(i.get(j).getDateImport());
				p.setLastModifiedBy(i.get(j).getLastModifiedBy());
				p.setLastModifiedDate(i.get(j).getLastModifiedDate());
				p.setUrlFile(i.get(j).getUrlFile());
				p.setStatus(i.get(j).getStatus());
				p.setNom(i.get(j).getNom());
				list.add(p);
			}
		}
		return list;
	}

}
