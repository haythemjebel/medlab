package com.medapp.web.mapper;

import java.util.List;

public interface Mapper<I, O> {
	O fromIToO(I i);
	I fromOToI(O o);
	List<O> toDtoList(List<I> i);
	List<I> toEntityList(List<O> o);
}
