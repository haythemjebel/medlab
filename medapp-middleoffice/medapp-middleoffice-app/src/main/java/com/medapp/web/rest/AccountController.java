package com.medapp.web.rest;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.medapp.service.UserService;
import com.medapp.service.dto.PasswordChangeDTO;
import com.medapp.web.rest.errors.InvalidPasswordException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(value="/account", description="gestion d'utilisateurs")
public class AccountController {

	@Inject private UserService userService;
	
    @PostMapping(path = "/account/change-password")
    @ApiOperation(value="Change password for current user")
    public ResponseEntity<String> changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        if (!checkPasswordLength(passwordChangeDto.getNewPassword())) {
        	 return ResponseEntity.badRequest().header("X-validation-error", "error.passwordlength8_100").build();
        }
        try {
        	userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());			
		} catch (InvalidPasswordException e) {
			 return ResponseEntity.badRequest().header("X-validation-error", "error.currentpwdIncorrect").build();
		}
        return ResponseEntity.ok().build();
    }
	
    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= 8 &&
            password.length() <= 100;
    }
}
