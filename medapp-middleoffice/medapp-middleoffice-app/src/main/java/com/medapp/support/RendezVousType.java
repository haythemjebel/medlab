package com.medapp.support;

public enum RendezVousType {
    CONTROLE("CONTROLE"),
    CONSULTATION("CONSULTATION");
    private String name;


    RendezVousType(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
}

