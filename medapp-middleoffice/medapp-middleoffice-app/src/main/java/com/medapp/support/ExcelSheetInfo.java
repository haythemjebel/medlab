package com.medapp.support;

public class ExcelSheetInfo {

	private String nom;

	private String fileUrl;

	public String getFileUrl() {

		return fileUrl;

	}

	public void setFileUrl(String fileUrl) {

		this.fileUrl = fileUrl;

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}




}
