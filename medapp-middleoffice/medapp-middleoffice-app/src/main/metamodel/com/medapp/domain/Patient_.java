package com.medapp.domain;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Patient.class)
public abstract class Patient_ extends com.medapp.domain.AbstractAuditingEntity_ {

	public static volatile SingularAttribute<Patient, String> profession;
	public static volatile SingularAttribute<Patient, Date> dateNaissance;
	public static volatile SingularAttribute<Patient, String> ficheMedicaleNumber;
	public static volatile SingularAttribute<Patient, String> cnamNum;
	public static volatile SingularAttribute<Patient, String> nom;
	public static volatile SingularAttribute<Patient, String> atcds;
	public static volatile SingularAttribute<Patient, String> adresse;
	public static volatile SingularAttribute<Patient, String> adressePar;
	public static volatile SingularAttribute<Patient, String> tel;
	public static volatile SingularAttribute<Patient, Long> id;
	public static volatile SingularAttribute<Patient, String> diagnostic;
	public static volatile SingularAttribute<Patient, String> prenom;
	public static volatile SingularAttribute<Patient, String> email;
	public static volatile ListAttribute<Patient, RendezVous> listRendezVous;

	public static final String PROFESSION = "profession";
	public static final String DATE_NAISSANCE = "dateNaissance";
	public static final String FICHE_MEDICALE_NUMBER = "ficheMedicaleNumber";
	public static final String CNAM_NUM = "cnamNum";
	public static final String NOM = "nom";
	public static final String ATCDS = "atcds";
	public static final String ADRESSE = "adresse";
	public static final String ADRESSE_PAR = "adressePar";
	public static final String TEL = "tel";
	public static final String ID = "id";
	public static final String DIAGNOSTIC = "diagnostic";
	public static final String PRENOM = "prenom";
	public static final String EMAIL = "email";
	public static final String LIST_RENDEZ_VOUS = "listRendezVous";

}

