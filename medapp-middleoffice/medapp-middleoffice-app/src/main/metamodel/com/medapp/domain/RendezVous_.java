package com.medapp.domain;

import com.medapp.support.RendezVousType;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RendezVous.class)
public abstract class RendezVous_ extends com.medapp.domain.AbstractAuditingEntity_ {

	public static volatile SingularAttribute<RendezVous, Date> dateRendezVous;
	public static volatile SingularAttribute<RendezVous, Patient> patient;
	public static volatile SingularAttribute<RendezVous, String> diagnosticAndTraittement;
	public static volatile SingularAttribute<RendezVous, Integer> dureeRendezVous;
	public static volatile SingularAttribute<RendezVous, Long> id;
	public static volatile SingularAttribute<RendezVous, RendezVousType> typeRendezVous;

	public static final String DATE_RENDEZ_VOUS = "dateRendezVous";
	public static final String PATIENT = "patient";
	public static final String DIAGNOSTIC_AND_TRAITTEMENT = "diagnosticAndTraittement";
	public static final String DUREE_RENDEZ_VOUS = "dureeRendezVous";
	public static final String ID = "id";
	public static final String TYPE_RENDEZ_VOUS = "typeRendezVous";

}

