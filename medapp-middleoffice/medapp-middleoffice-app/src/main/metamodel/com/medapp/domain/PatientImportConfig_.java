package com.medapp.domain;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PatientImportConfig.class)
public abstract class PatientImportConfig_ extends com.medapp.domain.AbstractAuditingEntity_ {

	public static volatile SingularAttribute<PatientImportConfig, Date> dateImport;
	public static volatile SingularAttribute<PatientImportConfig, Long> id;
	public static volatile SingularAttribute<PatientImportConfig, String> nom;
	public static volatile SingularAttribute<PatientImportConfig, String> urlFile;
	public static volatile SingularAttribute<PatientImportConfig, String> status;

	public static final String DATE_IMPORT = "dateImport";
	public static final String ID = "id";
	public static final String NOM = "nom";
	public static final String URL_FILE = "urlFile";
	public static final String STATUS = "status";

}

